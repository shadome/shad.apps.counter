
# Make it count!
This app is served [here](http://localhost:8082/) and [here](https://shadome.gitlab.io/shad.apps.counter/) *(prefer using the latter)*.

Take a look at [this file](https://gitlab.com/shadome/shad.apps.counter/-/blob/master/.gitlab-ci.yml) if you want to run it locally.
# Project motivation

This site is currently under construction and aims at:

* Serving a true functional purpose
* Keeping in touch with front end design and modern-ish technologies *(PWA, SPA, DBaaS, etc.)*
* Providing fun as I'm making it!

Be warned that you **will** see TODOs. Often. Everywhere.

*TODO: remove the above as soon as it is obsolete*

## Functional purpose

The idea of this app is to keep track of... anything, really, on a daily basis. The roadmap of tracked objects include, at this point in time:

* **Food.** MyFitnessPal, and similar apps, are a great way of tracking what has been eaten in a day. This data can then be leveraged to optimise muscle growth, improve weight regulation, track nutrient deficiencies, or treat other well-being or health related concerns.
* **Focus on micronutrients.** Calorie-tracking apps exist in abundance, yet there are few to none which go down to the micro nutrient level. Everybody knows that macro nutrients are not sufficient to ensure good health, but few people have an idea of their vitamins and minerals deficiencies, and even fewer know that some micro nutrients should not exceed a certain UL *(tolerable upper intake level)* threshold. Also, some nutrients can be efficiently assimilated into body "reserves" and used even months after they were ingested *(B12, Vit. D, etc.)*, but others are quickly purged and require very frequent intake.
* **Expenses.** Taking the habit of inputting things on a daily basis is the hard part. After that, input forms and restitution views, especially on a monthly basis, will make a great tool for managing expenses and subscription costs.
* **Food + Expenses**. Knowing the price per kilogram of food is fine, but there could be other ways to put food and costs into perspective. For instance, to know the price of a food per nutrient RDA *(recommended daily allowance)* for certain nutrients scarcer than others.
* **Physical exercise.** When tracking daily consumption for calories monitoring, is it important to quantify how many calories are burnt depending on one's basal metabolic rate and occasional or recurrent physical exercise.
* **Food + Physical exercise.** Draw linear graphic estimations of calories deficit or excess at any point during the day, taking into account what is burnt through basal metabolism and timestamped physical exercise and what is ingested with timestamped meals. Track intermittent fasting. **See** what it **looks** like!
* **Counting.** Create counters which should go as high as possible, or should go down to 0, or should keep track of the evolution of a value. Count calories, weight, hours of sleep, glasses of water, remaining number of pushups for today...
* **Visualise.** Every one of the above data points share something in common: they are timestamped numbers. Put those into a data visualisation tool and draw!
* **etc.**

# The stack

This application is served as an SPA and uses the browser's databases *(IndexedDB)* exclusively. It is built to run offline.

## Back end
Additional back end support is intended, either with a centralised default back end *(requires a user account management system, or OAuth2 authentication)* or with interconnection to outside DBaaS *(e.g., handling a FaunaDB authentication token, for users who want to keep track of their personal data)*.

An eventual true back end should be able to interoperate with IndexedDB, so that the applications keeps its offline capabilities. This will require a layer for handling synchronisation and conflicts between multiple data sources. For instance, a user might edit a previous (already synchronised) input on their mobile phone, delete the same input on their browser, and then try to synchronise both. Should they have an explicit conflict? Should all conflicts be implicitly resolved using timestamps?
## Styling
In order to get things done quickly, styling is mostly done using plain JSX/JavaScript with the `style` attribute of every `html` elements, e.g., `<div style={{...}} />`. Doing so is certainly not considered a good practice, but it works well and I haven't found much objective literature about it being bad either. 

Creation of `divs` for the sake of creating `divs` is also avoided: each `html` tag should have a meaning or should not exist. For instance, if a React custom component is made around a text input, its JSX should only expose an `<input ... />` component,  and should not wrap it in unnecessary `<div />`.
## Libraries
Third-party libraries, especially design and component libraries, are kept to a minimum number. Some pages of this application still use the MUI components library, but it is incrementally migrated towards components designed without any third-party library. The intention is to use plain JavaScript and CSS wherever possible.

A few words of personal motivation:
* Typical libraries don't feel indispensable and I want to see if not using them makes development *that much* slower.
* I have many ex-colleagues who complained on how difficult and time consuming it is to migrate to newer versions for mature production apps with a large dependency list. This somewhat violates the rule *Don't fix what is not broken*.
* It is also my belief that making one library as generic as possible in order to cover most use-cases on most browsers necessarily comes with a big trade-off in performance, readability of the generated css, complexity of APIs, etc. Trying to resolve issues arising as a result of using complex code with some even more complex code is not my thing, though it has already happened more than once with MUI components and style overrides I think the learning curve should be orientated towards learning a language or a framework, not a specific library's implementation choices.
* This implementation is expected to be easily refactorable using `tailwind`, which seems to best fit my needs and mindset

In any case, an abstraction for general-purpose components is sometimes desirable, especially when JavaScript event-handling or DOM manipulations are required. A few words about abstraction below.

## Abstraction

TODO abstraction is a tool which should be used carefully / mindfully, link to the talk of the big guy about general purpose consensual coding conventions

TODO incorporate what's still true from coding_styles.md

TODO talk about "highest order" abstractions which must be tested following Ian Cooper's insightful talk 


