# Todolist
## V0.1 - Adding support for generic counters
### MUST
- [ ] Integrate counters into the daily meal page
- [ ] Allow display (as recipe) and modification of user foods
- [ ] Set user food's base value (100g, 150g, etc.)
- [ ] Import / export user data
- [ ] Trigger validations for transforming a grouped meal into a new food
- [ ] Allow the user to input its own new food, without starting from a meal group
- [ ] Create a daily counter configuration store: name, +/- component value, initial value (yesterday's / constant), available tags, available and ordered colours.
- [ ] Create a store for counter counter inputs (different from mealstore but similar): counter name, value, day-based date, tags, colours.
- [ ] New menu entry: setup a new daily counter whose form registers a new row in the daily counter configuration store.
- [ ] New row in the daily tracking page for each counter, with quick-submit inner form (+/- value, add a colour, add/create a tag).
- [ ] New menu entry: calendar view, allowing to select a counter and review value inputs for the last month. Must display the value on each day cell, (maybe) the current month + the last one OR 'fixed last 4 weeks', the cell outlined in the first checked colour for the day, a coloured dot for every colours of the day, a label for each tag of the day, swiping or scrollbar mechanics to review past months, the mean value for the reviewed month, the amount number of colours and tags for the reviewed month.
### SHOULD
- [ ] Add a description of what is a daily counter.
### COULD
- [ ] New menu entry: graph view, similar to the calendar view but focused on linear graphs which go up and down, similarly to excel's, with the day of month on the x-axis.
## v0
### MUST
- [x] ~~*Add meal: input a quantity in grams, an overridable label and an overridable date*~~
- [x] ~~*Add meal: search from existing Ciqual entries*~~
- [x] ~~*Meal list: display a cool total bar with daily recommendations comparison*~~
- [x] ~~*Meal list: split the list page per day: add a next/previous day, a calendar, load the current day, etc.*~~
- [x] ~~*Meal list: split the list page per day: on submission, catch up the given date so that it matches the one from which the Add Meal dialog has been opened (users can reach another day using +/- minutes)*~~
- [x] ~~*Navigation bar: store the user's settings (colour, mode, etc.) in the IDB*~~
- [x] ~~*Navigation bar: all text should be white because the background color is always primary.900*~~
- [x] ~~*Navigation bar: name the colours in the custom colour selection (tooltip or to the side in the dropdown)*~~
- [x] ~~*Navigation bar: custom colour selection*~~
- [x] ~~*Navigation bar: dark/light modes toggle*~~
- [x] ~~*Data configuration: create a store for foods*~~
- [x] ~~*Data configuration: load the Ciqual's json into IDB (and vice-versa)*~~
- [x] ~~*Data configuration: make meal store point towards one or more foods of the food store (non-relational to relational) => DROPPED, would make statistics too hard*~~
- [x] ~~*Food list: make the app unusable unless the user has downloaded and loaded the ciqual's food into its IndexedDB (with size of the bundle if possible)*~~
- [x] ~~*Menu: make Menu Entries selected or not considering the current URL - use Match / History in the props*~~
- [ ] Food list: display the foods which are stored in indexedDB
- [ ] Add categories to foods
- [ ] Add meal: creating a meal immediately allows to input more than one food and creates a group, a single-food-meal is a specific case
- [ ] Meal list: a meal's group is stored in a dedicated store in a relational way (but a group is still unrequired for individual meals to figure in the meal list)
- [ ] Meal list: allow the creation of a recipe from a meal
- [ ] Data configuration: make the Ciqual's json excluded from the bundle.js by default => DONE BUT NOT WORKING, FIXME
- [ ] Recipes: create a store for recipes (points towards the food store as well)
- [ ] Recipes: allow the creation of a meal from a recipe
- [ ] Add a non-intrusive option to keep track of some food's storage, e.g., the user comes back from groceries and can input whatever they have bought, and then use it instead of general purpose database food when creating a meal (and the quantity of the ingredient decreases). Also serves for cakes etc., when somebody has cooked something and is eating a part of it. Some more reflection is required about the difference between cooked and uncooked units (will be the same in recipe), e.g., some water evaporates and 100g of cooked meal is not worth 100g of mixed ingredients

### SHOULD
- [x] ~~*Navigation bar: store the user's settings (colour, mode, etc.) in the IDB*~~
- [x] ~~*Navigation bar: menu to the left with all the entries rather than on the nav bar itself + responsive burger menu*~~
- [x] ~~*Navigation bar: center the app bar content, change the title depending on the current page*~~
- [x] ~~*Meal list: make it possible to know what nutrients come from what meal(s)*~~
- [x] ~~*Replace the indexeddb package with the native api*~~
- [ ] Refactoring: use functions instead of lambdas in components and avoid prefixing selector variables with `_`
- [ ] Navigation bar: fix the glitch of the navbar's theme and colour not synchronised with the rest of the UI
- [ ] Food list: make the foods searchable, esp. by category enabled/disabled (make wide-square toggles with icons?) + sorted by selected nutrient descendingly
- [ ] Food list: make it possible to check the average count of each nutrient per category/ies
- [ ] Meal list: use IDB's range object through the repository layer rather than loading the full database and filtering in the .jsx
- [ ] Meal list: display an even cooler graph for macros' evolution over time
- [ ] Meal list: add buttons for recurrent meals (vitamin B12, coffee, water, etc.)(bonus: make it customisable)
### COULD
- [ ] Navigation bar: EN/FR toggle