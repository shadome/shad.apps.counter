// libraries
import * as React from 'react';
// project dependencies
import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';

export default function PrevNextDateBar({  
    dayjsDate,
    onClickPrev = () => {},
    onClickNext = () => {},
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    return  (<>
        <div
            {...props}
            style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                ...props.style,
            }}
        >
            <a
                onClick={onClickPrev}
                className="hov-fg hov-underline"
                style={{
                    color: css.colour.text.strong,
                    cursor: 'pointer',
                    // text
                    fontWeight: 'bolder',
                    fontSize: css.size.font.sm,
                    // link as a container
                    display: 'inline-flex',
                    alignItems: 'center',
                    gap: '6px',
                }}
            >
                <Icons.ChevronLeft style={{
                    height: css.size.icons.xs,
                    width: css.size.icons.xs,
                }} />
                <span>
                    {dayjsDate?.add(-1, 'd').format('DD/MM/YYYY')}
                </span>
            </a>
            <a
                onClick={onClickNext}
                className="hov-fg hov-underline"
                style={{
                    color: css.colour.text.strong,
                    cursor: 'pointer',
                    // text
                    fontWeight: 'bolder',
                    fontSize: css.size.font.sm,
                    // link as a container
                    display: 'inline-flex',
                    alignItems: 'center',
                    gap: '6px',
                }}
            >
                <span>
                    {dayjsDate?.add(1, 'd').format('DD/MM/YYYY')}
                </span>
                <Icons.ChevronRight style={{
                    height: css.size.icons.xs,
                    width: css.size.icons.xs,
                }} />
            </a>
        </div>
    </>);
}
