import * as React from 'react';
import * as dayjs from 'dayjs';
import { useLocation, matchPath } from 'react-router-dom';

import { NavbarHeightInPx } from 'front/containers/navbar/Navbar';
import * as Contexts from 'context'
import MenuEntry from './MenuEntry';
import MenuSubEntry from './MenuSubEntry';
import Pages from 'front/pages';

import Drawer from 'front/components/Drawer';

export const MenuFixedWidthInPx = 230;

export default function Menu({
    isMenuMinimised,
    isOpen,
    onClose,
}) {
    const css = React.useContext(Contexts.CssContext);
    const [isCountingExpanded, setIsCountingExpanded] = React.useState(true);
    const [isFoodEntryExpanded, setIsFoodEntryExpanded] = React.useState(true);

    function isSelected(paths) {
        const locationPathname = useLocation().pathname;
        const match = matchPath(locationPathname, { path: paths, exact: true });
        return match !== null;
    };
    
    const MenuContent = () => (<>
        <div style={{
            width: '100%',
            height: '100%',
            overflow: 'auto',
        }}>
            <MenuEntry
                label="Home"
                icon="GiHome"
                isSelected={isSelected(Pages.HomePage.paths)}
                routerLinkHref={Pages.HomePage.paths[0]}
                onClick={() => { isMenuMinimised && onClose(); }}
            />

            <MenuEntry
                label="Today"
                icon="GiCalendarToday"
                routerLinkHref={Pages.DashboardPage.getUrl(dayjs())}
                isSelected={isSelected(Pages.DashboardPage.paths)}
                onClick={() => { isMenuMinimised && onClose(); }}
            />
            {/* link disabled in production until it works */}
            {/* <MenuEntry
                label="Dashboard"
                icon="Dashboard"
                routerLinkHref={Pages.DashboardPage.getUrlFunc(dayjs())}
                isSelected={isSelected(Pages.DashboardPage.paths)}
                onClick={() => { isMenuMinimised && onClose(); }}
            />
            */}
            {/* link disabled in production until it works */}
            {/* <MenuEntry
                label="Counting"
                icon="Calculator"
                isExpandable
                isExpanded={isCountingExpanded}
                onClick={() => { setIsCountingExpanded(!isCountingExpanded); }}
            >

                <MenuSubEntry
                    label="Create a counter"
                    routerLinkHref={Pages.CounterAddPage.paths[0]}
                    isSelected={isSelected(Pages.CounterAddPage.paths)}
                    onClick={() => { isMenuMinimised && onClose(); }}
                />

            </MenuEntry> */}

            <MenuEntry
                label="Food"
                icon="GiSandwich"
                routerLinkHref={Pages.FoodPage.paths[0]}
                isSelected={isSelected(Pages.FoodPage.paths)}
                onClick={() => { isMenuMinimised && onClose(); }}
            />
{/* 
                <MenuSubEntry
                    label="Daily tracking"
                    routerLinkHref={`/meals/${dayjs().format('YYYY-MM-DD')}`}
                    isSelected={isSelected(Pages.MealListPage.paths)}
                    onClick={() => { isMenuMinimised && onClose(); }}
                />
                    
                <MenuSubEntry
                    label="Add a custom food"
                    routerLinkHref={Pages.FoodAddPage.paths[0]}
                    isSelected={isSelected(Pages.FoodAddPage.paths)}
                    onClick={() => { isMenuMinimised && onClose(); }}
                /> */}
                    
            {/* link disabled in production until it works */}
                {/* <MenuSubEntry
                    label="Registered foods"
                    routerLinkHref={Pages.FoodListPage.paths[0]}
                    isSelected={isSelected(Pages.FoodListPage.paths)}
                    onClick={() => { isMenuMinimised && onClose(); }}
                /> */}
                
                {/* Overview, Recipes, Side preparations */}

            {/* Expenses, Daily tracking, Incoming expenses, Recurring expenses */}

            <MenuEntry
                label="Data management"
                icon="GiFloppyDiskUp"
                isSelected={isSelected(Pages.DataPage.paths)}
                routerLinkHref={Pages.DataPage.paths[0]}
                onClick={() => { isMenuMinimised && onClose(); }}
            />

            <MenuEntry
                label="Showcase"
                icon="GiCubeforce"
                isSelected={isSelected(Pages.ShowcasePage.paths)}
                routerLinkHref={Pages.ShowcasePage.paths[0]}
                onClick={() => { isMenuMinimised && onClose(); }}
            />
        </div>
    </>);


    return (<>
        {isMenuMinimised ? (
            <Drawer
                open={isOpen}
                onClose={onClose}
                position="left"
                style={{ width: '300px' }}
            >
                <MenuContent />
            </Drawer>
        ): (
            <div style={{
                position: 'absolute',
                top: `${NavbarHeightInPx}px`,
                left: '0',
                minWidth: `${MenuFixedWidthInPx}px`,
                maxWidth: `${MenuFixedWidthInPx}px`,
                height: `calc(100% - ${NavbarHeightInPx}px)`,
                overflowY: 'auto',
                backgroundColor: css.colour.background.dp1,
                scrollbarColor: `${css.colour.background.dp2} ${css.colour.background.dp1}`,
                borderRight: `solid 2px ${css.colour.border}`,
            }}>
                <MenuContent />
            </div>
        )}
    </>);
};
