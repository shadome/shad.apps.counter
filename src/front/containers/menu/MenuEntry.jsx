import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';
import { MenuSubEntryHeightInPx } from './MenuSubEntry';

function MenuEntryLink(props) {
    return (<>
        {props.to !== undefined
            ? <RouterLink {...props} />
            : <a {...props} />
        }
    </>);
    
};
    
export default function MenuEntry({
    icon,
    label,
    isSelected,
    isExpandable,
    isExpanded,
    routerLinkHref,
    onClick,
    children,
}) {
    const css = React.useContext(Contexts.CssContext);

    const IconComponent = Icons[icon];

    const [isHovered, setIsHovered] = React.useState(false);
    const overlay = (
        isSelected && isHovered ? css.overlay.hoveredAndSelected :
        isSelected ? css.overlay.selected :
        isHovered ? css.overlay.hovered :
        css.overlay.none);
    const borderRight = isSelected
        ? `6px solid ${css.colour.aspect.info.average}`
        : undefined;
    // note that this const is only exploited if the children is not undefined,
    // however, if there are only one children, 'children' is not an array 
    // (and has no `.length`, hence `?? 1`) but directly the child object (javascript TM >.<)
    const childrenHeightInPx = MenuSubEntryHeightInPx * (children?.length ?? 1);

    return (<>
        {/* <style>{`
            /* Dev's note about the following animations:
             * I'm testing it on Firefox and for some reason the collapse and expand animations
             * do not have the same rendering speed when specified with the same CSS timings.
             * Note 2: found out why, the 'maxHeight' in case of expansion actually goes all the way to its 100%, which is much more than the actual size of the collapse zone,
             * as stated below, read https://css-tricks.com/using-css-transitions-auto-dimensions/
             * It seems calculating the true height of the element using constants and/or pure javascript is the only true solution. Not prioritised yet.
             * Note 3: all those comments are old, this whole animation block is kept just as an example :) 
            .collapse-animation {
                max-height: 100%;
                overflow: hidden;
                animation: collapse 0.4s ease-out 0s 1 normal forwards;
            }

            @keyframes collapse {
                from    { max-height: 100%; }
                to      { max-height: 0%;   }
            }

            .expand-animation {
                max-height: 0%;
                overflow: hidden;
                animation: expand 1s ease-out 0.3s 1 normal forwards;
            }

            @keyframes expand {
                from    { max-height: 0%;   }
                to      { max-height: 100%; }
            }

            end of block comment : *
        `}</style> */}
        <MenuEntryLink
            to={routerLinkHref}
            onClick={onClick}
            style={{
                cursor: 'pointer',
                textDecoration: 'none',
                color: 'inherit',
            }}
        >
            <div
                onMouseEnter={() => { setIsHovered(true); }}
                onMouseLeave={() => { setIsHovered(false); }}
                style={{
                    backgroundColor: overlay,
                    borderRight: borderRight,
                    minHeight: '50px',
                    maxHeight: '50px',
                    padding: '8px',
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                {Boolean(IconComponent) &&
                    <IconComponent style={{
                        height: '40px',
                        width: '40px',
                        padding: '4px',
                        marginRight: '24px'
                    }} />
                }
                <span style={{
                    flexGrow: '1',
                    fontSize: '16px',
                    fontWeight: '400',
                    letterSpacing: '0.15px',
                    lineHeight: '24px',
                    overflow: 'hidden',
                    textoverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                }}>
                    {label}
                </span>
                {isExpandable &&
                    <div style={{
                        height: '40px',
                        width: '40px',
                        padding: '8px',
                    }}>
                        {isExpanded
                            ? <Icons.ChevronUp />
                            : <Icons.ChevronDown />
                        }
                    </div>
                }
            </div>
        </MenuEntryLink>
        {/* Dev's note: the following is still interesting but not working out of the box; 
            Mui uses it but seems to compute the 'height' in pixels 
            for the transitions (and then put it to 'auto' if expansion).
            Computing the height in pixels feels a bit too overzealous for me, animations will do. */}
        {/* Dev's note 2: read https://css-tricks.com/using-css-transitions-auto-dimensions/ for more info. */}
        {/* Dev's note 3: finally implemented it as it should be, the above comments are kept for information. */}
        {children &&
            <div style={{
                height: isExpanded ? `${childrenHeightInPx}px` : '0px',
                overflow: 'hidden',
                transition: 'height .2s',
            }}>
                {children}
            </div>
        }
        <hr style={{
            border: 'none',
            borderTop: '2px solid',
            borderColor: css.colour.border,
            margin: '0px',
        }} />
    </>);
}