import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';
    
const hrHeightInPx = 2;
const menuSubEntryRowHeightInPx = 40;
export const MenuSubEntryHeightInPx = hrHeightInPx + menuSubEntryRowHeightInPx;

export default function MenuSubEntry({
    label,
    isSelected,
    routerLinkHref,
    onClick,
}) {
    const css = React.useContext(Contexts.CssContext);

    const [isHovered, setIsHovered] = React.useState(false);
    const overlay = (
        isSelected && isHovered ? css.overlay.hoveredAndSelected :
        isSelected ? css.overlay.selected :
        isHovered ? css.overlay.hovered :
        css.overlay.none);
    const borderRight = isSelected
        ? `6px solid ${css.colour.aspect.info.average}`
        : undefined;
    
    return (<>
        
        <hr style={{
            visibility: isSelected ? 'hidden' : 'visible',
            border: 'none',
            borderTop: `${hrHeightInPx}px solid ${css.colour.border}`,
            margin: '0px',
            width: '80%',
            marginLeft: 'auto',
            marginRight: 'auto',
        }} />

        <RouterLink
            to={routerLinkHref}
            onClick={onClick}
            style={{
                cursor: 'pointer',
                textDecoration: 'none',
                color: 'inherit',
            }}
        >
            <div
                onMouseEnter={() => { setIsHovered(true); }}
                onMouseLeave={() => { setIsHovered(false); }}
                style={{
                    paddingLeft: '40px',
                    backgroundColor: overlay,
                    borderRight: borderRight,
                    minHeight: `${menuSubEntryRowHeightInPx}px`,
                    maxHeight: `${menuSubEntryRowHeightInPx}px`,
                    padding: '8px',
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <div style={{
                    width: `40px`,
                    height: `40px`,
                    padding: '8px',
                    marginLeft: '16px'
                }}>
                    <Icons.TriangleArrowRight />
                </div>
                <span style={{
                    flexGrow: '1',
                    marginLeft: '8px',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                }}>
                    {label}
                </span>
            </div>
        </RouterLink>
    </>);
}