import * as React from 'react';

import Tag from 'front/components/Tag';
import { aspectToIconName } from 'front/components/Icons';

export default function FeedbackArea({
    feedbacks,
}) {
    return (<>
        <div style={{
            position: 'absolute',
            bottom: '0px',
            right: '0px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'right',
            alignItems: 'flex-end',
            gap: '8px',
            padding: '8px',
        }}>
            {feedbacks.map(_feedback => (
                <Tag
                    key={_feedback.id}
                    size="lg"
                    aspect={_feedback.aspect}
                    variant="contained"
                    label={_feedback.label}
                    onClose={_feedback.onClose}
                    icon={aspectToIconName[_feedback.aspect]}
                    style={{
                        // allow wrapping for multiline text and recognise line breaks
                        whiteSpace: 'pre-line',
                        // add some extra space before and after the text
                        gap: '4px',
                        // textShadow: '1px black',
                        // text-stroke equivalent
                        // textShadow: `-1px -1px 0 ${colour}, 1px -1px 0 ${colour}, -1px 1px 0 ${colour}, 1px 1px 0 ${colour}`,
                        fontWeight: '500',
                    }}
                />
            ))}
        </div>
    </>);
}