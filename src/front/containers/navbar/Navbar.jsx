// libraries
import * as React from 'react';
// project dependencies
import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';
import * as SettingsStore from 'repository/StoreSettings';
import { MenuFixedWidthInPx } from 'front/containers/menu/Menu';
// project components
import ThemeModeToggle from 'front/components/ThemeModeToggle';

export const NavbarHeightInPx = 64;

export default function Navbar({
    title,
    setThemeMode,
    toggleOpenMenu,
    isMenuMinimised,
}) {
    const addFeedback = React.useContext(Contexts.AddFeedbackContext);
    const themeMode = React.useContext(Contexts.ThemeModeContext);
    const css = React.useContext(Contexts.CssContext);
    async function doSetThemeMode(newThemeMode) {
        if (newThemeMode === null || newThemeMode === themeMode) {
            return;
        }
        const newSettings = {
            mode: newThemeMode,
        };
        await SettingsStore.Delete();
        await SettingsStore.Create({ item: newSettings });
        setThemeMode(newThemeMode);
        if (newThemeMode === 'light') {
            addFeedback({
                label: 'Fool of a Took! \n This functionality is not ready yet and is way too dangerous for a small Hobbit like you. \n Switch the light off and walk away!',
                aspect: 'warning',
                displayTimeInMs: 15000,
            });
        }
    };

    return (<>
        <nav style={{
            display: 'block',
            zIndex: css.zIndexes.applicationBar,
            position: 'absolute',
            top: '0',
            left: '0',
            minWidth: '100%',
            maxWidth: '100%',
            minHeight: `${NavbarHeightInPx}px`,
            maxHeight: `${NavbarHeightInPx}px`,
            paddingRight: '24px',
            paddingLeft: '24px',
            backgroundColor: css.colour.background.dp1,
            border: 'none',
            borderBottom: '2px solid',
            borderColor: css.colour.border,
        }}>
            <div style={{
                height: `${NavbarHeightInPx}px`,
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
            }}>
                <div style={{
                    display: 'inline-block',
                }}>
                    {isMenuMinimised ?
                        <button
                            onClick={toggleOpenMenu}
                            style={{
                                cursor: 'pointer',
                                height: '36px',
                                width: '36px',
                                padding: '6px',
                                border: `1px solid ${css.colour.border}`,
                                borderRadius: '4px',
                                marginRight: '16px',
                            }}
                        >
                            <Icons.BurgerList />
                        </button>
                        :
                        <div style={{
                            width: `${MenuFixedWidthInPx}px`,
                            fontSize: '24px',
                            fontWeight: '400',
                            letterSpacing: 'normal',
                            lineHeight: '32px',
                            overflow: 'hidden',
                            textoverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                        }}>
                            Counter
                        </div>
                    }
                </div>
                <div style={{
                    display: 'inline-block',
                    flexGrow: '1',
                    fontSize: '20px',
                    fontWeight: '500',
                    letterSpacing: '0.15px',
                    lineHeight: '32px',
                    overflow: 'hidden',
                    textoverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                }}>
                    {title}
                </div>
                <ThemeModeToggle
                    themeMode={themeMode}
                    setThemeMode={doSetThemeMode}
                />
            </div>
        </nav>
    </>);
};