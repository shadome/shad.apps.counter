// libraries
import * as React from 'react';
// project dependencies
import * as FoodAdapter from 'model/adapters/FoodAdapter';
import * as FoodStore from 'repository/StoreFood';
import * as Contexts from 'context';
import * as Breakpoints from 'front/Breakpoints';
import * as Icons from 'front/components/Icons';
// project components
import Button from 'front/components/Button';
import CircularProgress from 'front/components/CircularProgress';

export default function EmptyFoodDatabaseContainer() {
    /* Context */
    const { _, setFoodDatabaseEmpty } = React.useContext(Contexts.IsFoodDatabaseEmptyContext);

    /* Variables */
    const [processed, setProcessed] = React.useState(0);
    const [totalToProcess, setTotalToProcess] = React.useState(0);

    const percent = totalToProcess > 0
        ? Math.min(100, Math.round(Number(processed) / Number(totalToProcess) * 100))
        : 0;

    /* Submission methods */
    const doLoadFoodDatabaseFromCiqual = async () => {
        if (totalToProcess > 0) {
            return; // void if a treatment is already running
        }
        // https://reactjs.org/docs/code-splitting.html
        // TODO the following syntax + the current webpack config does not suffice to download the
        // CiqualEnData.js file "just in time", no idea why, cf.firefox's F12 > Network > all > CTRL+F5
        const { default: ciqualEnData } = await import('../../../../data/external_data_source/ciqual/CiqualEnData');
        const foodFromReferential = ciqualEnData.map(FoodAdapter.ciqualToFood);
        setTotalToProcess(foodFromReferential.length);
        // % 100 precondition to avoid throttle (too much rerendering)
        const progressUpdateCallback = (i) => i % 100 === 0 && setProcessed(i);
        await FoodStore.Create({
            items: foodFromReferential,
            currentProgressCallback: progressUpdateCallback,
        });
        setProcessed(0);
        setTotalToProcess(0);
        setFoodDatabaseEmpty(false);
    };

    const CiqualDatabaseLink = (props) => (
        <a
            href='https://ciqual.anses.fr/'
            target="_blank"
            {...props}
        />
    );

    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);
    const isTinyDisplay = breakpoint < Breakpoints.xs;
    return (
        <>
            <fieldset style={{
                borderRadius: '1px',
                border: `1px solid ${css.colour.aspect.warning.average}`,
                color: css.colour.aspect.warning.foregroundSoft,
                padding: '16px',
                width: '100%',
                maxWidth: '100%',
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                cursor: 'default', // avoid cursor: text on pure html text
                fontSize: css.size.font.sm,
            }}>
                <legend style={{
                    padding: '0px 8px',
                    fontSize: css.size.font.sm,
                    fontWeight: '600',
                    color: css.colour.aspect.warning.foreground,
                    maxHeight: '0px',
                    margin: '0px',
                    overflowY: 'visible',
                    display: 'inline-flex',
                    gap: '8px',
                    alignItems: 'center',
                }}>
                    <Icons.Warning style={{
                        width: css.size.icons.sm,
                        height: css.size.icons.sm,
                    }} />
                    Your local database is empty
                </legend>
                <div> {/* text-field content */}
                    <p>Your local food database is empty. This means you cannot use the application yet.</p>
                    <p>
                        To download the <CiqualDatabaseLink>Ciqual's food database</CiqualDatabaseLink> and
                        insert it into your browser's database, <strong>click on the download button</strong>.
                    </p>
                    <p>Note that if you have configured your browser to clear site data when closed, you might want to add an exception for this web application.</p>
                </div>
                {totalToProcess === 0 &&
                    <Button
                        aspect="warning"
                        onClick={() => { doLoadFoodDatabaseFromCiqual(); }}
                        variant="outlined"
                        icon="Download"
                        label="Download and install"
                        style={{ width: isTinyDisplay ? '100%' : 'fit-content' }}
                    />
                }
                {totalToProcess > 0 &&
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '4px',
                        alignItems: 'center',
                    }}>
                        <p>
                            <em>Importing the Ciqual food database into the browser's database...</em>
                        </p>
                        <CircularProgress
                            percent={percent}
                            aspect="warning"
                            diameterInPx={100}
                            label={`${processed}/${totalToProcess}`}
                            mustDisplayPct
                        />
                    </div>
                }
            </fieldset>
        </>
    );
}
