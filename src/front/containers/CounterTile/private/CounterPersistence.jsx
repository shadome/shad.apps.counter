// libraries
import * as React from 'react';
// project dependencies
import CounterForm from './CounterForm';
import * as CounterStore from 'repository/StoreCounter';
import { DecimalPrecisionInitialValue } from './counter_layout/SettingsSection';

export const CounterStates = {
    daily: 'DAILY',
    dailyInit: 'DAILY_INITIALISATION',
    creation: 'CREATION',
    settings: 'SETTINGS',
};

export default function CounterPersistence({
    counterId,
    counterDate,
    areSettingsOpen,
    onOpenSettings,
    onSubmit,
    onCancel,
}) {

    /* Set this component's state (the same code can render different component's states)
     * Note that this code should conceptually be found in the CounterContainer layer, but
     * it needs to access the database.
     * when counterId == null && counterDate != null:
     *  - date is considered null, jumps below
     * when counterId == null && counterDate == null:
     *  - The component proposes a creation tile
     * when counterId != null && counterDate == null:
     *  - The component proposes to modify settings, daily counts are displayed for a better immersion but daily controls are disabled
     * when counterId != null && counterDate != null && counterDate exists:
     *  - The component proposes to modify the count of the given day
     * when counterId != null && counterDate != null && NOT(counterDate exist):
     *  - The component proposes to start counting for the given day, every other control is disabled
     */
    // TODO handle is_active (for now they are all active)
    // => activity can be changed in the settings view but is incompatible with the DAILY or DAILY_INIT view
    const [counterState, setCounterState] = React.useState(null);
    async function loadCounterState() {
        let isMounted = true;
        if (counterId === null) {
            setCounterState(CounterStates.creation);
        } else if (counterDate === null) {
            setCounterState(CounterStates.settings);
        } else if (Boolean(counterId)) {
            const existingCounterArray = await CounterStore.Read(counterId);
            const existingCounter = Array.isArray(existingCounterArray) && existingCounterArray.length > 0
                ? existingCounterArray[0]
                : null;
            if (Boolean(existingCounter)) {
                setCounterState(CounterStates.daily);
                loadCounterInitialSettings(existingCounter);
            } else {
                setCounterState(CounterStates.dailyInit);
            }
        }
        return () => { isMounted = false; };
    }
    React.useEffect(loadCounterState, []);

    function loadCounterInitialSettings(counter) {
        // TODO
        // Dev's note: if there is a counterId and a counterDate, 
        // i.e., the counter state is 'DAILY', it is required to merge
        // currently available tags and formerly checked tags, as the current implementation choice
        // is to keep and display tags which were checked during day N but deleted later
        // when re-consulting the counter's day N. Keeping those tags checked should not 
        // provoke an error. Un-checking those tags should make them disappear for this day.
        setInitialTitle(counter.title);
        setInitialStep(counter.step);
        setInitialTags(counter.tags);
        setInitialNewDaysValue(counter.new_day_default_value);
        setInitialIsNewDaysValueYesterdays(counter.starts_with_last_count);
        setInitialDecimalPrecision(counter.decimal_precision);
        setInitialIsHistoryEnabled(counter.is_history_enabled);
        setInitialPctEvolution(counter.evolution_percent_objective);
    }

    async function doSubmit({ counterSettings = undefined }) {
        const result = Boolean(counterId)
            ? await doUpdateCounter(CounterStore, counterId, counterSettings)
            : await doCreateCounter(CounterStore, counterSettings);
        onSubmit();
        return result;

        async function doCreateCounter(store, settings) {
            let result = false;
            await store
                .Create({ item: settings })
                .then(_ => { result = true; })
                .catch(error => {
                    console.warn(error);
                    result = false;
                });
            return result;
        }
        async function doUpdateCounter(store, id, settings) {
            let result = false;
            const updatedItem = {
                id: id,
                ...settings
            };
            await store
                .Update({ updatedItem: updatedItem })
                .then(_ => { result = true; })
                .catch(error => {
                    console.warn(error);
                    result = false;
                });
            return result;
        }
    }

    // initial states for the current counter entry, if any
    const [initialCount, setInitialCount] = React.useState(117.28123809412);
    const [initialColour, setInitialColour] = React.useState(null);
    const [initialCheckedTags, setInitialCheckedTags] = React.useState([]);
    // initial states for the current counter settings, if any
    const [initialStep, setInitialStep] = React.useState(1.1);
    const [initialNewDaysValue, setInitialNewDaysValue] = React.useState(0);
    const [initialTitle, setInitialTitle] = React.useState('');
    const [initialIsNewDaysValueYesterdays, setInitialIsNewDaysValueYesterdays] = React.useState(false);
    const [initialDecimalPrecision, setInitialDecimalPrecision] = React.useState(DecimalPrecisionInitialValue);
    const [initialIsHistoryEnabled, setInitialIsHistoryEnabled] = React.useState(false);
    const [initialTags, setInitialTags] = React.useState([]);
    const [initialPctEvolution, setInitialPctEvolution] = React.useState('none');

    return (<>
        <CounterForm
            purpose={counterState}
            // initial counter values
            initialCount={initialCount}
            initialColour={initialColour}
            initialCheckedTags={initialCheckedTags}
            // initial counter settings
            initialTitle={initialTitle}
            initialNewDaysValue={initialNewDaysValue}
            initialIsNewDaysValueYesterdays={initialIsNewDaysValueYesterdays}
            initialStep={initialStep}
            initialDecimalPrecision={initialDecimalPrecision}
            initialIsHistoryEnabled={initialIsHistoryEnabled}
            initialTags={initialTags}
            initialPctEvolution={initialPctEvolution}
            // other props
            areSettingsOpen={areSettingsOpen}
            onOpenSettings={onOpenSettings}
            onSubmit={doSubmit}
            onCancel={onCancel}
        />
    </>);
}
