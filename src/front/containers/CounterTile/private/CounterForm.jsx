// libraries
import * as React from 'react';
// project dependencies
import * as Checkers from 'utils/Checkers';
import { Form, FormComponent } from 'utils/FormComponent';
import CounterLayout from './CounterLayout';
import * as CounterStore from 'repository/StoreCounter';

export default function CounterForm({
    purpose,
    initialCount,
    initialTitle,
    initialColour,
    initialIsNewDaysValueYesterdays,
    initialNewDaysValue,
    initialStep,
    initialDecimalPrecision,
    initialIsHistoryEnabled,
    initialTags,
    initialCheckedTags,
    initialPctEvolution,
    areSettingsOpen,
    onOpenSettings,
    onSubmit,
    onCancel,
}) {
    // this value changes (whatever its actual value is) whenever errors are re-triggered
    const [errorResetFlag, setErrorResetFlag] = React.useState(false);
 
    let initialTagsIncludingOldCheckedTags = /* pattern matching */(
        /* case #1*/ Array.isArray(initialCheckedTags) && !Array.isArray(initialTags) ? [...initialCheckedTags] :
        /* case #2 */ Array.isArray(initialCheckedTags) && Array.isArray(initialTags) ? initialTags.concat(initialCheckedTags) :
        /* default */ []
    );
    // remove duplicates
    initialTagsIncludingOldCheckedTags = initialTagsIncludingOldCheckedTags.filter((_tag, _idx) => initialTagsIncludingOldCheckedTags.indexOf(_tag) === _idx);

    /* Form state values */
    const form = new Form()
        .addComponent({
            propertyName: 'count',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialCount), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => !isNaN(value),
                    errorMessage: 'Invalid counter value',
                })
        })
        .addComponent({
            propertyName: 'selectedColour',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialColour), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => !Boolean(value) || Checkers.isCssColour(value), // reset value or meaningful value
                    errorMessage: 'Invalid counter colour',
                })
        })
        .addComponent({
            propertyName: 'counterTitle',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialTitle), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => value?.trim().length > 0,
                    errorMessage: 'The counter title must not be empty',
                })
                .addValidation({
                    predicate: (value) => value?.trim().length <= 40,
                    errorMessage: 'The counter title cannot exceed 40 characters',
                })
                .addValidationAsync({
                    predicate: async (value) =>
                        !(await CounterStore.Read())
                            .map(_counter => _counter.title)
                            .includes(value.trim()),
                    errorMessage: 'A counter with this title already exists',
                })
        })
        .addComponent({
            propertyName: 'newDaysValue',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialNewDaysValue), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => !isNaN(value),
                    errorMessage: 'The new day\'s value must be a number',
                })
        })
        .addComponent({
            propertyName: 'isNewDaysValueYesterdays',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialIsNewDaysValueYesterdays) })
        })
        .addComponent({
            propertyName: 'increment',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialStep), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => !isNaN(value) && Number(value) > 0,
                    errorMessage: 'The plus/minus step must be a strictly positive number',
                })
        })
        .addComponent({
            propertyName: 'decimalPrecision',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialDecimalPrecision), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => !isNaN(value),
                    errorMessage: 'Invalid decimal precision',
                })
        })
        .addComponent({
            propertyName: 'isHistoryEnabled',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialIsHistoryEnabled) })
        })
        // Note: tags and checked tags are kept separate because the first is a global counter value and the other a counter instance value (checked or not depends on the day)
        .addComponent({
            propertyName: 'tags',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialTagsIncludingOldCheckedTags), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => Array.isArray(value) && value.filter(_item => typeof _item !== 'string').length === 0,
                    errorMessage: 'Invalid tags (should be an array of strings)',
                })
        })
        .addComponent({
            propertyName: 'checkedTags',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialCheckedTags), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => Array.isArray(value) && value.filter(_item => typeof _item !== 'string').length === 0,
                    errorMessage: 'Invalid checked tags (should be an array of strings)',
                })
                .addValidation({
                    predicate: (value) => value.filter(_item => form.components.tags.value.includes(_item)).length === value.length,
                    errorMessage: 'Invalid checked tags (should only contain valid tags)',
                })
        })
        // whether the pct change is disabled (null) minimising ('min') or maximising ('max')
        // only relevant if isLastDaysEnabled is true
        .addComponent({
            propertyName: 'pctEvolution',
            formComponent: FormComponent
                .make({ valueState: React.useState(initialPctEvolution), errorState: React.useState('') })
                .addValidation({
                    predicate: (value) => value === 'none' || value === 'mini' || value === 'maxi',
                    errorMessage: 'Invalid percentage evolution constant',
                })
        })
    ;
    
    /* Action methods */

    // using a flag because the doSaveModification uses state values and can't be called within an EventListener callback
    const [mustSaveKeybindingFlag, setMustSaveKeybindingFlag] = React.useState(false);
    React.useEffect(() => { mustSaveKeybindingFlag && doSaveModifications(); }, [mustSaveKeybindingFlag]);

    function doResetModifications() {
        form.reset();
        onCancel();
    }
    
    // Saves a new counter or updates an existing counter's settings
    async function doSaveModifications() {
        setMustSaveKeybindingFlag(false);
        setErrorResetFlag(!errorResetFlag);
        await form.validateAsync();
        if (form.isValid) {
            const counterSettings = {
                title: form.components.counterTitle.value,
                step: form.components.increment.value,
                tags: form.components.tags.value,
                new_day_default_value: form.components.newDaysValue.value,
                starts_with_last_count: form.components.isNewDaysValueYesterdays.value,
                decimal_precision: form.components.decimalPrecision.value,
                is_history_enabled: form.components.isHistoryEnabled.value,
                evolution_percent_objective: form.components.pctEvolution.value,
            };
            const result = await onSubmit({ counterSettings: counterSettings });
            if (!result) {
                console.warn(`something went wrong, the modifications could not be saved`);
            }
        }
    }
    
    /* Constants from state values */
    const unexpectedErrors =
        [
            form.components.count.error,
            form.components.selectedColour.error,
            form.components.decimalPrecision.error,
            form.components.pctEvolution.error,
            form.components.tags.error,
        ]
        .filter(x => Boolean(x));

    return (<>
        <CounterLayout
            onReset={doResetModifications}
            onSubmit={() => { setMustSaveKeybindingFlag(true); }}
            onOpenSettings={onOpenSettings}
            errorResetFlag={errorResetFlag}
            isModification={areSettingsOpen}
            unexpectedErrors={unexpectedErrors}
            legendTitle={form.components.counterTitle.value}
            setLegendTitle={form.components.counterTitle.setValue}
            legendTitleError={form.components.counterTitle.error}
            currentCount={form.components.count.value}
            setCurrentCount={form.components.count.setValue}
            tags={form.components.tags.value}
            checkedTags={form.components.checkedTags.value}
            setTags={form.components.tags.setValue}
            setCheckedTags={form.components.checkedTags.setValue}
            selectedColour={form.components.selectedColour.value}
            setSelectedColour={form.components.selectedColour.setValue}
            newDaysValue={form.components.newDaysValue.value}
            setNewDaysValue={form.components.newDaysValue.setValue}
            newDaysValueError={form.components.newDaysValue.error}
            isNewDaysValueYesterdays={form.components.isNewDaysValueYesterdays.value}
            setIsNewDaysValueYesterdays={form.components.isNewDaysValueYesterdays.setValue}
            increment={form.components.increment.value}
            setIncrement={form.components.increment.setValue}
            incrementError={form.components.increment.error}
            decimalPrecision={form.components.decimalPrecision.value}
            setDecimalPrecision={form.components.decimalPrecision.setValue}
            isHistoryEnabled={form.components.isHistoryEnabled.value}
            setIsHistoryEnabled={form.components.isHistoryEnabled.setValue}
            pctEvolution={form.components.pctEvolution.value}
            setPctEvolution={form.components.pctEvolution.setValue}
        />
    </>);
}
