// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Breakpoints from 'front/Breakpoints';
// project components
import TileLegendSection from './counter_layout/TileLegendSection';
import CurrentCountSection from './counter_layout/CurrentCountSection';
import HistorySection from './counter_layout/HistorySection';
import TagsSection from './counter_layout/TagsSection';
import ActionIconsSection from './counter_layout/ActionIconsSection';
import UnexpectedErrorsSection from './counter_layout/UnexpectedErrorsSection';
import SettingsSection from './counter_layout/SettingsSection';
import BottomActionsSection from './counter_layout/BottomActionsSection';

export default function CounterLayout({
    onReset,
    onSubmit,
    onOpenSettings,
    errorResetFlag,
    isModification,
    unexpectedErrors,
    legendTitle,
    setLegendTitle,
    legendTitleError,
    currentCount,
    setCurrentCount,
    tags,
    checkedTags,
    setTags,
    setCheckedTags,
    selectedColour,
    setSelectedColour,
    newDaysValue,
    setNewDaysValue,
    newDaysValueError,
    isNewDaysValueYesterdays,
    setIsNewDaysValueYesterdays,
    increment,
    setIncrement,
    incrementError,
    decimalPrecision,
    setDecimalPrecision,
    isHistoryEnabled,
    setIsHistoryEnabled,
    pctEvolution,
    setPctEvolution,
}) {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);

    const isTinyDisplay = breakpoint < Breakpoints.xs;

    // https://css-tricks.com/snippets/css/complete-guide-grid/
    // https://grid.layoutit.com/
    const areTagsRelevant = isModification || Boolean(tags?.length);
    const counterGridTemplate = isTinyDisplay
        ? {
            gap: areTagsRelevant ? '8px 12px' : '0px 12px', // ugly way to avoid 8px after the first row if there are no tags
            gridTemplateColumns: 'max-content 1fr max-content max-content',
            gridTemplateRows: 'max-content max-content',
            gridTemplateAreas: ` 
                "sub value add actions"
                "tags tags history history"
            `,
        }
        : {
            gap: areTagsRelevant ? '8px 12px' : '0px 12px', // ugly way to avoid 8px after the first row if there are no tags
            gridTemplateColumns: 'max-content 1fr max-content max-content max-content',
            gridTemplateRows: 'max-content max-content',
            gridTemplateAreas: ` 
                "sub value add history actions"
                "tags tags tags tags tags"
            `,
        };
    
    /* Keyboard shortcuts (esc, ctrl+s, etc.) */
    function doHandleKeyDown(event) {
        const charCode = String.fromCharCode(event.which).toLowerCase();
        const mustSave = isModification && (event.ctrlKey || event.metaKey) && charCode === 's';
        const mustReset = isModification && ['esc', 'escape'].includes(event.key.toLowerCase());
        if (mustSave) {
            // prevent default must be within the 'if' else every keydown is cancelled and can't bubble
            event.stopPropagation();
            event.preventDefault();
            onSubmit();
        } else if (mustReset) {
            // prevent default must be within the 'if' else every keydown is cancelled and can't bubble
            event.stopPropagation();
            event.preventDefault(); 
            // no need for a flag since this function does not need state values
            onReset(); 
        }
    }
    function registerKeyDownEventHandlerEffect() {
        // Note that colliding keybindings won't work in nested components if this setCapture is not false
        window.addEventListener('keydown', doHandleKeyDown, false);
        // Note that this symtax removes any previous event listener when useEffect's isModification condition changes
        return () => window.removeEventListener('keydown', doHandleKeyDown, false); // won't work with onKeyDowns if setCapture is not false
    }
    // isModification is used within the callback hence is a necessary dependency
    React.useEffect(registerKeyDownEventHandlerEffect, [isModification]);

    return (<>
        <fieldset style={{
            transition: '400ms',
            boxSizing: 'border-box',
            borderRadius: '1px',
            // From the console: Warning: Updating a style property during rerender (border)
            // when a conflicting property is set(borderLeft) can lead to styling bugs.
            // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
            // instead, replace the shorthand with separate values.
            borderTop: `1px solid ${css.colour.border}`,
            borderRight: `1px solid ${css.colour.border}`,
            borderBottom: `1px solid ${css.colour.border}`,
            borderLeft: `8px solid ${selectedColour ?? css.colour.border}`,
            color: css.colour.text.regular,
            padding: '8px',
            marginTop: '8px',
            width: isTinyDisplay ? '100%' : 'fit-content',
            maxWidth: isTinyDisplay ? '100%' : '430px', // a max width is necessary for flex elements
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            cursor: 'default', // avoid cursor: text on pure html text
        }} >
            <TileLegendSection
                errorResetFlag={errorResetFlag}
                legendTitle={legendTitle}
                setLegendTitle={setLegendTitle}
                legendTitleError={legendTitleError}
                isModification={isModification}
            />

            <UnexpectedErrorsSection
                unexpectedErrors={unexpectedErrors}
            />

            <div style={{
                display: 'grid', 
                ...counterGridTemplate,
            }}>
                <CurrentCountSection
                    currentCount={currentCount}
                    setCurrentCount={setCurrentCount}
                    decimalPrecision={decimalPrecision}
                    increment={increment}
                />
                <HistorySection
                    visible={isHistoryEnabled}
                    currentCount={currentCount}
                    decimalPrecision={decimalPrecision}
                    pctEvolution={pctEvolution}
                    style={{ gridArea: 'history' }}
                />
                <TagsSection
                    isModification={isModification}
                    tags={tags}
                    checkedTags={checkedTags}
                    setTags={setTags}
                    setCheckedTags={setCheckedTags}
                    style={{ gridArea: 'tags' }}
                />
                <ActionIconsSection
                    isModification={isModification}
                    onReset={onReset}
                    onOpenSettings={onOpenSettings}
                    selectedColour={selectedColour}
                    setSelectedColour={setSelectedColour}
                    style={{ gridArea: 'actions' }}
                />
            </div>
            <SettingsSection
                errorResetFlag={errorResetFlag}
                isModification={isModification}
                newDaysValue={newDaysValue}
                setNewDaysValue={setNewDaysValue}
                newDaysValueError={newDaysValueError}
                isNewDaysValueYesterdays={isNewDaysValueYesterdays}
                setIsNewDaysValueYesterdays={setIsNewDaysValueYesterdays}
                increment={increment}
                setIncrement={setIncrement}
                incrementError={incrementError}
                decimalPrecision={decimalPrecision}
                setDecimalPrecision={setDecimalPrecision}
                isLastDaysEnabled={isHistoryEnabled}
                setIsLastDaysEnabled={setIsHistoryEnabled}
                pctEvolution={pctEvolution}
                setPctEvolution={setPctEvolution}
            />
            <BottomActionsSection
                isModification={isModification}
                onSubmit={onSubmit}
            />
        </fieldset>
    </>);
}
