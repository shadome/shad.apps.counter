// libraries
import * as React from 'react';
// project dependencies
import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';
import Tooltip from 'front/components/Tooltip';

const colours = [
    'success',
    'warning',
    'error',
];

export default function ActionIconsSection({
    selectedColour,
    setSelectedColour,
    isModification,
    onOpenSettings,
    onReset,
    ...props
 }) {
    const css = React.useContext(Contexts.CssContext);

    const changeColourButtonRef = React.useRef(null);
    const [isColourTooltipOpen, setIsColourTooltipOpen] = React.useState(false);

    return (<>
        <Tooltip
            open={isColourTooltipOpen}
            onClose={() => { setIsColourTooltipOpen(false); }}
            triggerComponentRef={changeColourButtonRef}
            anchor="left"
            style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '4px',
                padding: '4px',
            }}
        >
            {colours.map((_colour, _i) => 
                <button
                    key={_i}
                    onClick={evt => setSelectedColour(selectedColour === _colour ? null : css.colour.aspect[_colour].average)}
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        border: `1px solid ${css.colour.border}`,
                        borderRadius: '4px',
                        backgroundColor: css.colour.aspect[_colour]?.average,
                        width: '40px',
                        height: '40px',
                        color: css.colour.text.onDarkSurface,
                        cursor: 'pointer',
                    }}
                >
                    {selectedColour === _colour &&
                        <Icons.Checkmark style={{
                            padding: '2px',
                            height: '30px',
                            width: '30px',
                        }} />
                    }
                </button>
            )}
        </Tooltip>
        <div style={{
            gridArea: 'actions',
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
        }}>

            {isModification ?
                <button
                    title="Cancel"
                    children={<Icons.Cross />}
                    onClick={onReset}
                    style={{
                        padding: '2px',
                        width: '30px',
                        height: '30px',
                        border: `1px solid ${css.colour.border}`,
                        cursor: 'pointer',
                        color: css.colour.text.onDarkSurface,
                        backgroundColor: css.colour.aspect.error.average,
                    }}
                />
                :

                <button
                    title="Edit"
                    children={<Icons.Edit />}
                    className="hov-bg"
                    onClick={onOpenSettings}
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: '2px',
                        width: '30px',
                        height: '30px',
                        border: `1px solid ${css.colour.border}`,
                        cursor: 'pointer',
                        justifySelf: 'center',
                        alignSelf: 'center',
                        color: css.colour.text.regular,
                    }}
                />
            }
            <button
                title="Set colour"
                children={<Icons.Palette />}
                className="hov-bg"
                ref={changeColourButtonRef}
                onClick={() => { setIsColourTooltipOpen(true); }}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: '2px',
                    width: '30px',
                    height: '30px',
                    border: `1px solid ${css.colour.border}`,
                    cursor: 'pointer',
                    justifySelf: 'center',
                    alignSelf: 'center',
                    color: css.colour.text.regular,
                }}
            />
        </div>
    </>);
}
