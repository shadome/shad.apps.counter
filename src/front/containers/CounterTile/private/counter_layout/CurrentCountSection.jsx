// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
import * as Breakpoints from 'front/Breakpoints';

export default function CurrentCountSection({ 
    currentCount,
    setCurrentCount,
    increment,
    decimalPrecision,
 }) {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);
    const isTinyDisplay = breakpoint < Breakpoints.xs;

    return (<>
        <button
            children={<Icons.Minus />}
            onClick={() => { setCurrentCount(Number(currentCount - increment)); }}
            className="hov-bg"
            style={{
                gridArea: 'sub',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '50px',
                height: '50px',
                border: `1px solid ${css.colour.border}`,
                cursor: 'pointer',
                justifySelf: 'center',
                alignSelf: 'center',
            }}
        />
        <span style={{
            gridArea: 'value',
            fontSize: '42px',
            fontWeight: 'bolder',
            color: css.colour.text.strong,
            overflow: 'hidden',
            justifySelf: 'center',
            alignSelf: 'center',
            width: isTinyDisplay ? '100%' : '140px',
            textAlign: 'center',
        }}>
            {currentCount.toFixed(decimalPrecision)}
        </span>
        <button
            children={<Icons.Plus />}
            onClick={() => { setCurrentCount(Number(currentCount + increment)); }}
            className="hov-bg"
            style={{
                gridArea:'add',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '50px',
                height: '50px',
                border: `1px solid ${css.colour.border}`,
                cursor: 'pointer',
                justifySelf: 'center',
                alignSelf: 'center',
            }}
        />
    </>);
}
