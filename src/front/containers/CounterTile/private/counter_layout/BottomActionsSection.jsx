// libraries
import * as React from 'react';
// project dependencies
import * as Icons from 'front/components/Icons';
import * as CssInterop from 'front/CssInterop';
import * as Contexts from 'context';

export default function BottomActionsSection({ 
    isModification,
    onSubmit,
 }) {
    const css = React.useContext(Contexts.CssContext);

    return Boolean(isModification) && (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'right',
            padding: '8px 16px',
        }}>
            <button
                className="hov-bg"
                onClick={onSubmit}
                style={{
                    color: css.colour.text.soft,
                    cursor: 'pointer',
                    padding: '6px 10px',
                    border: `1px solid ${css.colour.text.soft}`,
                    borderRadius: '1px',
                    // text
                    textTransform: 'uppercase',
                    fontSize: css.size.font.sm,
                    fontWeight: 'bolder',
                    // button as a container
                    display: 'inline-flex',
                    alignItems: 'center',
                    gap: '6px',
                }}
            >
                <Icons.Checkmark style={{
                    // refacto: same as text font size
                    width: `${CssInterop.getNumberFromCss(css.size.font.sm) * css.size.lineHeight}px`,
                    height: `${CssInterop.getNumberFromCss(css.size.font.sm) * css.size.lineHeight}px`,
                }}/>
                <span>Save and close</span>
            </button>
        </div>
    </>);
}
