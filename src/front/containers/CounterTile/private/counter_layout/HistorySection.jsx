// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

/**
 * @returns The variation in percent from number 1 to number 2,
 * e.g., 10 and 2.5 would return -75, 5 and 15 would return +200,
 * or undefined if those are not numbers or if number1 is 0.
 */
function GetPctDiff(number1, number2) {
    if (isNaN(number1) || isNaN(number2) || Number(number1) === 0) {
        return undefined;
    }
    const nb1 = Number(number1);
    const nb2 = Number(number2);
    return (nb2 - nb1) / Math.abs(nb1) * 100;
}

export default function HistorySection({
    visible,
    currentCount,
    decimalPrecision,
    pctEvolution,
    ...props
 }) {
    const css = React.useContext(Contexts.CssContext);

    return Boolean(visible) && (<>
        <div style={{
            ...props.style,
            minWidth: '86px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'start',
            gap: '4px',
        }}>
            <span style={{
                fontSize: '10px',
                fontWeight: 'bold',
                color: css.colour.text.soft,
                textTransform: 'uppercase',
            }}>
                Last days
            </span>
            <div style={{
                display: 'grid',
                rowGap: '2px',
                columnGap: '4px',
                gridTemplateColumns: 'auto auto',
                gridTemplateAreas: `
                    "lastval0 pct0"
                    "lastval1 pct1"
                `,
                alignItems: 'center',
            }}>
                {[118.91, 119].map((_value, _i) =>
                    <span
                        key={_i}
                        style={{
                            gridArea: `lastval${_i}`,
                            fontSize: '14px',
                            color: css.colour.text.regular,
                        }}
                    >
                        {_value.toFixed(decimalPrecision)}
                    </span>
                )}
                {pctEvolution && [118.91, 119].map((_value, _i) =>
                    <span
                        key={_i}
                        style={{
                            gridArea: `pct${_i}`,
                            color: currentCount < _value ^ pctEvolution === 'max'
                                ? css.colour.aspect.success.foreground
                                : css.colour.aspect.error.foreground,
                            fontSize: '10px',
                            fontWeight: 'bold',
                            width: '40px',
                        }}
                    >
                        {currentCount >= _value && '+'}{GetPctDiff(_value, currentCount).toFixed(decimalPrecision)}%
                    </span>
                )}
            </div>
        </div>
    </>);
}
