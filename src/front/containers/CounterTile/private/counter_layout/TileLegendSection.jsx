// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import TooltipedErrorAppendix from 'front/components/TooltipedErrorAppendix';

export default function TileLegendSection({
    errorResetFlag,
    legendTitle,
    setLegendTitle,
    legendTitleError,
    isModification,
}) {
    const css = React.useContext(Contexts.CssContext);

    const hasError = Boolean(legendTitleError);
    const legendTitleInputRef = React.useRef(null);
    const legendTextZoneWidth = '250px';

    return (<>
        <legend style={{
            padding: '0px 8px 0px 8px',
            fontSize: css.size.font.sm,
            fontWeight: '600',
            color: css.colour.text.soft,
            maxHeight: '0px',
            margin: '0px',
            overflowY: 'visible',
            display: 'inline-flex',
            alignItems: 'center',
        }}>
        
            {isModification ?
                <input
                    ref={legendTitleInputRef}
                    type="text"
                    placeholder="Set the title of the counter"
                    className={hasError ? 'focusable placeholder-error' : 'focusable placeholder-italic'}
                    value={legendTitle}
                    onChange={(evt) => { setLegendTitle(evt.target.value); }}
                    style={{
                        borderRadius: '1px',
                        marginTop: '4px',
                        background: 'none',
                        width: legendTextZoneWidth,
                        padding: '0px 20px 0px 0px',
                        fontSize: css.size.font.sm,
                        margin: '0px',
                        color: hasError ? css.colour.aspect.error.average : 'inherit',
                    }}
                />
                :
                <span style={{
                    display: 'inline-block',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    maxWidth: legendTextZoneWidth,
                }}>
                    {legendTitle}
                </span>
            }
            <TooltipedErrorAppendix
                errorResetFlag={errorResetFlag}
                withinInput
                tooltipAnchor="left"
                gap={30}
                errorText={legendTitleError}
                inputRef={legendTitleInputRef}
                tooltipStyle={{
                    minWidth: '200px',
                    maxWidth: '200px',
                    whiteSpace: 'default',
                }}
            />
        </legend>
    </>);
}
