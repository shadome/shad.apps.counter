// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

export default function UnexpectedErrorsSection({
    unexpectedErrors,
    ...props
 }) {
    const css = React.useContext(Contexts.CssContext);

    return unexpectedErrors.length > 0 && (<>
        <div style={{
            border: `1px solid ${css.colour.aspect.error.foreground}`,
            borderRadius: '1px',
            color: css.colour.aspect.error.foreground,
            backgroundColor: css.colour.aspect.error.background,
            margin: '8px 0px',
            padding: '4px 8px',
        }}>
            <span style={{ fontWeight: '600' }}>
                Unexpected errors occured
            </span>
            <ul style={{
                display: 'block',
                listStyleType: 'disc',
            }}>
                {unexpectedErrors.map(_error => 
                    <li
                        key={_error}
                        style={{
                            display: 'list-item',
                            listStyleType: 'square',
                            marginLeft: '20px',
                        }}
                    >
                        {_error}
                    </li>
                )}
            </ul>
        </div>
    </>);
}
