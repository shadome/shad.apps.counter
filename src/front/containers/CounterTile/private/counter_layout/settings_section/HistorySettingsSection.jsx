// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
import * as Breakpoints from 'front/Breakpoints';
import Tooltip from 'front/components/Tooltip';
import Toggle from 'front/components/Toggle';

const GridAreas = {
    title: 'hist_title',
    historyFullWidth: 'hist_fullw',
    evolutionValueLabel: 'evol_label',
    evolutionValueInput: 'evol_value',
};
export const GridTemplateAreaRows_TwoColumns = `
    "${GridAreas.title}                ${GridAreas.title}"
    "${GridAreas.historyFullWidth}     ${GridAreas.historyFullWidth}"
    "${GridAreas.evolutionValueLabel}  ${GridAreas.evolutionValueInput}"
`;

export default function HistorySettingsSection({
    isLastDaysEnabled,
    setIsLastDaysEnabled,
    pctEvolution,
    setPctEvolution,
 }) {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);

    const isTinyDisplay = breakpoint < Breakpoints.xs;

    const pctEvolutionHelpRef = React.useRef(null);
    const [isPctEvolutionHelpOpen, setIsPctEvolutionHelpOpen] = React.useState(false);

    return (<>
        <span style={{
            gridArea: GridAreas.title,
            margin: '4px 0px 0px 12px',
            fontSize: '10px',
            fontWeight: 'bold',
            color: css.colour.text.regular,
            textTransform: 'uppercase',
        }}>
            History settings
        </span>
        <div style={{
            gridArea: GridAreas.historyFullWidth,
            display: 'inline-flex',
            flexDirection: 'row',
            alignItems: 'center',
            gap: '8px',
        }}>
            <Toggle
                id="isLastDaysEnabled"
                isChecked={isLastDaysEnabled}
                onToggle={(newValue) => { setIsLastDaysEnabled(newValue); !newValue && setPctEvolution(null); }}
            />
            <label
                htmlFor="isLastDaysEnabled"
                style={{
                    color: css.colour.text.soft,
                    fontSize: isTinyDisplay ? css.size.font.sm : css.size.font.md,
                    whiteSpace: 'nowrap',
                    display: 'inline-flex',
                    cursor: 'pointer',
                }}
            >
                Enable last days history
            </label>
        </div>
        <div style={{
            gridArea: GridAreas.evolutionValueLabel,
            display: 'inline-flex',
        }}>
            <span style={{
                color: css.colour.text.soft,
                fontSize: isTinyDisplay ? css.size.font.sm : css.size.font.md,
                whiteSpace: 'nowrap',
            }}>
                Evolution percent
            </span>
            <button
                ref={pctEvolutionHelpRef}
                children={<Icons.Help className="hov-bg" />}
                onClick={() => { setIsPctEvolutionHelpOpen(true); }}
                style={{
                    width: '24px',
                    height: '24px',
                    marginLeft: '4px',
                    cursor: 'pointer',
                    color: isPctEvolutionHelpOpen
                        ? css.colour.aspect.info.average
                        : css.colour.text.disabled,
                }}
            />
            <Tooltip
                open={isPctEvolutionHelpOpen}
                onClose={() => { setIsPctEvolutionHelpOpen(false); }}
                triggerComponentRef={pctEvolutionHelpRef}
                anchor="top"
                style={{ width: '200px', maxWidth: '100%' }}
                children="Whether this counter's objective is to maximise or minimise the value"
            />
        </div>
        <div style={{
            gridArea: GridAreas.evolutionValueInput,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifySelf: 'center',
            gap: '0px',
        }}>
            <button
                disabled={!isLastDaysEnabled ? 'disabled' : undefined}
                className={isLastDaysEnabled ? 'hov-bg' : undefined}
                onClick={() => { setPctEvolution('none'); }}
                style={{
                    cursor: !isLastDaysEnabled || pctEvolution === 'none' ? 'default' : 'pointer',
                    height: '32px',
                    width: '48px',
                    padding: '6px',
                    backgroundColor: isLastDaysEnabled && !pctEvolution && css.overlay.selected,
                    // From the console: Warning: Updating a style property during rerender (border)
                    // when a conflicting property is set(borderLeft) can lead to styling bugs.
                    // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
                    // instead, replace the shorthand with separate values.
                    borderTop: `1px solid ${css.colour.border}`,
                    borderBottom: `1px solid ${css.colour.border}`,
                    borderLeft: `1px solid ${css.colour.border}`,
                    borderRight: `none`,
                    borderTopLeftRadius: '1px',
                    borderBottomLeftRadius: '1px',
                    // text
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    fontSize: css.size.font.xs,
                    fontWeight: 'bolder',
                    color: !isLastDaysEnabled && css.colour.text.disabled,
                }}
            >
                None
            </button>
            <button
                disabled={!isLastDaysEnabled ? 'disabled' : undefined}
                className={isLastDaysEnabled ? 'hov-bg' : undefined}
                onClick={() => { setPctEvolution('mini'); }}
                style={{
                    cursor: !isLastDaysEnabled || pctEvolution === 'mini' ? 'default' : 'pointer',
                    height: '32px',
                    width: '48px',
                    padding: '6px',
                    backgroundColor: isLastDaysEnabled && pctEvolution === 'mini' && css.overlay.selected,
                    border: `1px solid ${css.colour.border}`,
                    // text
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    fontSize: css.size.font.xs,
                    fontWeight: 'bolder',
                    color: !isLastDaysEnabled && css.colour.text.disabled,
                }}
            >
                Mini
            </button>
            <button
                disabled={!isLastDaysEnabled ? 'disabled' : undefined}
                className={isLastDaysEnabled ? 'hov-bg' : undefined}
                onClick={() => { setPctEvolution('maxi'); }}
                style={{
                    cursor: !isLastDaysEnabled || pctEvolution === 'maxi' ? 'default' : 'pointer',
                    height: '32px',
                    width: '48px',
                    padding: '6px',
                    backgroundColor: isLastDaysEnabled && pctEvolution === 'maxi' && css.overlay.selected,
                    // From the console: Warning: Updating a style property during rerender (border)
                    // when a conflicting property is set(borderLeft) can lead to styling bugs.
                    // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
                    // instead, replace the shorthand with separate values.
                    borderTop: `1px solid ${css.colour.border}`,
                    borderBottom: `1px solid ${css.colour.border}`,
                    borderRight: `1px solid ${css.colour.border}`,
                    borderLeft: `none`,
                    borderTopRightRadius: '1px',
                    borderBottomRightRadius: '1px',
                    // text
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    fontSize: css.size.font.xs,
                    fontWeight: 'bolder',
                    color: !isLastDaysEnabled && css.colour.text.disabled,
                }}
            >
                Maxi
            </button>
        </div>
    </>);
}
