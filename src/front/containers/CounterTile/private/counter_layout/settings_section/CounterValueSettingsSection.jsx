// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
import * as Breakpoints from 'front/Breakpoints';
import TooltipedErrorAppendix from 'front/components/TooltipedErrorAppendix';

export const DecimalPrecisionMinValue = 0;
export const DecimalPrecisionMaxValue = 2;

const GridAreas = {
    title: 'cntr_title',
    stepValueLabel: 'step_label',
    stepValueInput: 'step_value',
    decimalValueLabel: 'deci_label',
    decimalValueInput: 'deci_value',
};
export const GridTemplateAreaRows_TwoColumns = `
    "${GridAreas.title}              ${GridAreas.title}"
    "${GridAreas.stepValueLabel}     ${GridAreas.stepValueInput}"
    "${GridAreas.decimalValueLabel}  ${GridAreas.decimalValueInput}"
`;

export default function CounterValueSettingsSection({
    errorResetFlag,
    increment,
    setIncrement,
    incrementError,
    decimalPrecision,
    setDecimalPrecision,
 }) {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);

    const isTinyDisplay = breakpoint < Breakpoints.xs;

    return (<>
        <span style={{
            gridArea: GridAreas.title,
            margin: '4px 0px 0px 12px',
            fontSize: '10px',
            fontWeight: 'bold',
            color: css.colour.text.regular,
            textTransform: 'uppercase',
        }}>
            Counter value settings
        </span>
        <span style={{
            gridArea: GridAreas.stepValueLabel,
            color: css.colour.text.soft,
            fontSize: isTinyDisplay ? css.size.font.sm : css.size.font.md,
            whiteSpace: 'nowrap',
        }}>
            Plus / minus step
        </span>
        <div style={{
            gridArea: GridAreas.stepValueInput,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifySelf: 'center',
        }}>
            <input
                className="focusable"
                // Note: type number makes the onChange.evt.target.value always be a number, 0 by default, thus hinders the error management
                value={increment}
                onChange={(evt) => { setIncrement(evt.target.value); }}
                style={{
                    borderRadius: '1px',
                    marginTop: '4px',
                    background: 'none',
                    width: '96px',
                    padding: '4px 8px',
                    fontSize: css.size.font.sm,
                    // hide arrows from the type=number input
                    // appearance: 'textfield',
                    // MozAppearance: 'textfield',
                    // WebkitAppearance: 'textfield',
                    margin: '0px',
                    // the border is handled without the shorthand to be compatible with the 'focusable' class
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: Boolean(incrementError)
                        ? css.colour.aspect.error.average
                        : css.colour.border,
                }}
            />
            <TooltipedErrorAppendix
                gap={20}
                errorResetFlag={errorResetFlag}
                errorText={incrementError}
                style={{ marginLeft: '4px' }}
                tooltipStyle={{
                    minWidth: '200px',
                    maxWidth: '200px',
                    whiteSpace: 'default',
                }}
            />
        </div>
        <span style={{
            gridArea: GridAreas.decimalValueLabel,
            color: css.colour.text.soft,
            fontSize: isTinyDisplay ? css.size.font.sm : css.size.font.md,
            whiteSpace: 'nowrap',
        }}>
            Decimal precision
        </span>
        <div style={{
            gridArea: GridAreas.decimalValueInput,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifySelf: 'center',
            gap: '4px',
        }}>
            <button
                onClick={() => { setDecimalPrecision(Math.max(DecimalPrecisionMinValue, decimalPrecision - 1)); }}
                children={<Icons.Minus />}
                className={decimalPrecision > DecimalPrecisionMinValue ? 'hov-bg' : undefined}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '30px',
                    height: '30px',
                    border: `1px solid ${css.colour.border}`,
                    cursor: decimalPrecision > DecimalPrecisionMinValue ? 'pointer' : 'default',
                    color: decimalPrecision > DecimalPrecisionMinValue ? 'inherit' : css.colour.text.disabled,
                }}
            />
            <span style={{ width: '28px', textAlign: 'center' }}>
                {decimalPrecision}
            </span>
            <button
                onClick={() => { setDecimalPrecision(Math.min(DecimalPrecisionMaxValue, decimalPrecision + 1)); }}
                children={<Icons.Plus />}
                className={decimalPrecision < DecimalPrecisionMaxValue ? 'hov-bg' : undefined}
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '30px',
                    height: '30px',
                    border: `1px solid ${css.colour.border}`,
                    cursor: decimalPrecision < DecimalPrecisionMaxValue ? 'pointer' : 'default',
                    color: decimalPrecision < DecimalPrecisionMaxValue ? 'inherit' : css.colour.text.disabled,
                }}
            />
        </div>
    </>);
}
