// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
import * as Breakpoints from 'front/Breakpoints';
import TooltipedErrorAppendix from 'front/components/TooltipedErrorAppendix';
import Tooltip from 'front/components/Tooltip';
import Toggle from 'front/components/Toggle';

const GridAreas = {
    title: 'init_title',
    newDaysValueLabel: 'init_label',
    newDaysValueInput: 'init_value',
    startWithLastCount: 'init_sbrow',
};
export const GridTemplateAreaRows_TwoColumns = `
    "${GridAreas.title}                 ${GridAreas.title}"
    "${GridAreas.newDaysValueLabel}     ${GridAreas.newDaysValueInput}"
    "${GridAreas.startWithLastCount}    ${GridAreas.startWithLastCount}"
`;

export default function InitialCounterValueSection({
    errorResetFlag,
    newDaysValue,
    setNewDaysValue,
    newDaysValueError,
    isNewDaysValueYesterdays,
    setIsNewDaysValueYesterdays,
 }) {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    const css = React.useContext(Contexts.CssContext);

    const isTinyDisplay = breakpoint < Breakpoints.xs;

    const newDaysValueToYesterdaysHelpRef = React.useRef(null);
    const [isNewDaysValueToYesterdaysHelpOpen, setIsNewDaysValueToYesterdaysHelpOpen] = React.useState(false);

    return (<>
        <span style={{
            gridArea: GridAreas.title,
            margin: '4px 0px 0px 12px',
            fontSize: css.size.font.tn,
            fontWeight: 'bold',
            color: css.colour.text.regular,
            textTransform: 'uppercase',
        }}>
            Initial counter value
        </span>
        <span style={{
            gridArea: GridAreas.newDaysValueLabel,
            color: css.colour.text.soft,
            fontSize: css.size.font.md,
            whiteSpace: 'nowrap',
        }}>
            New day's value
        </span>
        <div style={{
            gridArea: GridAreas.newDaysValueInput,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifySelf: 'center',
        }}>
            <input
                className="focusable"
                // Note: type number makes the onChange.evt.target.value always be a number, 0 by default, thus hinders the error management
                value={newDaysValue}
                onChange={(evt) => setNewDaysValue(evt.target.value)}
                style={{
                    borderRadius: '1px',
                    marginTop: '4px',
                    background: 'none',
                    width: '96px',
                    padding: '4px 8px',
                    fontSize: css.size.font.sm,
                    margin: '0px',
                    // the border is handled without the shorthand to be compatible with the 'focusable' class
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: Boolean(newDaysValueError)
                        ? css.colour.aspect.error.average
                        : css.colour.border,
                }}
            />
            <TooltipedErrorAppendix
                gap={20}
                errorResetFlag={errorResetFlag}
                errorText={newDaysValueError}
                style={{ marginLeft: '4px' }}
            />
        </div>
        <div style={{
            gridArea: GridAreas.startWithLastCount,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            gap: '8px',
        }}>
            <Toggle
                id="isNewDaysValueYesterdays"
                isChecked={isNewDaysValueYesterdays}
                onToggle={(newValue) => { setIsNewDaysValueYesterdays(newValue); }}
            />
            <label
                htmlFor="isNewDaysValueYesterdays"
                style={{
                    color: css.colour.text.soft,
                    fontSize: isTinyDisplay ? css.size.font.sm : css.size.font.md,
                    cursor: 'pointer',
                }}
            >
                Start with last count
            </label>
            <button
                ref={newDaysValueToYesterdaysHelpRef}
                children={<Icons.Help className="hov-bg" />}
                onClick={() => { setIsNewDaysValueToYesterdaysHelpOpen(true); }}
                style={{
                    width: '24px',
                    height: '24px',
                    cursor: 'pointer',
                    color: isNewDaysValueToYesterdaysHelpOpen
                        ? css.colour.aspect.info.average
                        : css.colour.text.disabled,
                }}
            />
            <Tooltip
                open={isNewDaysValueToYesterdaysHelpOpen}
                onClose={() => { setIsNewDaysValueToYesterdaysHelpOpen(false); }}
                triggerComponentRef={newDaysValueToYesterdaysHelpRef}
                anchor="top"
                style={{ width: '250px', maxWidth: '100%' }}
                children="On a new day, the count starts at the last meaningful day's count if any, else with the new day's value"
            />
        </div>
    </>);
}
