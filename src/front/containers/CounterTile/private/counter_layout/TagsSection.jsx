// libraries
import * as React from 'react';
// project dependencies
import * as Icons from 'front/components/Icons';
import * as Contexts from 'context';

export default function TagsSection({ 
    isModification,
    tags,
    setTags,
    checkedTags,
    setCheckedTags,
    ...props
 }) {
    const css = React.useContext(Contexts.CssContext);

    function doToggleTag(tag) {
        const idx = checkedTags.indexOf(tag);
        const newValue = idx === -1
            ? [tag, ...checkedTags]
            : [...checkedTags.filter((_, _i) => _i !== idx)];
        setCheckedTags(newValue);
    }

    // also serves as a flag: null if no tag is being added, empty string (or current value) otherwise
    const [addTagCurrentValue, setAddTagCurrentValue] = React.useState(null);
    const addTagInputRef = React.useRef(null);

    function focusAddTagInput() {
        addTagCurrentValue === '' && addTagInputRef.current.focus();
    }
    React.useEffect(focusAddTagInput, [addTagCurrentValue]);

    function doSubmitAddTagInputValue() {
        const trimmedNewTag = addTagCurrentValue?.trim();
        const doesTagAlreadyExist = tags.includes(trimmedNewTag);
        !doesTagAlreadyExist && trimmedNewTag && setTags([...tags, trimmedNewTag]);
        setAddTagCurrentValue(null);
    }

    function doProcessAddTagInputKeyUpEvent(evt) {
        if (evt.keyCode === 13) { // enter key
            // prevent default must be within the 'if' else every keydown is cancelled and can't bubble
            evt.stopPropagation();
            evt.preventDefault();
            doSubmitAddTagInputValue();
            // after hitting enter, the tag is submitted and the focus is set to a new tag input
            setAddTagCurrentValue('');
        } else if (['esc', 'escape'].includes(evt.key.toLowerCase())) {
            // prevent default must be within the 'if' else every keydown is cancelled and can't bubble
            evt.stopPropagation();
            evt.preventDefault();
            setAddTagCurrentValue(null);
        }
    }

    return Boolean(tags) && (Boolean(tags.length) || isModification) && (<>
        {/* CAREFUL ABOUT THE OMNIPRESENT isModification CHECKS WHICH DRASTICALLY CHANGES DISPLAY AND FUNCTIONALITIES */}
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            ...props.style,
        }}>
            {isModification &&
                <span style={{
                    marginBottom: '8px',
                    marginLeft: '12px',
                    fontSize: '10px',
                    fontWeight: 'bold',
                    color: css.colour.text.regular,
                    textTransform: 'uppercase',
                }}>
                    Tags settings
                </span>
            }
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: '4px',
                width: '100%',
            }}>
                {tags.map((_tag, _i) => {
                    const isTagSelected = checkedTags.indexOf(_tag) !== -1;
                    const tagBackgroundColour = isTagSelected && css.colour.border;
                    const tagBorderColour = isTagSelected ? css.colour.text.strong : css.colour.border;
                    return (
                        <button
                            key={_tag} // invariant: tags are unique
                            onClick={_ => doToggleTag(_tag)}
                            className="hov-bg"
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                backgroundColor: tagBackgroundColour,
                                color: 'inherit',
                                border: `1px solid ${tagBorderColour}`,
                                borderRadius: '1px',
                                padding: isModification
                                    ? '2px 2px 2px 8px'
                                    : '2px 8px',
                                gap: '4px',
                                cursor: 'pointer',
                                height: '30px',
                            }}
                        >
                            <span style={{
                                display: 'inline-block',
                                whiteSpace: 'nowrap',
                                textOverflow: 'ellipsis',
                                overflow: 'hidden',
                                maxWidth: '165px',
                            }}>
                                {_tag}
                            </span>
                            {isModification &&
                                <Icons.Cross
                                    onClick={() => { setTags(tags.filter(x => x !== _tag)); }}
                                    style={{ width: '1.2em', height: '1.2em' }}
                                />
                            }
                            
                        </button>
                    );
                })}
                {isModification && 
                    <>
                        {addTagCurrentValue === null ?
                            <button
                                onClick={_ => { setAddTagCurrentValue(''); }}
                                className="hov-bg"
                                style={{
                                    display: 'inline-flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    color: 'inherit',
                                    border: `1px solid ${css.colour.border}`,
                                    borderRadius: '1px',
                                    padding: '2px',
                                    cursor: 'pointer',
                                    width: '30px',
                                    height: '30px',
                                }}
                            >
                                <Icons.Plus style={{ width: '1.2em', height: '1.2em' }} />
                            </button>
                            :
                            <input
                                ref={addTagInputRef}
                                onBlur={doSubmitAddTagInputValue}
                                value={addTagCurrentValue}
                                onChange={evt => { setAddTagCurrentValue(evt.target.value); }}
                                onKeyDown={doProcessAddTagInputKeyUpEvent}
                                style={{
                                    boxSizing: 'border-box',
                                    height: '30px',
                                    width: '75px',
                                    padding: '2px',
                                    border: `1px solid ${css.colour.border}`,
                                    borderRadius: '1px',
                                }}
                            />
                        }
                    </>
                }
            </div>
        </div>
    </>);
}
