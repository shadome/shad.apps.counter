// libraries
import * as React from 'react';
// project dependencies
import InitialCounterValueSection, * as InitialCounterValueSectionExports from './settings_section/InitialCounterValueSection';
import CounterValueSettingsSection, * as CounterValueSettingsSectionExports from './settings_section/CounterValueSettingsSection';
import HistorySettingsSection, * as HistorySettingsSectionExports from './settings_section/HistorySettingsSection';

// used by this component's callers which shouldn't have to know about this component's subcomponents
export const DecimalPrecisionInitialValue = CounterValueSettingsSectionExports.DecimalPrecisionMinValue;

export default function SettingsSection({
    errorResetFlag,
    isModification,
    newDaysValue,
    setNewDaysValue,
    newDaysValueError,
    isNewDaysValueYesterdays,
    setIsNewDaysValueYesterdays,
    increment,
    setIncrement,
    incrementError,
    decimalPrecision,
    setDecimalPrecision,
    isLastDaysEnabled,
    setIsLastDaysEnabled,
    pctEvolution,
    setPctEvolution,
 }) {

    return Boolean(isModification) && (<>
        <div style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            // tricky syntax: the caller chooses the order and ensures the outer grid's columns
            // are aligned, the children expose their grid areas displayable in N columns-format
            gridTemplateAreas: `
                ${InitialCounterValueSectionExports.GridTemplateAreaRows_TwoColumns}
                ${CounterValueSettingsSectionExports.GridTemplateAreaRows_TwoColumns}
                ${HistorySettingsSectionExports.GridTemplateAreaRows_TwoColumns}
            `,
            alignItems: 'center',
            gap: '12px',
        }}>

            <InitialCounterValueSection
                errorResetFlag={errorResetFlag}
                newDaysValue={newDaysValue}
                setNewDaysValue={setNewDaysValue}
                newDaysValueError={newDaysValueError}
                isNewDaysValueYesterdays={isNewDaysValueYesterdays}
                setIsNewDaysValueYesterdays={setIsNewDaysValueYesterdays}
            />
            <CounterValueSettingsSection
                errorResetFlag={errorResetFlag}
                increment={increment}
                setIncrement={setIncrement}
                incrementError={incrementError}
                decimalPrecision={decimalPrecision}
                setDecimalPrecision={setDecimalPrecision}
            />
            <HistorySettingsSection
                isLastDaysEnabled={isLastDaysEnabled}
                setIsLastDaysEnabled={setIsLastDaysEnabled}
                pctEvolution={pctEvolution}
                setPctEvolution={setPctEvolution}
            />
        </div>
    </>);
}
