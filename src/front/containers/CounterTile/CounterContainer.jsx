// libraries
import * as React from 'react';
// project dependencies
import CounterPersistence from './private/CounterPersistence';

/* Display modalities of this component;
 * when counterId == null && counterDate != null:
 *  - date is considered null, jumps below
 * when counterId == null && counterDate == null:
 *  - The component proposes a creation tile
 * when counterId != null && counterDate == null:
 *  - The component proposes to modify settings, daily counts are displayed for a better immersion but daily controls are disabled
 * when counterId != null && counterDate != null && counterDate exists:
 *  - The component proposes to modify the count of the given day
 * when counterId != null && counterDate != null && NOT(counterDate exist):
 *  - The component proposes to start counting for the given day, every other control is disabled
 * 
 * Note: This component handles all of the above use cases, 
 * but it is still up to the caller to know whether to display 
 * this component in modals, center page, etc. with regards to 
 * its state if necessary
 */
export default function CounterContainer({
    // id of the counter to display
    // Note: set to null if a new counter must be created
    counterId = null,
    // optional date to which the counter is bound
    // Note: if no date is provided, a previous date may be searched for display purpose, but the associated record would never be updated
    // Note: if a date is provided but does not exist in the store, the user has to activate the counter for this date and a row would be created
    counterDate = null,
    // whether the tile's settings are displayed and can be modified
    // considered true when the component is under creation
    openedSettings: areSettingsOpen = false,
    // pen button callback ONLY IF SETTINGS CAN BE OPENED
    onOpenSettings = () => { },
    // on save button AND ctrl+s shortcut ONLY IF SETTINGS ARE OPEN (the database creation/update occurs whether this field is set or not)
    onSubmit = () => { },
    // big red cross button callback AND escape shortcut ONLY IF SETTINGS ARE OPEN
    onCancel = () => { },
    // other props
    match,
    history,
}) {

    /*
        Counters page:
        - create counter
            => counter must not modify any days' values
            => display a dummy value which is never saved
            => 1 DEACTIVATE DAILY CONTROLS
        - update counter
            => counter must not modify any days' values
            => 1 DEACTIVATE DAILY CONTROLS
        - delete counter
            => action button + double-check popup for definitive deletion
            => 1 DEACTIVATE DAILY CONTROLS
        - de-activate counter
            => action button for re/deactivation
            => 1 DEACTIVATE DAILY CONTROLS
        - re-activate counter
            => action button for re/deactivation
            => 2 DEACTIVATE EVERYTHING ELSE (no settings modification on a deactivated counter)
        Dashboard page:
        - start counting proposal 
            => action from the user for creating a row in the idb
            => 2 DEACTIVATE EVERYTHING ELSE
        - count
            => everything is active
    */

    // TODO the caller should use something like  that
    // Out of form stateful values (do not need validation, database saving, etc.)
    const [isModification, setIsModification] = React.useState(true);

    function doSubmit() {
        onSubmit();
        setIsModification(false);
    }

    function doCancel() {
        onCancel();
        setIsModification(false);
    }

    function doOpenSettings() {
        onOpenSettings();
        setIsModification(true);
    }
    
    return (<>
        TODO
        validation popup to not lose values on escape;<br/>
        no daily modifications version;<br/>
        bigger (and collapsible?) settings titles;<br/>
        not using input type=number makes fields display the full keyboard, find a way to use type=number and to pass down something else than 0 through onChange when invalid;<br/>
        try unset:all as a class for concerned elements / resolve whatever does not display on friend's laptop/phone;<br/>
        tab does not work: tabbed element is not visually selected (see the highligh heuristic, there are two highlight types handled by the browser, else search the web) and help links are in the tab list but toggles are not<br/>
        <CounterPersistence
            counterId={counterId}
            counterDate={counterDate}
            areSettingsOpen={isModification /* TODO */}
            onOpenSettings={doOpenSettings}
            onSubmit={doSubmit}
            onCancel={doCancel}
        />
    </>);
}
