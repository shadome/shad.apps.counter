/*

New link : setup a daily counter

A daily counter is the registration of a specific daily-based metric which can be easily 
modified in the daily tracking page.

It consists of a daily number and associated tags and colours.

For instance, one might want to count the number of coffees drunk every day,
the amount of money spent, the weight in kilograms, etc.

Any number of counters can be set concurrently.

Each counter's evolution can be reviewed on a month-basis through a calendar view
or a graph view.

(calendar view: everyday, tile in colour (first registered colour) + a dot for each colour,
display of the number for each day, and display of labels of the tags)
(also sum up the number's mean value and the count of the different tags and colours for the month)
(enable swiping left / right for switching months)

stores:
* one store for each parametered counters
   * used to add a quick-submit header in the daily tracking page
   * used to add a dedicated link towards review pages
   * define quick + and -
   * define whether to load from yesterday's value or starting value (0, etc.)
* one store for accounted values (with reference to the above store's counter through name)
   * read and written for data
*/




/* 
gitlab pages
CICD GitLab : netlify (appeler, gratuit si opensource)
hébergement : infomaniak
*/


import * as React from 'react';
// project dependencies
import * as Contexts from '../context';
import * as Breakpoints from './Breakpoints';

const cssSize = {
    lineHeight: 1.5,
    font: {
        xt: '08px', // extra tiny
        tn: '10px', // tiny
        xs: '12px',
        sm: '14px',
        md: '16px',
        lg: '18px',
        xl: '20px',
    },
    icons: {
        xt: '12px',
        tn: '15px',
        xs: '18px',
        sm: '21px',
        md: '24px',
        lg: '27px',
        xl: '30px',
    },
};
const cssModal = (breakpoint) => ({
    maxDimension: (
        breakpoint >= Breakpoints.lg ? '700px' :
        breakpoint >= Breakpoints.sm ? '400px' :
        '100%'),

});
const cssZIndexes = {
    none: '0',
    applicationBar: '1',
    modal: '2',
};

function getDarkModeCss(breakpoint) {
    return {
        size: cssSize,
        modal: cssModal(breakpoint),
        zIndexes: cssZIndexes,
        colour: {
            border: '#373b42',
            text: {
                strong: '#e0e0e0',
                regular: '#c9d1d9',
                soft: '#a0a0a0',
                disabled: '#5a5a5a',
                link: '#9af0de',
                // text colour on a "foreground-like" surface
                reverse: '#0d1117',
                onDarkSurface: '#ffffff',
            },
            background: {
                dp0: '#0d1117',
                dp1: '#161b22',
                dp2: '#21262d',
            },
            aspect: {
                neutral: {
                    average: '#21262d', // for now: background.dp2
                    background: '#21262d', // for now: background.dp2
                    foreground: '#a0a0a0', // for now: text.soft
                },
                info: {
                    // average: '#1d95d6',
                    // foreground: '#46b1b7',
                    // background: '#013a3a',
                    background: '#1b2839',
                    foreground: '#A5D7E8',
                    average: '#1C82AD',
                },
                success: {
                    average: '#009b22',
                    foreground: '#88c38b',
                    background: '#10452d',
                },
                warning: {
                    average: '#f68a1c',
                    foreground: '#f5a524',
                    foregroundSoft: '#ffe2b7',
                    background: '#581600',
                },
                // https://mdigi.tools/color-shades/#d74545 9 shades, number 3, 5, 7
                error: {
                    average: '#d22d2d',
                    foreground: '#ff7689',
                    background: '#560015',
                },
            },
        },
        overlay: {
            hovered: '#fff1',
            selected: '#fff2',
            hoveredAndSelected: '#fff3',
            none: '#fff0',
            unreachable: '#0005',
        }
    }
}

// TODO need to rework colours, same foreground and average at the moment
// TODO use colours: make a colour variable used for coloured background which will not be used in dark theme but will be used in light theme
// TODO no need to duplicate all this object, just duplicate colours and overlays (maybe put overlay in colour?)
function getLightModeCss(breakpoint) {
    return {
        size: cssSize,
        modal: cssModal(breakpoint),
        zIndexes: cssZIndexes,
        colour: {
            border: '#e0e0e0',
            text: {
                strong: '#212121',
                regular: '#424242',
                soft: '#656565',
                disabled: '#9f9f9f',
                link: '#013a3a',
                // text colour on a "foreground-like" surface
                reverse: '#ffffff',
                onDarkSurface: '#ffffff',
            },
            background: {
                // note that dpX have their X inverted compared to the dark mode: lighter is deeper (dp2 is not used)
                // it's still not great though
                dp1: '#ffffff',
                dp0: '#eeeeee',
                dp2: '#dddddd',
            },
            aspect: {
                neutral: {
                    average: '#dddddd', // for now: background.dp2
                    background: '#dddddd', // for now: background.dp2
                    foreground: '#656565', // for now: text.soft
                },
                info: {
                    average: '#1d95d6',
                    background: '#46b1b7',
                    foreground: '#1d95d6',
                },
                success: {
                    average: '#009b22',
                    background: '#88c38b',
                    foreground: '#009b22',
                },
                warning: {
                    average: '#f68a1c',
                    background: '#f5a524',
                    foreground: '#f68a1c',
                },
                // https://mdigi.tools/color-shades/#d74545 9 shades, number 3, 5, 7
                error: {
                    average: '#d22d2d',
                    background: '#ff7689',
                    foreground: '#d22d2d',
                },
            },
        },
        overlay: {
            hovered: '#0001',
            selected: '#0002',
            hoveredAndSelected: '#03',
            none: '#0000',
            unreachable: '#0005',
        }
    }
}

/// Return a size relatively bigger or smaller than the provided one.
/// Positive offset will return a bigger size, negative offset will return a smaller size, 0 returns the given size.
/// If the given size is not relevant, 'md' is returned.
export function getRelativeSize(size, offset) {
    const defaultResult = 'md';
    const orderedSizes = ['xt', 'tn', 'xs', 'sm', 'md', 'lg', 'xl'];
    if (isNaN(offset)) {
        return defaultResult;
    }
    const idx = orderedSizes.indexOf(size);
    if (idx === -1) {
        return defaultResult;
    }
    const newIdxUpperBounded = Math.min(orderedSizes.length, idx + offset);
    const newIdx = Math.max(0, newIdxUpperBounded);
    return orderedSizes[newIdx];
}

export function getNumberFromCss(str) {
    const regex = /\d+/g;
    return Number(str.match(regex).join(''));
}

export function getCss(themeMode, breakpoint) {
    const _themeMode = themeMode;
    const _breakpoint = breakpoint;
    return _themeMode === 'light'
        ? getLightModeCss(_breakpoint)
        : getDarkModeCss(_breakpoint);
}

export default function CssInteropInitialiser() {

    // TODO color fill vs. typo
    // https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors#13532993
    // answer +28 but don't use the amount itself, make a percent out of it, considering the max of RGB is 100% and the rest is < 100%

    const css = React.useContext(Contexts.CssContext);

    /* Serialises the above getCss object to pure css root variables. */
    function serialiseObjectStrings(separator, object, currentPath, currentOutput) {
        // do not mix up what is passed to recursive calls through reference (currentOutput) and what is copied (currentPath)
        for (const key in object) {
            const element = object[key];
            if (typeof element === 'string' || typeof element === 'number') {
                const elementStr = `${currentPath.concat(key.toString()).join(separator)}: ${element};`;
                currentOutput.push(elementStr);
            } else if (typeof element === 'object') {
                serialiseObjectStrings(separator, element, currentPath.concat([key.toString()]), currentOutput);
            }
        }
    }
    const separator = '-';
    let path = ['--css'];
    let cssOutput = [];
    serialiseObjectStrings(separator, css, path, cssOutput);

    return (
        <style style={{
            display: 'none',
        }}>
            {`:root { ${cssOutput.join('')} }`}
        </style>
    );
}