export const xxs = 0;
export const xs = 500;
export const sm = 600;
export const md = 900;
export const lg = 1200;
export const xl = 1536;

export const fromPx = (px) =>
    px >= xl ? xl :
    px >= lg ? lg :
    px >= md ? md :
    px >= sm ? sm :
    px >= xs ? xs :
    /* default */ xxs;