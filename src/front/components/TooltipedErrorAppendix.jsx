// libraries
import * as React from 'react';
// project dependencies
import * as Icons from './Icons';
import * as CssInterop from 'front/CssInterop';
import * as Contexts from 'context';
import Tooltip from './Tooltip';

const defaultButtonDimInPx = 24;

export default function TooltipedErrorAppendix({
    errorResetFlag, // meant to change value (whatever the actual value is) whenever errors are re-checked and tooltips should be re-opened
    errorText, // set to null if none
    inputRef,
    withinInput: isWithinInput = false,
    tooltipAnchor = "topleft",
    gap = 5,
    crossGap = 0,
    tooltipStyle,
    buttonStyle,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const [isTooltipOpen, setIsTooltipOpen] = React.useState(Boolean(errorText));

    const buttonRef = React.useRef(null);

    function reopenTooltipOnNewErrorEffect() {
        setIsTooltipOpen(Boolean(errorText));
    }
    React.useEffect(reopenTooltipOnNewErrorEffect, [errorText, errorResetFlag]);

    /* Set the error appendix's position */
    // const [errorComponentPositionStyle, seterrorComponentPositionStyle] = React.useState({});
    // function setErrorComponentPositionEffect() {
    //     seterrorComponentPositionStyle({
    //         transform: isWithinInput
    //             ? 'translate(-100%, -50%)'
    //             : 'translate(   0%, -50%)',
    //     });
    // }

    // not perfect but good enough for now, see Tooltip's similar algorithm
    // const setErrorComponentPositionEffectDep = [
    //     inputRef?.current?.offsetWidth,
    //     inputRef?.current?.offsetHeight,
    //     inputRef?.current?.offsetTop,
    //     inputRef?.current?.offsetLeft,
    // ];
    // React.useEffect(setErrorComponentPositionEffect, setErrorComponentPositionEffectDep);

    // get the width of the button as a number
    // const buttonWidth = buttonStyle?.width ? CssInterop.getNumberFromCss(buttonStyle.width) : defaultButtonDimInPx;
    // get the height of the button as a number
    // const buttonHalfHeight = (buttonStyle?.height ? CssInterop.getNumberFromCss(buttonStyle.height) : defaultButtonDimInPx) / 2;


// TODO 230410 - rework this thing, overly complicated now that there is LabelledInput

    return Boolean(errorText) && (<>
        <button
            {...props}
            ref={buttonRef}
            children={<Icons.CircledExclamation className="hov-bg" />}
            onClick={() => { setIsTooltipOpen(true); }}
            style={{
                // ...errorComponentPositionStyle,
                height: `${defaultButtonDimInPx}px`,
                width: `${defaultButtonDimInPx}px`,
                cursor: 'pointer',
                color: isTooltipOpen
                    ? css.colour.aspect.error.average
                    : css.colour.aspect.error.average,
                ...props?.style,
                ...buttonStyle,
                // has been overriden taking the `buttonStyle` prop into account
                // width: `${buttonWidth}px`,
            }}
        />
        <Tooltip
            open={isTooltipOpen}
            onClose={() => { setIsTooltipOpen(false); }}
            triggerComponentRef={buttonRef}
            anchor={tooltipAnchor}
            gap={gap}
            crossGap={crossGap}
            style={{
                color: css.colour.aspect.error.foreground,
                backgroundColor: css.colour.aspect.error.background,
                borderColor: css.colour.aspect.error.foreground,
                fontSize: css.size.font.sm,
                fontWeight: '400',
                maxWidth: '100%',
                whiteSpace: 'nowrap',
                ...tooltipStyle,
            }}
            children={errorText}
        />
    </>);
}
