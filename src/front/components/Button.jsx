// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as CssInterop from 'front/CssInterop';
import * as Icons from './Icons';

export default React.forwardRef((props, ref) => (<Button _ref={ref} {...props} />));

function Button({
    // either use the label prop of the children implicit prop
    label,
    // text, outlined (default), contained
    variant,
    // neutral (default), info, success, warning, error
    aspect,
    // string of a variable name from Icons (case sensitive) (no icon is default)
    icon: iconLeft,
    // font weight
    weight = 'normal',
    // size of the icons and the text, as in CssInterop: tn, sm, md, lg, xl, etc.
    size = 'sm',
    iconRight,
    disableHoverAnimation,
    _ref,
    iconStyle,
    spanStyle,
    iconRightStyle,
    htmlElement = "button",
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const [isHovered, setIsHovered] = React.useState(false);
    const aspectValue = css.colour.aspect[aspect] ?? css.colour.aspect.neutral;
    const averageColourFromAspect = aspectValue.average;
    const foregroundColourFromAspect = aspectValue.foreground;
    const backgroundColourFromAspect = aspectValue.background;

    const isDisabled = props.disabled === 'disabled';
    const isText = !isDisabled && variant === 'text';
    const isContained = !isDisabled && variant === 'contained';
    const [unhoveredForegroundColour, hoveredForegroundColour, unhoveredBackgroundColour, hoveredBackgroundColour, hasBorder] = (
        /* case #1 */ isText ? [foregroundColourFromAspect, foregroundColourFromAspect, undefined, backgroundColourFromAspect, false] :
        /* case #2 */ isContained ? [css.colour.text.reverse, css.colour.text.strong, foregroundColourFromAspect, averageColourFromAspect, false] :
        /* case #3 */ isDisabled ? [css.colour.text.disabled, css.colour.text.disabled, undefined, undefined, true] :
        /* default */[foregroundColourFromAspect, foregroundColourFromAspect, undefined, backgroundColourFromAspect, true]);
    const foregroundColour = isHovered ? hoveredForegroundColour : unhoveredForegroundColour;
    const backgroundColour = isHovered ? hoveredBackgroundColour : unhoveredBackgroundColour;
    const content = label ?? props?.children;
    
    const IconLeftComponent = Icons[iconLeft];
    const IconRightComponent = Icons[iconRight];
    
    const HtmlElement = _props => React.createElement(
        htmlElement,
        {
            ..._props,
            ref: _ref,
        },
    );
    
    // The padding must be doubled on the right (/left) when there is text but no right (/left) icon
    const paddingInPx = CssInterop.getNumberFromCss(props?.style?.padding ?? '8px');

    const hasLabel = Boolean(content);
    const hasRightIcon = Boolean(IconRightComponent);
    const hasLeftIcon = Boolean(IconLeftComponent);
    const htmlElementPaddingRight = hasLabel && !hasRightIcon ? `${paddingInPx * 2}px` : `${paddingInPx}px`;
    const htmlElementPaddingLeft = hasLabel && !hasLeftIcon ? `${paddingInPx * 2}px` : `${paddingInPx}px`;

    function doClick(evt) {
        // must reset the isHovered state upon click, else clicking
        // for opening a modal leaves the button hovered forever
        !disableHoverAnimation && setIsHovered(false);
        props?.onClick && props.onClick(evt);
    }

    function doMouseEnter(evt) {
        !disableHoverAnimation && setIsHovered(true);
        props?.onMouseEnter && props.onMouseEnter(evt);
    }

    function doMouseLeave(evt) {
        !disableHoverAnimation && setIsHovered(false);
        props?.onMouseLeave && props.onMouseLeave(evt);
    }

    return (<>
        {/* 
            Note that the margins/paddings are handled by the container 
            and the text because there are three components only and text 
            has extra padding if near a button border 
        */}
        <HtmlElement
            // Note: ref is set in the HtmlElement creation
            {...props}
            // overriding some callbacks to handle isHovered state
            onMouseEnter={doMouseEnter}
            onMouseLeave={doMouseLeave}
            onClick={doClick}
            style={{
                display: 'inline-flex',
                alignItems: 'center',
                cursor: isDisabled ? 'default' : 'pointer',
                paddingTop: `${paddingInPx}px`,
                paddingBottom: `${paddingInPx}px`,
                paddingLeft: htmlElementPaddingLeft,
                paddingRight: htmlElementPaddingRight,
                color: foregroundColour ?? 'inherit',
                backgroundColor: backgroundColour ?? 'inherit',
                borderTop: hasBorder ? `1px solid ${foregroundColour}` : 'none',
                borderRight: hasBorder ? `1px solid ${foregroundColour}` : 'none',
                borderLeft: hasBorder ? `1px solid ${foregroundColour}` : 'none',
                borderBottom: hasBorder ? `1px solid ${foregroundColour}` : 'none',
                borderRadius: '1px',
                gap: '8px',
                fontWeight: weight,
                fontSize: css.size.font[size],
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                textTransform: 'uppercase',
                letterSpacing: '1.25',
                transition: '500ms',
                ...props?.style,
            }}>
                {Boolean(IconLeftComponent) &&
                    <IconLeftComponent style={{
                        width: css.size.icons[size],
                        height: css.size.icons[size],
                        ...iconStyle,
                    }} />
                }
                {Boolean(content) &&
                    <span style={{
                        flexGrow: '1',
                        ...spanStyle,
                    }}>
                        {content}
                    </span>
                }
                {Boolean(IconRightComponent) &&
                    <IconRightComponent style={{
                        width: css.size.icons[size],
                        height: css.size.icons[size],
                        ...iconRightStyle,
                    }} />
                }
            </HtmlElement>
    </>);
};