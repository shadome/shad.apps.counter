// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import TooltipedErrorAppendix from './TooltipedErrorAppendix';

export default React.forwardRef((props, ref) => (<LabelledInput _ref={ref} {...props} />));

function LabelledInput({
    // set to undefined if there is no error, else set to the error text to display (will display the component in the error state)
    errorText,
    // holds a meaningless value, which is only required to CHANGE when the error state of the component should be switched off
    errorResetFlag,
    // value of the input, handled by the caller
    value,
    // <input callback />
    onChange,
    // descriptive label of the input
    label,
    // <input type />
    // Note: input of type 'number' makes the onChange.evt.target.value always be a number, 0 by default, thus may be incompatible with error management
    type,
    // PRIVATE, USE "ref" - ref of the input, may be omitted if not required by the caller
    _ref,
    // children are considered as elements which will be placed inside the LabelledInput,
    // at the right, similarly to the error icon (which remains at the rightmost position)
    children,
    // nested element props optional overrides
    inputProps,
    fieldsetProps,
    legendProps,
    tooltipProps,
    // is applied to the input, overriden by input props
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const hasError = Boolean(errorText);
    _ref = _ref ?? React.useRef();

    return (<>
        <fieldset
            className="focusable"
            {...fieldsetProps}
            style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
                borderRadius: '1px',
                // the border is handled without the shorthand to be compatible with the 'focusable' class
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: hasError
                    ? css.colour.aspect.error.average
                    : css.colour.border,
                // a margin top must exist so that the legend's overflow-y + maxHeight of 0 can overflow
                marginTop: '8px',
                // a padding left is used to display a bit of the vertical border before legend: -<legend>----
                paddingLeft: '8px',
                ...fieldsetProps?.style,
            }}
        >
            <legend
                {...legendProps}
                style={{
                    padding: '0px 8px 0px 8px',
                    fontSize: css.size.font.xs,
                    fontWeight: '600',
                    color: hasError
                        ? css.colour.aspect.error.average
                        : css.colour.text.soft,
                    maxHeight: '0px',
                    margin: '0px',
                    overflowY: 'visible',
                    display: 'inline-flex',
                    alignItems: 'center',
                    ...legendProps?.style,
                }}
            >
                {label}
            </legend>
            <input
                ref={_ref}
                type={type}
                value={value}
                onChange={onChange}
                {...props}
                {...inputProps}
                style={{
                    flexGrow: 1,
                    background: 'none',
                    padding: '4px 8px',
                    margin: '0px',
                    height: '48px',
                    ...inputProps?.style,
                }}
            />
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '8px',
                marginRight: '8px',
                alignItems: 'center',
            }}>

                    {children}
                <TooltipedErrorAppendix
                    inputRef={_ref}
                    gap={20}
                    errorResetFlag={errorResetFlag}
                    errorText={errorText}
                    withinInput={true}
                    {...tooltipProps}
                />
            </div>
        </fieldset>
    </>);
}
