// libraries
import * as React from 'react';
// project dependencies
import * as Icons from './Icons';
import * as Contexts from 'context';

export default function Drawer({
    open: isOpen = false,
    onClose: doClose,
    position = 'left',
    withChevron: isChevronVisible = false,
    // zone to the left of the cross, if any
    // only relevant for positions "fullscreen" and "center"
    title = undefined,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    // goes along with isOpen: is still 'true' 
    // shortly after isOpen is set to 'false' by the caller
    // so that animations can be displayed
    const [isStillVisible, setIsStillVisible] = React.useState(false);
    const [isChevronHovered, setIsChevronHovered] = React.useState(false);
    // note: the following syntaxes ensure the default behaviour stays 'left', even if 'position' is provided with irrelevant values
    const isFullScreen = position === 'full';
    const isCenterModal = position === 'center';
    const isFullWidth = !isCenterModal && (isFullScreen || position === 'top' || position === 'bottom');
    const isFullHeight = !isCenterModal && (isFullScreen || !isFullWidth);
    // not sure this is really relevant
    // const maxHeight = !isFullHeight ? css.modal.maxDimension : undefined;
    // const maxWidth = !isFullWidth ? css.modal.maxDimension : undefined;
    const overlayModalFlexDirection = (
        /* case #1 */ position === 'top' ? 'column' :
        /* case #2 */ position === 'right' ? 'row-reverse' :
        /* case #3 */ position === 'bottom' ? 'column-reverse' :
        /* default */ 'row');
    const chevronIconModalContentFlexDirection = (
        /* case #1 */ position === 'top' ? 'column-reverse' :
        /* case #2 */ position === 'right' ? 'row' :
        /* case #3 */ position === 'bottom' ? 'column' :
        /* default */ 'row-reverse');
    const ChevronIcon = (
        /* case #1 */ position === 'top' ? Icons.ChevronUp :
        /* case #2 */ position === 'right' ? Icons.ChevronRight :
        /* case #3 */ position === 'bottom' ? Icons.ChevronDown :
        /* default */ Icons.ChevronLeft);

    const [width, setWidth] = React.useState();
    const [height, setHeight] = React.useState();
    const transitionDurationInMs = isFullScreen || isCenterModal ? 0 : 300;
    const transition = (
        /* case #1 */ !isCenterModal && !isFullWidth ? `width ${transitionDurationInMs}ms` :
        /* case #2 */ !isCenterModal && !isFullHeight ? `height ${transitionDurationInMs}ms` :
        /* default */ undefined);
    function refreshDimensionEffect() {
        const isSetWidthDelayed = !isOpen && isFullWidth;
        const isSetHeightDelayed = !isOpen && isFullHeight;
        const _width = (
            /* case #1 */ isOpen && isFullWidth ? '100%' :
            /* case #2 */ isOpen && props?.style?.width !== undefined ? props.style.width :
            /* case #3 */ isOpen && props?.style?.width === undefined ? '40%' : // default value if none provided
            /* default */ '0px');
        const _height = (
            /* case #1 */ isOpen && isFullHeight ? '100%' :
            /* case #2 */ isOpen && props?.style?.height !== undefined ? props.style.height :
            /* case #3 */ isOpen && props?.style?.height === undefined ? '40%' : // default value if none provided
            /* default */ '0px');
        // setting the width/height is delayed of the animation duration
        // IF the drawer is NOT full width/height (respectively)
        // ELSE the animation is broken with one dimension set to 0 immediately
        // note that dimensions must be reset in case the call sets a variable props.position
        isSetWidthDelayed
            ? setTimeout(() => setWidth(_width), transitionDurationInMs)
            : setWidth(_width);
        isSetHeightDelayed
            ? setTimeout(() => setHeight(_height), transitionDurationInMs)
            : setHeight(_height);
        isOpen
            ? setIsStillVisible(true)
            : isSetWidthDelayed || isSetHeightDelayed
                ? setTimeout(() => setIsStillVisible(false), transitionDurationInMs)
                : setIsStillVisible(false);
    };
    React.useEffect(refreshDimensionEffect, [isOpen]);

    return (<>
        <div style={{
            position: 'fixed',
            display: isOpen || isStillVisible ? 'flex' : 'none',
            top: '0',
            left: '0',
            height: '100%',
            width: '100%',
            zIndex: css.zIndexes.modal,
            backgroundColor: css.overlay.unreachable,
            flexDirection: overlayModalFlexDirection,
            alignItems: isCenterModal && 'center',
            justifyContent: isCenterModal && 'center',
        }}>
            {/* The below div is used because the above div 
                cannot handle the onClick event 
                else clicking within the modal would trigger it */}
            <div
                onClick={doClose}
                style={{
                    top: '0',
                    left: '0',
                    height: '100%',
                    width: '100%',
                    position: 'absolute',
                }}
            />
            <div {...props} style={{
                border: `solid 1px ${css.colour.border}`,
                borderRadius: '1px',
                backgroundColor: css.colour.background.dp1,
                position: 'relative',
                // maxHeight: maxHeight, // not sure this is really relevant
                // maxWidth: maxWidth, // not sure this is really relevant
                display: 'flex',
                flexDirection: isFullScreen || isCenterModal ? 'column' : chevronIconModalContentFlexDirection,
                gap: '16px',
                transition: transition,
                overflowX: 'scroll',
                overflowY: 'scroll',
                ...props?.style,
                // the following elements already take props.style.width/height 
                // into account and musn´t be overriden
                height: height,
                width: width,
            }}>
                {!isFullScreen && !isCenterModal && isChevronVisible &&
                    <div
                        onClick={doClose}
                        onMouseEnter={() => { setIsChevronHovered(true); }}
                        onMouseLeave={() => { setIsChevronHovered(false); }}
                        style={{
                            width: isFullWidth ? '100%' : undefined,
                            height: isFullHeight ? '100%' : undefined,
                            display: 'flex',
                            flexDirection: isFullHeight ? 'column' : 'row',
                            justifyContent: 'space-around',
                            backgroundColor: isChevronHovered ? css.overlay.hovered : undefined,
                            cursor: 'pointer',
                        }}
                    >
                        <ChevronIcon style={{
                            width: '36px',
                            height: '36px',
                        }} />
                    </div>
                }
                {(isFullScreen || isCenterModal) &&
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                        justifyContent: 'space-between',
                        gap: '4px',
                    }}>
                        <Icons.Cross
                            onClick={doClose}
                            onMouseEnter={() => { setIsChevronHovered(true); }}
                            onMouseLeave={() => { setIsChevronHovered(false); }}
                            style={{
                                width: '36px',
                                height: '36px',
                                alignSelf: 'flex-end',
                                backgroundColor: isChevronHovered ? css.overlay.hovered : undefined,
                                cursor: 'pointer',
                            }}
                        />
                        {title}
                    </div>
                }
                {props?.children}
            </div>
        </div>
    </>);
};
