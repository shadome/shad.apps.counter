// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

export default function Hoverable({
    // provide the children which are hoverable
    children,
    // provide an optional selected prop if relevant
    selected: isSelected = false,
    // optional component if this should not add an enclosing <div>
    component = undefined,
    // when this changes value, resets the Hovered state to unhovered
    resetTrigger = undefined,
    // other optional props
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);

    const [isHovered, setIsHovered] = React.useState(false);
    const overlay = (
        /* case #1 */ isSelected && isHovered ? css.overlay.hoveredAndSelected :
        /* case #2 */ isSelected ? css.overlay.selected :
        /* case #3 */ isHovered ? css.overlay.hovered :
        /* default */ css.overlay.none);
    
    React.useEffect(() => { setIsHovered(false); }, [resetTrigger]);
        
    const Component = component ?? ((props2) => (<div {...props2} />));
    return (<>
        <Component
            onMouseEnter={() => { setIsHovered(true); }}
            onMouseLeave={() => { setIsHovered(false); }}
            {...props}
            style={{
                backgroundColor: overlay,
                ...props.style,
            }}
        >
            {children}
        </Component>
    </>);
};