import * as React from 'react';
import * as Icons from './Icons';
// project dependencies
import * as Contexts from 'context';

export default function ThemeModeToggle({
    themeMode,
    setThemeMode,
}) {
    // Hovering events  are handled programmatically because:
    // * it feels simpler than maintaining css (at this point in time)
    // * making a generic ":hover" class in css seems impossible: for this component
    // is would define 'background-color: #fffN' with N the opacity level, whereas
    // other components might not need a slight white overlay
    const [isLeftHovered, setIsLeftHovered] = React.useState(false);
    const [isRightHovered, setIsRightHovered] = React.useState(false);
    const isLight = themeMode === 'light';
    const isDark = themeMode === 'dark';
    const css = React.useContext(Contexts.CssContext);
    return (<>
        <div style={{
            display: 'inline-flex',
        }}>
            <button
                onMouseEnter={() => { setIsLeftHovered(true); }}
                onMouseLeave={() => { setIsLeftHovered(false); }}
                onClick={() => { !isDark && setThemeMode('dark'); }}
                style={{
                    cursor: isDark ? 'default' : 'pointer',
                    height: '36px',
                    width: '36px',
                    padding: '6px',
                    backgroundColor:
                        isDark ? css.overlay.selected :
                        isLeftHovered ? css.overlay.hovered :
                        css.overlay.none,
                    border: `1px solid ${css.colour.border}`,
                    borderRightWidth: isDark ? '1px' : '0px',
                    borderTopLeftRadius: '1px',
                    borderBottomLeftRadius: '1px',
                }}
            >
                <Icons.Moon />
            </button>
            <button
                onMouseEnter={() => { setIsRightHovered(true); }}
                onMouseLeave={() => { setIsRightHovered(false); }}
                onClick={() => { !isLight && setThemeMode('light'); }}
                style={{
                    cursor: isLight ? 'default' : 'pointer',
                    height: '36px',
                    width: '36px',
                    padding: '6px',
                    backgroundColor:
                        isLight ? css.overlay.selected :
                        isRightHovered ? css.overlay.hovered :
                        css.overlay.none,
                    border: `1px solid ${css.colour.border}`,
                    borderLeftWidth: isLight ? '1px' : '0px',
                    borderTopRightRadius: '1px',
                    borderBottomRightRadius: '1px',
                }}
            >
                <Icons.Sun />
            </button>
        </div>
    </>);
};