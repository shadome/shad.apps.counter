// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

export default function CircularProgress({
    // must be an even number (will be rounded upwards otherwise)
    diameterInPx,
    // percent, from 0 to 100 (default to 0)
    percent,
    // string, see CssInterop
    aspect,
    // custom label to display (above the percentage if mustDisplayPct)
    label,
    // whether the current percentage must be displayed
    mustDisplayPct,
    // custom style override for the label display
    labelSpanStyle,
    // custom style override for the percentage display
    pctSpanStyle,
    // remaining props are applied to the label-wrapping div
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const aspectValue = css.colour.aspect[aspect] ?? css.colour.aspect.neutral;
    const diameter = Number.isInteger(diameterInPx)
        ? (Number.isInteger(diameterInPx) % 2 === 0
            ? Number(diameterInPx)
            : Number(diameterInPx) - 1)
        : 60;
    const radius = diameter / 2;
    const strokeWidth = Math.floor(radius / 4);
    const r = radius - Math.floor(strokeWidth / 2) - 1;
    const pct = Number.isInteger(percent) && Number(percent) >= 0 && Number(percent) <= 100
        ? percent
        : 0;
    const revPct = 100 - pct;
    return (<>
        <div style={{
            position: 'relative',
            width: `${diameter}px`,
            height: `${diameter}px`,
        }}>
            <div style={{
                position: 'absolute',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                height: '100%',
                textAlign: 'center',
                fontSize: css.size.font.sm,
                fontWeight: 'bold',
                ...props,
            }}>
                {label &&
                    <span style={{ fontSize: css.size.font.xs, ...labelSpanStyle }}>{label}</span>
                }
                {mustDisplayPct &&
                    <span style={{ ...pctSpanStyle }}>{pct}%</span>
                }
            </div>
            <svg
                width={diameter}
                height={diameter}
                viewBox={`0 0 ${diameter} ${diameter}`}
            >
                <circle
                    cx={radius}
                    cy={radius}
                    r={r}
                    fill="none"
                    stroke={aspectValue.background}
                    strokeWidth={strokeWidth}
                />
                <circle
                    strokeDasharray={`${pct} ${revPct}`}
                    cx={radius}
                    cy={radius}
                    r={r}
                    fill="none"
                    transform={`rotate(270, ${radius} ${radius})`}
                    stroke={aspectValue.foreground}
                    strokeWidth={strokeWidth}
                    pathLength="100"
                />
            </svg>
        </div>
    </>);
}