A component should only be made if it provides abstraction which cannot be provided with pure javascript on one single HTML tag.

In other words, a component should either:
* use `React.use` hooks, or
* make a component from more than one HTML component *(e.g., a model typically has a `div` for the modal content and a `div` for screen-wide black opacity which handles clics to close the modal)*

If a component only encapsulates some `const` not based on React hooks for one html element, rather use a pure javascript function or constant with spread operator, e.g.:
```jsx
<button
    // this does not require a React component, rather a pure javascript function or constant
    {...DefaultProps.button}
    style={{
        // this does not require a React component, rather a pure javascript function or constant
        ...DefaultProps.button.Style({ 
            variant: 'danger',
            otherParam: 'value',
        })
        // overriding default props
        borderRadius: '4px',
    }}
>
    Click me
</button>
```