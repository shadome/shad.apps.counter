import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import Button from './Button';

export default function DualButton({
    onLeftButtonClick,
    onRightButtonClick,
    label,
    leftButtonProps,
    rightButtonProps,
}) {
    const isLeftButtonDisabled = leftButtonProps?.disabled === true || leftButtonProps?.disabled === 'disabled';
    const isRightButtonDisabled = rightButtonProps?.disabled === true || rightButtonProps?.disabled === 'disabled';
    const css = React.useContext(Contexts.CssContext);
    return (<>
        <div style={{
            display: 'inline-flex',
            flexDirection: 'row',
            alignItems: 'stretch',
            justifyContent: 'flex-start',
            height: '40px',
            maxHeight: '40px',
        }}>
            <Button
                onClick={onLeftButtonClick}
                icon="Minus"
                {...leftButtonProps}
                disabled={isLeftButtonDisabled && 'disabled'}
                style={{ borderRight: 'none' }}
            />
            <span style={{
                display: 'inline-flex',
                justifyContent: 'center',
                alignItems: 'center',
                minWidth: '50px',
                height: '100%',
                padding: '0px 4px',
                borderStyle: 'solid',
                borderWidth: 1,
                borderColor: css.colour.text.soft,
                color: css.colour.text.regular,
                cursor: 'default',
            }}>
                {label}
            </span>
            <Button
                onClick={onRightButtonClick}
                icon="Plus"
                {...rightButtonProps}
                disabled={isRightButtonDisabled && 'disabled'}
                style={{ borderLeft: 'none' }}
            />
        </div>
    </>);
};
