// libraries
import * as React from 'react';
// project dependencies
import * as Icons from './Icons';
import * as CssInterop from 'front/CssInterop';
import * as Contexts from 'context';

{/* TODO TAGS modify margins: 
1- when no text, same left/right margin 
2- size shortcut => fix sizes (max height) 3- margins (dense/normal/etc., cf. emoji + number) */}
export default function Tag({
    // either use the label prop of the children implicit prop
    label,
    // string of a variable name from Icons (case sensitive) (no icon is default)
    icon,
    // neutral (default), info, success, warning, error
    aspect,
    // outlined (default) highlighted, contained, contained-highlighted,
    variant = 'outlined',
    // xs, sm (default), md, lg or xl
    size = 'sm',
    onClose,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    // component constants
    const IconComponent = Icons[icon];

    const aspectValue = css.colour.aspect[aspect] ?? css.colour.aspect.neutral;
    const averageColourFromAspect = aspectValue.average;
    const foregroundColourFromAspect = aspectValue.foreground;
    const backgroundColourFromAspect = aspectValue.background;
    const [colour, backgroundColour, borderColour] = (
        /* case #1 */ variant === 'contained' ? [css.colour.text.reverse, foregroundColourFromAspect, foregroundColourFromAspect]:
        /* case #2 */ variant === 'contained-highlighted' ? [css.colour.text.strong, averageColourFromAspect, averageColourFromAspect]:
        /* case #3 */ variant === 'highlighted' ? [foregroundColourFromAspect, backgroundColourFromAspect, foregroundColourFromAspect] :
        /* default */ [foregroundColourFromAspect, 'none', foregroundColourFromAspect]);

    const fontSize = css.size.font[size] ?? css.font.icon.sm;
    const iconSize = CssInterop.getNumberFromCss(fontSize) * (css.size.lineHeight ?? 1);
    
    return (
        <span {...props} style={{
            display: 'inline-flex',
            alignItems: 'baseline',
            backgroundColor: backgroundColour,
            color: colour,
            border: `1px solid ${borderColour}`,
            borderRadius: '4px',
            padding: '4px',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            ...props?.style,
        }}>
            {Boolean(IconComponent) &&
                <IconComponent style={{
                    alignSelf: 'center',
                    width: iconSize,
                    height: iconSize,
                    minWidth: iconSize,
                    minHeight: iconSize,
                }}/> 
            }
            {label !== undefined &&
                <span style={{
                    paddingRight: '4px',
                    paddingLeft: '4px',
                    fontSize: fontSize,
                    fontWeight: props.style?.fontWeight ?? '600',
                }}>
                    {label}
                    {props?.children}
                </span>
            }
            {Boolean(onClose) &&
                <Icons.Cross
                    onClick={onClose}
                    style={{
                        alignSelf: 'flex-start', // flex-start is like center for one-row tags, but like top for multiple-rows tags
                        cursor: 'pointer',
                        width: iconSize,
                        height: iconSize,
                        minWidth: iconSize,
                        minHeight: iconSize,
                    }}
                />
            }
        </span>
    );
};
