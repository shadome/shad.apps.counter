// libraries
import * as React from 'react';
// project dependencies
import * as CssInterop from 'front/CssInterop';
import * as Contexts from 'context';

export default function Toggle({
    id,
    isChecked,
    onToggle,
    // size of the icons and the text, as in CssInterop: tn, sm, md, lg, xl, etc.
    size = 'sm',
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    // component constants
    const toggleBorderSize = 1;
    const toggleHeight = CssInterop.getNumberFromCss(css.size.icons[size]);
    const toggleWidth = Math.ceil(toggleHeight * 40 / 24);
    const togglePadding = 4;
    const squareDimension = toggleHeight - (toggleBorderSize * 2) - (togglePadding * 2);
    const squareLeftOffset = isChecked
        ? toggleWidth - togglePadding - squareDimension - toggleBorderSize
        : togglePadding;

    return (<>
        <label
            htmlFor={id}
            {...props}
            style={{
                // display: 'inline-flex',
                backgroundColor: isChecked ? css.colour.aspect.info.average : css.colour.background.dp0,
                cursor: 'pointer',
                transition: '200ms',
                width: `${toggleWidth}px`,
                ...props.style,
                // required styling which cannot be overriden by the caller
                position: 'relative',
                height: `${toggleHeight}px`,
                border: `${toggleBorderSize}px solid ${css.colour.border}`,
                padding: `${togglePadding}px`,
            }}
        >
            <input
                id={id}
                type="checkbox"
                checked={isChecked}
                onChange={evt => { onToggle(evt.target.checked); }}
                style={{
                    width: '0',
                    height: '0',
                    display: 'none',
                }}
            />
            <span
                style={{
                    transition: 'inherit',
                    position: 'absolute',
                    left: `${squareLeftOffset}px`,
                    backgroundColor: isChecked ? 'currentcolor' : css.colour.text.soft,
                    height: `${squareDimension}px`,
                    width: `${squareDimension}px`,
                }}
            />
        </label>
    </>);
}
