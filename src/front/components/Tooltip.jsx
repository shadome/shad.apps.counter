// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

// TODO does not work when resizing, for some reason, enquire into it
export default function Tooltip({
    open: isOpen,
    onClose,
    anchor = 'bottom',
    // ref towards the component to which the tooltip is attached, i.e., its trigger
    triggerComponentRef,
    // min width set to the width of the trigger component's
    bindMinWidth = false,
    // max width set to the width of the trigger component's
    bindMaxWidth = false,
    // nb of pixels representing the gap between the triggerComponentRef and the actual tooltip on the main axis
    gap = 0,
    // nb of pixels representing the gap between the triggerComponentRef and the actual tooltip on the cross axis
    crossGap = 0,
    opacity = 0.9,
    disableAnimations = false,
    component: Component = x => (<div {...x} />),
    additionalOffsetX = 0,
    additionalOffsetY = 0,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    // goes along with isOpen: is still 'true' 
    // shortly after isOpen is set to 'false' by the caller
    // so that animations can be displayed
    const [isStillVisible, setIsStillVisible] = React.useState(false);
    const isTooltipEnabled = isOpen || isStillVisible;

    /* Set the tooltip's min width, top and left offsets */
    const [triggerComponentWidth, setTriggerComponentWidth] = React.useState(0);
    const [tooltipPositionStyle, setTooltipPositionStyle] = React.useState({});
    function setDropdownDimensionsEffect() {
        let isMounted = true;
        if (!triggerComponentRef?.current) {
            return;
        }
        isMounted && setTriggerComponentWidth(triggerComponentRef.current.offsetWidth);
        let top;
        let left;
        // Note about plus and minus for crossGap: basically,
        // when the translation is -50 % or 0 %, the crossGap is added,
        // but when the translation is -100%, which signifies a referential
        // reversal, the crossGap is subtracted
        switch (anchor) {
            case 'top':
                top = triggerComponentRef.current.offsetTop - 1 + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft + (triggerComponentRef.current.offsetWidth / 2) + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    transform: 'translate(-50%, -100%)',
                    top: `${top - gap}px`,
                    left: `${left + crossGap}px`,
                });
                break;
            case 'topleft':
                top = triggerComponentRef.current.offsetTop - 1 + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft + triggerComponentRef.current.offsetWidth + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    transform: 'translate(-100%, -100%)',
                    top: `${top - gap}px`,
                    left: `${left - crossGap}px`,
                });
                break;
            case 'left':
                top = triggerComponentRef.current.offsetTop + (triggerComponentRef.current.offsetHeight / 2) + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft - 1 + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    transform: 'translate(-100%, -50%)',
                    top: `${top + crossGap}px`,
                    left: `${left - gap}px`,
                });
                break;
            case 'bottomright':
                top = 1 + triggerComponentRef.current.offsetTop + triggerComponentRef.current.offsetHeight + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    top: `${top + gap}px`,
                    left: `${left + crossGap}px`,
                });
                break;
            case 'bottomleft':
                top = 1 + triggerComponentRef.current.offsetTop + triggerComponentRef.current.offsetHeight + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft + triggerComponentRef.current.offsetWidth + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    transform: 'translate(-100%, 0%)',
                    top: `${top + gap}px`,
                    left: `${left - crossGap}px`,
                });
                break;
            case 'bottom':
            default:
                top = 1 + triggerComponentRef.current.offsetTop + triggerComponentRef.current.offsetHeight + additionalOffsetY;
                left = triggerComponentRef.current.offsetLeft + (triggerComponentRef.current.offsetWidth / 2) + additionalOffsetX;
                isMounted && setTooltipPositionStyle({
                    transform: 'translate(-50%, 0%)',
                    top: `${top + gap}px`,
                    left: `${left + crossGap}px`,
                });
                break;
        }
        return () => { isMounted = false; }
    }
    // TODO find a way to compare this value with the previous frame's and trigger setDropdownDimensions if it is different
    // note that this component must re-render when the windows resizes or something like that happens
    // const triggerComponentVariablesToWatch = getTriggerComponentVariablesToWatch(triggerComponentRef);
    const setDropdownDimensionsEffectDep = [// not perfect but good enough for now
        triggerComponentRef?.current?.offsetWidth,
        triggerComponentRef?.current?.offsetHeight,
        triggerComponentRef?.current?.offsetTop,
        triggerComponentRef?.current?.offsetLeft,
    ];
    React.useEffect(setDropdownDimensionsEffect, setDropdownDimensionsEffectDep);

    /* Programmatically handle the tooltip's opacity for animations */
    const transitionDurationInMs = !disableAnimations ? 200 : 0;
    const transition = !disableAnimations ? `opacity ${transitionDurationInMs}ms` : undefined;
    const [tooltipOpacity, setTooltipOpacity] = React.useState(opacity);
    function setDropdownHeightEffect() {
        let isMounted = true;
        isMounted && (isOpen || disableAnimations)
            ? setIsStillVisible(isOpen)
            : setTimeout(() => isMounted && setIsStillVisible(false), transitionDurationInMs);
        isMounted && setTooltipOpacity(isOpen ? opacity : 0);
        return () => { isMounted = false; }
    }
    React.useEffect(setDropdownHeightEffect, [isOpen]);

    /* Listen to the click event to close the tooltip if open */
    // Dev note: the following syntax may be overly complicated
    // but it ensures no more than one similar event listener is active at the same time (also, only when isOpen is true)
    /* Clean up when closing the dropdown area */
    function doClose(event) {
        // TODO do not close if the click occurs within the tooltip area (i.e., allow copy/paste)
        event && event.stopPropagation();
        event && event.preventDefault();
        onClose();
    }
    // Callback required to persist the reference of the onClose function for event listener removal
    const doCloseCallback = React.useCallback(doClose, []);
    /* Attach (/Detach) the keydown listener (attached when the dropdown area is open) */
    function updateListenersEffect() {
        // Warning: the onClose prop is expected to update the isOpen prop to 'false',
        // which should result in detaching the event listener, but the caller could
        // also handle isOpen itself, hence the event must be detached when isOpen toggles,
        // not in the above doClose function
        isOpen
            ? window.addEventListener('click', doCloseCallback)
            : window.removeEventListener('click', doCloseCallback);
    }
    React.useEffect(updateListenersEffect, [isOpen]);

    const forcedMinWidth = bindMinWidth ? { minWidth: `${triggerComponentWidth}px` } : {};
    const forcedMaxWidth = bindMaxWidth ? { maxWidth: `${triggerComponentWidth}px` } : {};

    return (<>
        {isTooltipEnabled &&
            <Component
                {...props}
                style={{
                    position: 'absolute',
                    zIndex: css.zIndexes.modal,
                    ...tooltipPositionStyle,
                    ...forcedMinWidth,
                    ...forcedMaxWidth,
                    overflowY: 'auto',
                    backgroundColor: css.colour.background.dp1,
                    border: `solid 1px ${css.colour.border}`,
                    padding: '8px',
                    scrollbarColor: `${css.colour.background.dp2} ${css.colour.background.dp1}`,
                    opacity: tooltipOpacity,
                    transition: transition,
                    ...props.style,
                }}
            />
        }
    </>);
}
