// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from './Icons';
import * as KeyBindings from 'utils/keyBindings';
// project components
import Tooltip from './Tooltip';

// TODO does not work when resizing, for some reason, enquire into it

function DefaultTriggerComponent({
    _ref,
    isOpen,
    setIsOpen,
    placeholder,
    selectedValue,
    valueToTextFunc,
    ...otherProps
}) {
    const css = React.useContext(Contexts.CssContext);
    return (<>
        <fieldset
            ref={_ref}
            {...otherProps}
            style={{
                borderStyle: 'solid',
                borderRadius: '1px',
                borderColor: css.colour.border,
                borderWidth: '1px',
                display: 'flex',
                padding: '0px',
                margin: '0px',
                ...otherProps.style,
            }}
        >
            {selectedValue &&
                <legend style={{
                    padding: '0px',
                    paddingLeft: '8px',
                    paddingRight: '8px',
                    marginLeft: '12px',
                    marginRight: '12px',
                    fontSize: '14px',
                    fontWeight: '600',
                    color: otherProps?.color ?? css.colour.text.soft,
                    display: placeholder ? 'flex' : 'none',
                    alignItems: 'center',
                    maxHeight: '0px',
                    overflowY: 'visible',
                }}>
                    {placeholder}
                </legend>
            }
            <button
                onClick={() => { !isOpen && setIsOpen(true); }}
                style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    padding: '8px',
                    paddingLeft: '8px',
                    paddingRight: '8px',
                    color: otherProps.style?.color ?? css.colour.text.soft,
                    border: 'none',
                    borderRadius: '1px',
                    fontSize: '14px',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    textTransform: otherProps.style?.textTransform ?? 'none',
                    letterSpacing: '1.25',
                    width: '100%',
                    textAlign: otherProps.style?.textAlign ?? 'start',
                }}
            >
                <span style={{
                    marginLeft: '8px',
                    marginRight: '8px',
                    flexGrow: '1',
                    fontSize: css.size.font.md,
                    ...(!selectedValue ? { color: css.colour.text.soft } : {})
                }}>
                    {/* TODO this is a bit tricky: must be the label when nothing is selected, 
                    but must be the selected text value (not just 'value') when something is selected,
                    so the dropdown items should not accept children but instead come with a 'text' attribute?
                    or make a 'isSelectedValueCallback' which is a comparison function to tell if the given children is the selected value?
                    like the second best, but harder to understand for users */}
                    {selectedValue
                        ? valueToTextFunc(selectedValue)
                        : placeholder
                    }
                </span>
                {isOpen
                    ? <Icons.ChevronUp style={{ width: '20px', height: '20px' }} />
                    : <Icons.ChevronDown style={{ width: '20px', height: '20px' }} />
                }
            </button>
        </fieldset>
    </>);
}

function DefaultNoChildrenComponent() {
    return (<>
        <span
            style={{ fontWeight: '500', padding: '8px' }}
            children="No element"
        />
    </>);
}

export default function Dropdown({
    // placeholder and triggerComponent and htmlElement are exclusive
    htmlElement, // e.g., "button",
    // placeholder and triggerComponent and htmlElement are exclusive
    triggerComponent: customTriggerComponent, // e.g., {<button ... />}
    // placeholder and triggerComponent and htmlElement are exclusive
    placeholder,
    selectedValue,
    onSelect = () => {},
    disableAnimations = false,
    disableTooltipMinWidth = false,
    forceTooltipMaxWidth = false,
    valueToTextFunc = x => x,
    children,
    dropdownChildren, // override to "children" for the prop to attach to the htmlElement or customTriggerComponent (children are reserved for DropdownItems)
    tooltipProps,
    ...props
}) {
    // Warning: call doClose rather than setIsOpen(false);
    const [isOpen, setIsOpen] = React.useState(false);

    // ref required by the tooltip for positionning
    const triggerComponentRef = customTriggerComponent?.ref ?? React.useRef();

    /* Extract and set the children considered as valid Dropdown items */
    // list of the children components considered as valid Dropdown items
    const [selectableDropdownItems, setSelectableDropdownItems] = React.useState([]);
    function onChildrenChange() {
        let isMounted = true;
        const newChildren = React.Children
            .toArray(children)
            // filters using a native value, i.e., _child.type.name, would not work
            // in production where such strings are optimised:
            // https://mparavano.medium.com/find-filter-react-children-by-type-d9799fb78292
            .filter(_child => _child.props.__private_selector === 'DropdownItem');
        isMounted && setSelectableDropdownItems(newChildren);
        return () => { isMounted = false; }
    }
    React.useEffect(onChildrenChange, [children]);
    
    // Both state and stateRef are needed so that: 
    // * the state is used in the rendering 
    // * and the ref is used through the event listener
    const [selectedIdx, _setSelectedIdx] = React.useState(null);
    const selectedIdxRef = React.useRef(null);
    function setSelectedIdx(idx) {
        _setSelectedIdx(idx);
        selectedIdxRef.current = idx;
    }

    /* Listen to certain key bindings to allow the user to navigate the list using their keyboard */
    const keyBindings = [
        [KeyBindings.KeyCodes.arrowUp, () => { setSelectedIdx(Math.max((selectedIdxRef.current ?? 0) - 1, 0)); }],
        [KeyBindings.KeyCodes.arrowDown, () => { setSelectedIdx(Math.min((selectedIdxRef.current ?? -1) + 1, selectableDropdownItems.length - 1)); }],
        [KeyBindings.KeyCodes.enter, () => { selectedIdxRef.current !== null && onSelect(selectableDropdownItems[selectedIdxRef.current].props.value); doClose(); }],
        [KeyBindings.KeyCodes.escape, doClose],
    ];
    const dispatchKeyCodeHandler = KeyBindings.useKeyBindings(keyBindings);
    // ref required to persist the reference of the function for event listener removal
    // note that using a callback through useKeyBindings does not work when the keyBindings
    // array changes (with dynamic DropdownItem lists), but useRef works fine
    const lastRegisteredDispatchKeyCodeHandler = React.useRef(dispatchKeyCodeHandler);

    /* Attach (/Detach) the keydown listener (attached when the dropdown area is open) */
    function updateListenersEffect() {
        let isMounted = true;
        if (isOpen) {
            window.removeEventListener('keydown', lastRegisteredDispatchKeyCodeHandler.current);
            if (isMounted) {
                lastRegisteredDispatchKeyCodeHandler.current = dispatchKeyCodeHandler;
            }
            window.addEventListener('keydown', dispatchKeyCodeHandler);
        } else {
            window.removeEventListener('keydown', lastRegisteredDispatchKeyCodeHandler.current);
        }
        return () => { isMounted = false; }
    }
    React.useEffect(updateListenersEffect, [isOpen, selectableDropdownItems]);

    /* Cleanup when closing the dropdown area */
    // Warning: use this function to close the dropdown area, do not call setIsOpen(false)
    function doClose() {
        setIsOpen(false);
        setSelectedIdx(null);
        window.removeEventListener('keydown', dispatchKeyCodeHandler);
    }

    /* Set the trigger component, either default or a provided custom one */
    const triggerComponent = (
        htmlElement ? React.createElement(
            htmlElement,
            {
                ...props,
                children: dropdownChildren,
                ref: triggerComponentRef,
                onClick: (evt) => {
                    props.onClick && props.onClick(evt);
                    !isOpen && setIsOpen(true);
                },
                onChange: (evt) => {
                    props.onChange && props.onChange(evt);
                    !isOpen && setIsOpen(true);
                },
            },
        ) :
        customTriggerComponent ? React.cloneElement(
            customTriggerComponent,
            {
                ...customTriggerComponent.props,
                ...props,
                children: dropdownChildren,
                ref: triggerComponentRef,
                onClick: (evt) => {
                    customTriggerComponent.props.onClick && customTriggerComponent.props.onClick(evt);
                    !isOpen && setIsOpen(true);
                },
                onChange: (evt) => {
                    customTriggerComponent.props.onChange && customTriggerComponent.props.onChange(evt);
                    !isOpen && setIsOpen(true);
                },
            }
        ) : (
            <DefaultTriggerComponent
                _ref={triggerComponentRef}
                isOpen={isOpen}
                placeholder={placeholder}
                selectedValue={selectedValue}
                setIsOpen={setIsOpen}
                valueToTextFunc={valueToTextFunc}
                {...props}
            />
        )
    );

    /* Set the dropdown options, either a default unselectable "no option" span or the provided selectable ones */
    const childrenComponents = selectableDropdownItems?.length > 0
        ? React.Children.map(
            React.Children.toArray(selectableDropdownItems),
            (child, idx) => React.cloneElement(
                child,
                {
                    focused: selectedIdx === idx++,
                    dropdownOpen: isOpen,
                    onClick: () => { onSelect(child.props.value); },
                    ...child.props,
                    key: child.props.value,
                }))
        : <DefaultNoChildrenComponent />;

    return (<>
        {triggerComponent}
        <Tooltip
            open={isOpen}
            onClose={() => { doClose(); }}
            triggerComponentRef={triggerComponentRef}
            disableAnimations={disableAnimations}
            anchor="bottomright"
            bindMinWidth={!disableTooltipMinWidth}
            bindMaxWidth={forceTooltipMaxWidth}
            component={x => (<ul {...x} />)}
            {...tooltipProps}
            style={{
                // override default UL styles
                listStyleType: 'none',
                padding: '0px',
                marginTop: '0px',
                marginBottom: '0px',
                ...tooltipProps?.style,
            }}
        >
            {childrenComponents}
        </Tooltip>
    </>);
}