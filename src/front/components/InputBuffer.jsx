// libraries
import * as React from 'react';
// project dependencies
import * as Icons from './Icons';
import * as Contexts from 'context';
import Tooltip from './Tooltip';

export default function InputBuffer({
    placeholder,
    onBufferChange,
    value,
    tooltipValue,
    // Note: the styling will go to the outermost container
    // while the rest of the props will go to the input html tag
    ...props
}) {
    // project constants
    const css = React.useContext(Contexts.CssContext);
    // Whether the input field is focused by the user
    const [isFocused, setIsFocused] = React.useState(false);
    // Whether the help tooltip is open
    const [isHelpOpen, setIsHelpOpen] = React.useState(false);
    // Whether the help tooltip is hovered
    const [isHelpHovered, setIsHelpHovered] = React.useState(false);

    // Used to position the help tooltip, if any
    const helpIconRef = tooltipValue
        ? React.useRef(null)
        : undefined;

    const hasLegend = Boolean(placeholder) && Boolean(value);

    const { style: propsStyle, ...otherProps } = { ...props };
    
    function doChange(evt) {

    }

    return (<>
        {tooltipValue &&
            <Tooltip
                open={isHelpOpen}
                onClose={() => { setIsHelpOpen(false); }}
                triggerComponentRef={helpIconRef}
                anchor="topleft"
                style={{
                    minWidth: '300px',
                }}
            >
                {tooltipValue}
            </Tooltip>
        }
        <fieldset style={{
            borderRadius: '1px',
            borderColor: isFocused ? css.colour.aspect.info.average : css.colour.border,
            borderWidth: '1px',
            display: 'flex',
            flexDirection: 'column',
            padding: '8px 12px 8px 12px',
            ...propsStyle,
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.soft,
                display: hasLegend ? 'flex' : 'none',
                alignItems: 'center',
                maxHeight: '0px',
                overflowY: 'visible',
            }}>
                {placeholder}
            </legend>
            <div style={{
                display: 'flex',
                gap: '4px',
                flexWrap: 'wrap',
            }}>
                {['Toto'].map(_value => 
                    <span key={_value} style={{
                        backgroundColor: 'gray',
                        padding: '4px',
                    }}>
                        {_value}
                    </span>
                )}
            </div>
            <input
                value={value}
                onChange={doChange}
                placeholder={placeholder}
                onFocus={() => { setIsFocused(true); }}
                onBlur={() => { setIsFocused(false); }}
                {...otherProps}
                style={{
                    color: css.colour.text.regular,
                    marginTop: '4px',
                    background: 'none',
                    border: '0',
                    fontSize: '18px',
                    width: '100%',
                }}
            />
            {tooltipValue &&
                <div
                    style={{
                        width: '28px',
                        height: '28px',
                        cursor: 'pointer',
                        color: isHelpOpen || isHelpHovered
                            ? css.colour.aspect.info.average
                            : css.colour.text.disabled,
                    }}
                    ref={helpIconRef}
                    onClick={() => { setIsHelpOpen(true); }}
                    onMouseEnter={() => { setIsHelpHovered(true); }}
                    onMouseLeave={() => { setIsHelpHovered(false); }}
                >
                    <Icons.Help />
                </div>
            }
        </fieldset>
    </>);
}