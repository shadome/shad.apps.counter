// libraries
import * as React from 'react';
import PropTypes from 'prop-types';

// project dependencies
import Hoverable from './Hoverable';

function DropdownItem({
    focused: isFocused,
    // forwarded ref
    _ref,
    dropdownOpen: isDropdownOpen,
    // specific to Dropdown
    selectableDropdownItem = true,
    children,
    ...props
}) {
    return (<>
        <Hoverable
            component={(_props) => <li ref={_ref} {...props} {..._props} />}
            selected={isFocused}
            tabIndex="0"
            resetTrigger={isDropdownOpen}
            {...props}
            style={{
                display: 'block',
                cursor: 'pointer',
                padding: '8px',
                height: '35px',
                width: '100%',
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                ...props.style,
            }}
        >
            {children}
        </Hoverable>
    </>);
};

// The following is required for React.Children.ToArray(children).filter( /* filter on something which will not change between dev and prod environments */ )
// see https://mparavano.medium.com/find-filter-react-children-by-type-d9799fb78292
DropdownItem.propTypes = {
    children: PropTypes.node,
    __private_selector: PropTypes.string,
};

DropdownItem.defaultProps = {
    __private_selector: 'DropdownItem',
};

// export default React.forwardRef((props, ref) => (<DropdownItem _ref={ref} {...props} />));
export default DropdownItem;
