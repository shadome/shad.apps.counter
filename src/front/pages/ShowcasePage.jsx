// libraries
import * as React from 'react';
// project dependencies
import * as Breakpoints from '../Breakpoints';
import * as Contexts from '../../context';
// project components
import ButtonSection from './ShowcasePage/ButtonSection';
import DrawerSection from './ShowcasePage/DrawerSection';
import TagSection from './ShowcasePage/TagSection';
import DropdownSection from './ShowcasePage/DropdownSection';
import ProgressSection from './ShowcasePage/ProgressSection';

export default function ShowcasePage() {
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    /* Set the page's title */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    React.useEffect(() => { setAppTitle('🎉 Show time 🎉'); }, []);

    const isMobile = breakpoint <= Breakpoints.sm;
    const padding = !isMobile ? '32px' : undefined;

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'start',
            alignSelf: 'center',
            width: '100%',
            maxWidth: '1000px',
            gap: '8px',
            paddingLeft: padding,
            paddingRight: padding,
        }}>

            <div>
                <p style={{ all: 'revert' }}>
                    Some pages of this application still use the MUI components library,
                    but it is currently migrated towards components designed without
                    any third-party library.
                    The intention is to use non-encapsulated CSS wherever possible and make the JavaScript bundle file smaller.
                    A few words of personal motivation:
                </p>
                <ul style={{ all: 'revert' }}>
                    <li>usual (e.g., components) libraries don't feel indispensable to me and I want to see if using them saves that much time or not</li>
                    <li>usual libraries cover way too many use cases and make for big bundle files and fuzzy interfaces / signatures: I think the learning curve should be orientated towards learning a language or a framework, not a specific library's implementation choices</li>
                    <li>many ex-colleagues have complained on how difficult and time consuming it is to migrate to newer versions of every dependency on mature production apps for a large dependency list</li>
                    <li>this implementation is supposed to be easily interoperable / refactorable using tailwind, which is the library which seems to best fit my needs and mindset</li>
                </ul>
                <p style={{ all: 'revert' }}>
                    In any case, an abstraction for general-purpose
                    components is sometimes desirable, especially when JavaScript event-handling or DOM
                    manipulations are involved. This page showcases those components, made from pure React and CSS code.
                </p>
            </div>

            <DropdownSection />

            <ButtonSection />

            <DrawerSection />
            
            <TagSection />

            <ProgressSection />
            
        </div>
    </>);
};
