// libraries
import * as React from 'react';
import * as ReactRouterDom from 'react-router-dom';
// project dependencies
import * as Contexts from 'context';
import * as FoodMetadata from 'model/FoodMetadata';
import * as FoodAdapter from 'model/adapters/FoodAdapter';
import * as FoodStore from 'repository/StoreFood';
import * as FoodRelationsStore from 'repository/StoreFoodRelations';
import * as StrUtils from 'utils/strings';
import * as Icons from 'front/components/Icons';
import useForm from 'utils/form';
import FoodCategory from 'model/FoodCategory';
import FoodDataSource from 'model/FoodDataSource';
// project components
import Button from 'front/components/Button';
import SetIngredientModal from './FoodPage/SetIngredientModal';
import SetFoodNutrientTabs, { FoodAddTabEnum } from './FoodPage/SetFoodNutrientTabs';
import SetFoodIngredientsInfo from './FoodPage/SetFoodIngredientsInfo';
import SetFoodIngredients from './FoodPage/SetFoodIngredients';
import SetFoodMacronutrients from './FoodPage/SetFoodMacronutrients';
import SetFoodMicronutrients from './FoodPage/SetFoodMicronutrients';
import SetFoodMain from './FoodPage/SetFoodMain';
import SetFoodButtonBar from './FoodPage/SetFoodButtonBar';
import SelectFoodToEditModal from './FoodPage/SelectFoodToEditModal';

/*
 * 
 * 
 * TODO
 * total row in ingredients if more than one => is the nominal quantity
 * edit button out of the ellipsis in ingredients for desktop display
 * 
 * 
 * TODO
 * - possibility to delete an existing food
 * 
 * TODO
 * - when updating a food, propose to update the macros of the foods which have the updated food in their composition?
 * - use the nominal quantity when adding an ingredient in a composed food
 * 
*/

export function getPageUrl({ label, foods, code } = {}) {
    let params = [];
    if (Boolean(label)) {
        const labelStr = encodeURIComponent(label);
        params.push(`label=${labelStr}`);
    }
    if (!isNaN(code)) {
        const codeStr = encodeURIComponent(code);
        params.push(`code=${codeStr}`);
    }
    if (Array.isArray(foods) && foods.length > 0) {
        const foodCodesAndQuantitiesJson = JSON.stringify(foods);
        const foodCodesAndQuantitiesStr = encodeURIComponent(foodCodesAndQuantitiesJson);
        params.push(`foodCodesAndQuantities=${foodCodesAndQuantitiesStr}`);
    }
    return params.length > 0
        ? `/food?${params.join('&')}`
        : '/food';
}

export default function FoodPage() {
    /* Extract optional url parameters if this page is called for creating a food from a meal group */
    const history = ReactRouterDom.useHistory();
    const location = ReactRouterDom.useLocation();
    async function handleUrlParametersEffect() {
        let isMounted = true;
        const searchParams = new URLSearchParams(location.search);
        const urlLabelStr = searchParams.get('label');
        const urlFoodCodeToEditStr = searchParams.get('code');
        const urlFoodCodesAndQuantitiesStr = searchParams.get('foodCodesAndQuantities');
        const urlLabel = urlLabelStr && decodeURIComponent(urlLabelStr);
        const urlFoodCodeToEdit = urlFoodCodeToEditStr && decodeURIComponent(urlFoodCodeToEditStr);
        const urlFoodCodesAndQuantitiesJson = urlFoodCodesAndQuantitiesStr && decodeURIComponent(urlFoodCodesAndQuantitiesStr);
        const urlFoodCodesAndQuantities = JSON.parse(urlFoodCodesAndQuantitiesJson) || [];
        const formattedFoodCodeAndQuantityList = urlFoodCodesAndQuantities
            .map(x => ({
                subfoodCode: x.c,
                quantityInGrams: x.q,
            }));

        if (isMounted && urlLabel && urlFoodCodesAndQuantities) {
            isMounted && setForm({ label: urlLabel });
            isMounted && doCreateSubfoods(formattedFoodCodeAndQuantityList);
        } else if (isMounted && urlFoodCodeToEdit) {
            const foodToEdit = await FoodStore.Read({ code: urlFoodCodeToEdit });
            isMounted && setForm({
                ...foodToEdit, // lazy way to inflate values from the food to edit, note that irrelevant fields will be ignored
            });
            isMounted && setFoodToEdit(foodToEdit);

            const foodRelations = isMounted
                ? await FoodRelationsStore.Read({ superfood_code: foodToEdit.code })
                : [];
            const ingredientCodes = foodRelations.map(x => x.subfood_code);
            // higher order array for method chaining, contains 0 or 1 elements
            const ingredients = isMounted && ingredientCodes.length > 0
                ? [await FoodStore.Read({ codes: ingredientCodes })]
                    // the Read function would return a single value as an object rather than an array of size 1
                    .map(x => x && !Array.isArray(x) ? [x] : x)
                    .filter(x => Array.isArray(x) && x.length > 0)
                    .map(x => x.map(y => ({
                        subfood: y,
                        quantityInGrams: foodRelations.find(z => z.subfood_code === y.code).quantity_abs,
                    })))
                    .map(x => x.sort((a, b) => b.quantityInGrams - a.quantityInGrams))
                    // end of method chaining: first element or undefined
                    .find(x => true)
                : [];
            const nutrientsAndNominalQuantityUpdate = ingredients.length > 0
                ? getFormUpdateFromIngredients(ingredients)
                : getFormUpdateFromFoodWithoutIngredients(foodToEdit);
            isMounted && setForm({
                ingredientsAndQty: ingredients,
                ...nutrientsAndNominalQuantityUpdate,
                ...(foodToEdit.nominal_quantity && { nominalQuantity: foodToEdit.nominal_quantity }),
            });
        }

        return () => { isMounted = false; }
    }
    React.useEffect(handleUrlParametersEffect, [location.search]);

    /* Set the app's title using the dedicated context */
    const addFeedback = React.useContext(Contexts.AddFeedbackContext);
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    React.useEffect(() => { setAppTitle('Create a food'); }, []);

    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    // null if adding a new food, inflated if modifying an existing food
    const [foodToEdit, setFoodToEdit] = React.useState(null);

    // selected type of form for creating a new food
    const [selectedTab, setSelectedTab] = React.useState(FoodAddTabEnum.ingredients);
    // since 0 is a legitimate value for all fields, this is a (bad) way of knowing if the user has input at least something somewhere
    const [hasAtLeastOneModification, setHasAtLeastOneModification] = React.useState(false);

    /* Form for adding a new food composed of other foods */
    const [form, setForm, validateForm, resetForm, resetFormErrors, setFormErrors] = useForm({
        label: {
            value: '',
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'Set a label for the new food',
            }, {
                predicate: (val) => val.length <= 500,
                error: 'The new food label should not exceed 500 characters',
            }, {
                predicate: (val) => val.length >= 2,
                error: 'The new food label should be at least 2 characters',
            }],
        },
        nominalQuantity: {
            value: 100,
            validations: [{
                predicate: (val) => !isNaN(val),
                error: 'The value should be a number',
            }, {
                predicate: (val) => Number(val) >= 0,
                error: 'The value should be a positive number',
            }],
        },
        ingredientsAndQty: {
            // [{ subfood: {...}, quantityInGrams: Number(...) }]
            value: [],
        },
        // reduce every nutrient into a form component
        ...Object.values(FoodMetadata.All).reduce(
            (acc, cur) => ({
                ...acc,
                [cur.id]: {
                    value: 0,
                    validations: [{
                        predicate: (val) => !isNaN(val),
                        error: 'The value should be a number',
                    }, {
                        predicate: (val) => Number(val) >= 0,
                        error: 'The value should be a positive number',
                    }],
                },
            }),
            /* initialValue: */ {}),
    });

    // setForm hook override
    function doSetFormAndRegisterNutrientModification(args) {
        setHasAtLeastOneModification(true);
        setForm(args);
    }

    // reset everything in this page
    function resetState() {
        resetForm();
        setSelectedTab(FoodAddTabEnum.ingredients);
        setFoodToEdit(null);
        setHasAtLeastOneModification(false);
        setSubfoodModalInfo({ isOpen: false });
        setSelectFoodToEditModalInfo({ isOpen: false });
    }
    
    /* Create or update an ingredient/subfood (modal) */
    const [subfoodModalInfo, setSubfoodModalInfo] = React.useState({ isOpen: false });

    // list of existing ingredient codes which cannot be re-added through CreateSubfood or UpdateSubfood
    const existingIngredientCodes = form.ingredientsAndQty.value
        .map(x => x.subfood.code)
        .filter(x => x !== subfoodModalInfo.ingredientAndQtyToUpdate?.subfood.code);

    function goCreateSubfood() {
        setSubfoodModalInfo({ isOpen: true });
    }

    function goUpdateSubfood({ idx, ingredientAndQtyToUpdate }) {
        setSubfoodModalInfo({
            isOpen: true,
            ingredientAndQtyToUpdate: ingredientAndQtyToUpdate,
            subfoodIdxToUpdate: idx,
        });
    }

    async function doCreateSubfood({ subfoodCode, quantityInGrams }) {
        const subfoodsSingle = [{ subfoodCode: subfoodCode, quantityInGrams: quantityInGrams }];
        doCreateSubfoods(subfoodsSingle);
    }

    async function doCreateSubfoods(subfoodCodeAndQtyInGramList /* [{ subfoodCode, quantityInGrams }] */) {
        const fullFoods = await FoodStore.Read({ codes: subfoodCodeAndQtyInGramList.map(x => x.subfoodCode) });
        const newSubfoods = fullFoods
            .map(food => ({
                subfood: food,
                // Note about complexity: subfoodCodeAndQtyInGramList will never be very large
                quantityInGrams: subfoodCodeAndQtyInGramList.find(x => x.subfoodCode === food.code).quantityInGrams,
            }));
        const updatedSubfoods = [...form.ingredientsAndQty.value, ...newSubfoods];
        const sortedSubfoods = updatedSubfoods.sort((a, b) => b.quantityInGrams - a.quantityInGrams)
        const nutrientsAndNominalQuantityUpdate = getFormUpdateFromIngredients(sortedSubfoods);
        setHasAtLeastOneModification(true);
        setForm({
            ingredientsAndQty: sortedSubfoods,
            ...nutrientsAndNominalQuantityUpdate,
        });
        setSubfoodModalInfo({ isOpen: false });
    }

    async function doUpdateSubfood({ subfoodIdxToUpdate, subfoodCode, quantityInGrams }) {
        const fullFood = await FoodStore.Read({ code: subfoodCode });
        const newSubfood = {
            subfood: fullFood,
            quantityInGrams: quantityInGrams,
        };
        const newSubfoods = [
            ...form.ingredientsAndQty.value.slice(0, subfoodIdxToUpdate),
            newSubfood,
            ...form.ingredientsAndQty.value.slice(subfoodIdxToUpdate + 1),
        ];
        const sortedSubfoods = newSubfoods.sort((a, b) => b.quantityInGrams - a.quantityInGrams);
        const nutrientsAndNominalQuantityUpdate = getFormUpdateFromIngredients(sortedSubfoods);
        setForm({
            ingredientsAndQty: sortedSubfoods,
            ...nutrientsAndNominalQuantityUpdate,
        });
        setSubfoodModalInfo({ isOpen: false });
    }

    function doRemoveSubfood(idx) {
        const updatedSubfoods = [
            ...form.ingredientsAndQty.value.slice(0, idx),
            ...form.ingredientsAndQty.value.slice(idx + 1),
        ];
        const nutrientsAndNominalQuantityUpdate = getFormUpdateFromIngredients(updatedSubfoods);
        if (updatedSubfoods.length === 0) {
            setHasAtLeastOneModification(false);
        }
        setForm({
            ingredientsAndQty: updatedSubfoods,
            ...nutrientsAndNominalQuantityUpdate,
        });
    }

    /* Toggle between adding a new food and editing an existing food */
    const [selectFoodToEditModalInfo, setSelectFoodToEditModalInfo] = React.useState({ isOpen: false });

    function goAddNewFood() {
        const url = getPageUrl();
        resetState();
        history.push(url);
    }

    function goEditExistingFood() {
        setSelectFoodToEditModalInfo({ isOpen: true });
    }

    function doSetFoodCodeToEdit(code) {
        // const url = getPageUrl({ code: 1680650773013 });
        const url = getPageUrl({ code: code });
        const currentUrl = `${history.location.pathname}${history.location.search}`;
        if (url !== currentUrl) {
            resetState();
            history.push(url);
        }
    }

    /* Main form actions */
    async function doSaveNewFood() {
        resetFormErrors();
        const isValid = validateForm();
        if (!isValid) {
            return;
        }

        // ensure the food label doesn't already exist
        const existingFoods = await FoodStore.Read();
        const doesNormalisedFoodLabelAlreadyExist = existingFoods
            .map(food => StrUtils.normaliseStr(food.label))
            .includes(StrUtils.normaliseStr(form.label.value));
        if (doesNormalisedFoodLabelAlreadyExist) {
            setFormErrors({
                label: `This food label already exists`,
            });
            return;
        }

        // ensure the user has set at least one value at some point
        if (!hasAtLeastOneModification) {
            addFeedback({
                label: `No modification has been made`,
                aspect: 'info',
            });
            return;
        }

        const newFoodCode = FoodAdapter.getNewCode();

        // extract all the nutrients already calculated in the form
        const nutrients = Object.values(FoodMetadata.All).reduce(
            (acc, cur) => ({
                ...acc,
                [cur.id]: form[cur.id].value,
            }),
            /* initialValue: */ {});
        // construct the updated food
        const newFood = {
            code: newFoodCode,
            label: form.label.value,
            nominal_quantity: Number(form.nominalQuantity.value),
            category: FoodCategory.composed.id,
            data_source: FoodDataSource.user,
            ...nutrients,
        };
        // construct the relations of the composed new food
        const totalQuantity = form.ingredientsAndQty.value.reduce((acc, cur) => acc + cur.quantityInGrams, 0);
        const relations = form.ingredientsAndQty.value
            .map(ingredient => ({
                superfood_code: newFoodCode,
                subfood_code: ingredient.subfood.code,
                quantity_abs: ingredient.quantityInGrams,
                quantity_pct: Number(ingredient.quantityInGrams) * 100 / Number(totalQuantity),
            }));
        // insert the values in the local database
        const successFeedbackMessage = 'The food has been created';
        await FoodStore.Create({ item: newFood });
        await FoodRelationsStore.Create({ items: relations });

        // reset the form after a successful submission
        addFeedback({
            label: successFeedbackMessage,
            aspect: 'success',
        });
        resetState();
    }

    async function doEditFood() {
        resetFormErrors();
        const isValid = validateForm();
        if (!isValid) {
            return;
        }

        const isEditingExistingFood = Boolean(foodToEdit);

        // ensure the user has set at least one value at some point
        if (!hasAtLeastOneModification) {
            addFeedback({
                label: `No modification has been made`,
                aspect: 'info',
            });
            return;
        }

        const editedFoodCode = foodToEdit.code;

        // extract all the nutrients already calculated in the form
        const nutrients = Object.values(FoodMetadata.All).reduce(
            (acc, cur) => ({
                ...acc,
                [cur.id]: form[cur.id].value,
            }),
            /* initialValue: */ {});
        // construct the updated food
        const newFood = {
            code: editedFoodCode,
            label: form.label.value,
            nominal_quantity: Number(form.nominalQuantity.value),
            category: FoodCategory.composed.id,
            data_source: FoodDataSource.user,
            ...nutrients,
        };
        // construct the relations of the composed new food
        const totalQuantity = form.ingredientsAndQty.value.reduce((acc, cur) => acc + cur.quantityInGrams, 0);
        const relations = form.ingredientsAndQty.value
            .map(ingredient => ({
                superfood_code: editedFoodCode,
                subfood_code: ingredient.subfood.code,
                quantity_abs: ingredient.quantityInGrams,
                quantity_pct: Number(ingredient.quantityInGrams) * 100 / Number(totalQuantity),
            }));
        // insert the values in the local database
        if (isNaN(foodToEdit.code) || isNaN(foodToEdit.id)) {
            console.error(`Trying to edit an existing food which was not fully inflated: ${foodToEdit}`);
            return;
        }
        newFood.id = foodToEdit.id;
        await FoodStore.Update({ updatedItem: newFood });
        await FoodRelationsStore.Delete({ superfood_code: foodToEdit.code });
        await FoodRelationsStore.Create({ items: relations });
        // reset the form after a successful submission
        const successFeedbackMessage = 'The food has been edited';
        addFeedback({
            label: successFeedbackMessage,
            aspect: 'success',
        });
        resetState();
    }

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '16px',
            width: '100%',
            maxWidth: '1000px',
            alignSelf: 'center',
            backgroundColor: css.colour.background.dp1,
            border: `solid 1px ${css.colour.border}`,
            borderRadius: '1px',
            height: 'fit-content',
            padding: '16px',
        }}>
            <SetIngredientModal
                title={subfoodModalInfo.ingredientAndQtyToUpdate !== undefined ? 'Modify an ingredient' : 'Add an ingredient'}
                isOpen={subfoodModalInfo.isOpen}
                onClose={() => { setSubfoodModalInfo({ isOpen: false }); }}
                onSubmit={subfoodModalInfo.ingredientAndQtyToUpdate !== undefined
                    ? (params) => doUpdateSubfood({ subfoodIdxToUpdate: subfoodModalInfo.subfoodIdxToUpdate, ...params })
                    : doCreateSubfood}
                existingIngredientCodes={existingIngredientCodes}
                subfoodToUpdate={subfoodModalInfo.ingredientAndQtyToUpdate?.subfood}
                quantityInGramsToUpdate={subfoodModalInfo.ingredientAndQtyToUpdate?.quantityInGrams}
            />
            <SetFoodButtonBar
                foodLabelToEdit={foodToEdit?.label}
                onAddNewFood={goAddNewFood}
                onEditFood={goEditExistingFood}
            />

            <SelectFoodToEditModal
                isOpen={selectFoodToEditModalInfo.isOpen}
                onSubmit={doSetFoodCodeToEdit}
                onClose={() => { setSelectFoodToEditModalInfo({ isOpen: false }); }}
            />

            <form style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                height: '100%',
                width: '100%',
                maxWidth: '100%',
            }}>

                <SetFoodMain
                    form={form}
                    setForm={setForm}
                />
                
                <SetFoodIngredientsInfo />

                <SetFoodNutrientTabs
                    selected={selectedTab}
                    onSelect={setSelectedTab}
                />
                {selectedTab === FoodAddTabEnum.ingredients && <>

                    <SetFoodIngredients
                        ingredientsAndQty={form.ingredientsAndQty.value}
                        onCreateSubfood={goCreateSubfood}
                        onUpdateSubfood={goUpdateSubfood}
                        onRemoveSubfood={doRemoveSubfood}
                        onClearSubfoodsOnly={() => { setForm({ ingredientsAndQty: [] }); }}
                    />
                </>}
                {selectedTab === FoodAddTabEnum.macronutrients &&
                    <SetFoodMacronutrients
                        form={form}
                        setForm={doSetFormAndRegisterNutrientModification}
                    />
                }
                {selectedTab === FoodAddTabEnum.vitamins &&
                    <SetFoodMicronutrients
                        form={form}
                        setForm={doSetFormAndRegisterNutrientModification}
                        micronutrientsMetadata={FoodMetadata.Vitamins}
                    />
                }
                {selectedTab === FoodAddTabEnum.minerals &&
                    <SetFoodMicronutrients
                        form={form}
                        setForm={doSetFormAndRegisterNutrientModification}
                        micronutrientsMetadata={FoodMetadata.Minerals}
                    />
                }
                <div style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    gap: '16px',
                }}>
                    <Button
                        onClick={() => Boolean(foodToEdit) ? doEditFood() : doSaveNewFood()}
                        aspect="info"
                        variant="outlined"
                        icon="Save"
                        children="Save"
                    />
                </div>
            </form>
        </div>

    </>);
}

/// Returns an object which can be given to the `setForm` hook and updates every field
/// with the food's nutrient values.
function getFormUpdateFromFoodWithoutIngredients(food) {
    const formUpdate = {
        nominalQuantity: 100,
        ...Object.values(FoodMetadata.All).reduce(
            (acc, cur) => ({ ...acc, [cur.id]: Number(food[cur.id]?.toFixed(2)) || 0 }),
            /* initialValue: */ {}),
    };
    return formUpdate;
}

/// Returns an object which can be given to the `setForm` hook and updates every field
/// based on ingredients and their respective quantities.
function getFormUpdateFromIngredients(
    ingredientsAndQty, // [{ food: {...}, quantityInGrams: Number(...) }]
) {
    if (!Array.isArray(ingredientsAndQty) || ingredientsAndQty.length === 0) {
        return {
            nominalQuantity: 100,
            ...Object.values(FoodMetadata.All).reduce((acc, cur) => ({ ...acc, [cur.id]: 0 }), {}),
        };
    }
    // calculate the nominal quantity
    const nominalQuantity = ingredientsAndQty.reduce((acc, cur) => acc + cur.quantityInGrams, 0);
    // Create a "meal" from the ingredients, ponderated by their respective quantity,
    // compatible with the FoodAdapter.mealToFood option, for convenience.
    // Note that the main difference here between what is called a meal and what is called a food
    // is that nutrients values are meant for 100g for foods, whereas meals come with a quantity.
    const consolidatedMeal = {
        quantity: nominalQuantity,
        ...Object.values(FoodMetadata.All).reduce(
            (acc, cur) => consolidateOneMealFromAllIngredientsForAllNutrientsReducer(ingredientsAndQty, acc, cur),
            /* initialValue: */ {}),
    };
    // create a food from the meal
    const newFood = FoodAdapter.mealToFood(consolidatedMeal);
    // create an object which can directly be passed to the `setForm` hook
    const formUpdate = {
        nominalQuantity: nominalQuantity,
        // take the new food's nutrient fields, which have been
        // normalised per 100g, and add them in this object
        ...Object.values(FoodMetadata.All).reduce(
            (acc, cur) => ({ ...acc, [cur.id]: Number(newFood[cur.id]?.toFixed(2)) || 0 }),
            /* initialValue: */ {}),
    };
    return formUpdate;

    function consolidateOneMealFromAllIngredientsForAllNutrientsReducer(ingredientsAndQty, accMealNutrientFields, curNutrient) {
        return {
            ...accMealNutrientFields,
            [curNutrient.id]: ingredientsAndQty.reduce(consolidateOneNutrientSumForAllIngredientsReducer, 0),
        };

        function consolidateOneNutrientSumForAllIngredientsReducer(sumCurrentNutrientForConsolidatedMeal, curIngredientAndQty) {
            return sumCurrentNutrientForConsolidatedMeal + ((Number(curIngredientAndQty.subfood[curNutrient.id]) || 0) * Number(curIngredientAndQty.quantityInGrams) / 100);
        }
    }

}

