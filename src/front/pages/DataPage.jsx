// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as FoodStore from 'repository/StoreFood';
// project components
import Button from 'front/components/Button';
import FoodDataSource from 'model/FoodDataSource';

export default function DataPage({ match, history }) {
    /* Context */
    const { isFoodDatabaseEmpty, setFoodDatabaseEmpty } = React.useContext(Contexts.IsFoodDatabaseEmptyContext);

    /* Set the app's title using the dedicated context */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    React.useEffect(() => { setAppTitle('Data management'); }, []);

    /* Variables */
    const [foods, setFoods] = React.useState([]);
    const [isLoading, setLoading] = React.useState(true);

    /* Load the recipe list */
    function foodsInitialisationEffect() {
        function doCatch(error) {
            setFoods([]);
            setLoading(false);
            console.warn(`Catching foodsInitialisationEffect: '${error}'`);
        }
        let isMounted = true;
        FoodStore
            .Read()
            .then((foods) => isMounted && setFoods(foods.splice(0, 5)))
            .then(() => isMounted && setLoading(false))
            .catch(doCatch);
        return () => { isMounted = false; };
    };
    React.useEffect(foodsInitialisationEffect, [isFoodDatabaseEmpty]);

    /* Submission methods */
    async function doClearFoodDatabase() {
        await FoodStore.Delete({
            data_source: FoodDataSource.ciqual,
        });
        const foods = await FoodStore.Read({
            data_source: FoodDataSource.user,
        });
        setFoods(foods || []);
        setLoading(false);
        setFoodDatabaseEmpty(true);
    }

    return (
        <>
            {isLoading &&
                <span>Loading...</span>
            }
            {!isLoading && foods && foods.length > 0 &&
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    spacing: 3,
                }}>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                    }}>
                        <Button
                            onClick={() => { doClearFoodDatabase(); }}
                            aspect="error"
                            label="Clear the food database"
                            icon="Cross"
                        />
                    </div>
                    <div>
                        <p>
                            Here are some of the food elements in your browser's database:
                        </p>
                        {/* Revert the fact that ul, li etc. have unset padding and before:* etc. */}
                        <ul style={{ all: 'revert' }}>
                            {foods.map(_food =>
                                <li key={_food.id}>{_food.label}</li>
                            )}
                        </ul>
                    </div>
                </div>
            }
        </>

    );
}
