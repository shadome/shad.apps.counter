// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import IngredientsButtonsBar from './SetFoodIngredients/IngredientsButtonsBar';
import IngredientsRow from './SetFoodIngredients/IngredientsRow';

export default function SetFoodIngredients({
    ingredientsAndQty, // [{ subfood: {...}, quantityInGrams: Number(...) }]
    onCreateSubfood = () => {},
    onRemoveSubfood = ({ idx }) => {},
    onUpdateSubfood = ({ idx, ingredientAndQtyToUpdate }) => {},
    onClearSubfoodsOnly = () => {},
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);

    return (<>
        <div style={{
            borderRadius: '1px',
            border: `1px solid ${css.colour.border}`,
            padding: '8px',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            cursor: 'default', // avoid cursor: text on pure html text
            minWidth: 0, // trick required for flex + text ellipsis - https://stackoverflow.com/questions/29947245/css-ellipsis-inside-flex-item
        }}>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: '4px',
                width: '100%',
            }}>
                <span style={{
                    fontSize: css.size.font.xs,
                    fontWeight: '600',
                    color: css.colour.text.soft,
                }}>
                    Food ingredients (optional)
                </span>
                <IngredientsButtonsBar
                    onCreateSubfood={onCreateSubfood}
                    onClearSubfoodsOnly={onClearSubfoodsOnly}
                />
            </div>
            {ingredientsAndQty.map((ingredientAndQty, idx) =>
                <IngredientsRow
                    idx={idx}
                    key={ingredientAndQty.subfood.code}
                    ingredientAndQty={ingredientAndQty}
                    onUpdateSubfood={onUpdateSubfood}
                    onRemoveSubfood={onRemoveSubfood}
                 />
                
            )}
            {ingredientsAndQty.length === 0 &&
                <span style={{
                    margin: '0px 8px 8px 8px', // additional margin as the padding of the fieldset is also affecting the title, which shouldn't be aligned
                    color: css.colour.text.soft,
                    fontStyle: 'italic',
                }}>
                    No items
                </span>
            }
        </div>
    </>);
}
