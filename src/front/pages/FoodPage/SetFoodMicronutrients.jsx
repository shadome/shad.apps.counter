// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import NutrientInput from './common/NutrientInput';

export default function SetFoodMicronutrients({
    form,
    setForm = () => { },
    micronutrientsMetadata = [],
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const columnNbs = isMobileDisplay ? [0] : [0, 1, 2];

    return (<>
        <div style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            gap: '8px',
        }}>
            {columnNbs.map(colNb =>
                <div
                    key={colNb}
                    style={{
                        width: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '8px',
                    }}
                >
                    {micronutrientsMetadata.map((nutrientMetadata, idx) => idx % columnNbs.length === colNb &&
                        <NutrientInput
                            key={nutrientMetadata.id}
                            nutrientMetadata={nutrientMetadata}
                            form={form}
                            setForm={setForm}
                        />
                    )}
                </div>
            )}
        </div>
        <span style={{
            color: css.colour.text.soft,
            fontStyle: 'italic',
            fontSize: css.size.font.sm,
        }}>
            The Recommended Daily Allowance (RDA) is a nutrient intake recommendation established by various national and international organizations, including the Food and Nutrition Board of the National Academy of Medicine in the United States. RDAs are meant to provide a general guideline for the amount of each nutrient that an average healthy adult needs to consume daily to maintain good health, although individual needs may vary based on factors such as age, sex, and activity level.
        </span>
    </>); 
}
