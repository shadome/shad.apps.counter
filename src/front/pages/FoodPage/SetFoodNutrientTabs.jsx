import * as React from 'react';
// project dependencies
import * as Contexts from 'context';

export const FoodAddTabEnum = {
    ingredients: 1,
    macronutrients: 2,
    vitamins: 3,
    minerals: 4,
};

export default function SetFoodNutrientTabs({
    selected = FoodAddTabEnum.ingredients,
    onSelect = () => {},
}) {
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);
    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
        }}>
            <button
                className="hov-bg"
                onClick={() => { onSelect(FoodAddTabEnum.ingredients); }}
                style={{
                    cursor: selected === FoodAddTabEnum.ingredients ? 'default' : 'pointer',
                    height: '40px',
                    width: '100%',
                    padding: '6px',
                    backgroundColor: selected === FoodAddTabEnum.ingredients && css.overlay.selected,
                    // From the console: Warning: Updating a style property during rerender (border)
                    // when a conflicting property is set(borderLeft) can lead to styling bugs.
                    // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
                    // instead, replace the shorthand with separate values.
                    borderTop: `1px solid ${css.colour.border}`,
                    borderBottom: `1px solid ${css.colour.border}`,
                    borderLeft: `1px solid ${css.colour.border}`,
                    borderRight: `none`,
                    borderTopLeftRadius: '1px',
                    borderBottomLeftRadius: '1px',
                    // text
                    textAlign: 'center',
                    fontSize: isMobileDisplay ? css.size.font.xs : css.size.font.sm,
                    fontWeight: 'bolder',
                }}
                children="Ingredients"
            />
            <button
                className="hov-bg"
                onClick={() => { onSelect(FoodAddTabEnum.macronutrients); }}
                style={{
                    cursor: selected === FoodAddTabEnum.macronutrients ? 'default' : 'pointer',
                    height: '40px',
                    width: '100%',
                    padding: '6px',
                    backgroundColor: selected === FoodAddTabEnum.macronutrients && css.overlay.selected,
                    // From the console: Warning: Updating a style property during rerender (border)
                    // when a conflicting property is set(borderLeft) can lead to styling bugs.
                    // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
                    // instead, replace the shorthand with separate values.
                    borderTop: `1px solid ${css.colour.border}`,
                    borderBottom: `1px solid ${css.colour.border}`,
                    borderLeft: `1px solid ${css.colour.border}`,
                    borderRight: `none`,
                    borderTopLeftRadius: '1px',
                    borderBottomLeftRadius: '1px',
                    // text
                    textAlign: 'center',
                    fontSize: isMobileDisplay ? css.size.font.xs : css.size.font.sm,
                    fontWeight: 'bolder',
                }}
                children="Macronutrients"
            />
            <button
                className="hov-bg"
                onClick={() => { onSelect(FoodAddTabEnum.vitamins); }}
                style={{
                    cursor: selected === FoodAddTabEnum.vitamins ? 'default' : 'pointer',
                    height: '40px',
                    width: '100%',
                    padding: '6px',
                    backgroundColor: selected === FoodAddTabEnum.vitamins && css.overlay.selected,
                    // From the console: Warning: Updating a style property during rerender (border)
                    // when a conflicting property is set(borderLeft) can lead to styling bugs.
                    // To avoid this, don't mix shorthand and non-shorthand properties for the same value;
                    // instead, replace the shorthand with separate values.
                    borderTop: `1px solid ${css.colour.border}`,
                    borderBottom: `1px solid ${css.colour.border}`,
                    borderLeft: `1px solid ${css.colour.border}`,
                    borderRight: `none`,
                    borderTopLeftRadius: '1px',
                    borderBottomLeftRadius: '1px',
                    // text
                    textAlign: 'center',
                    fontSize: isMobileDisplay ? css.size.font.xs : css.size.font.sm,
                    fontWeight: 'bolder',
                }}
                children="Vitamins"
            />
            <button
                className="hov-bg"
                onClick={() => { onSelect(FoodAddTabEnum.minerals); }}
                style={{
                    cursor: selected === FoodAddTabEnum.minerals ? 'default' : 'pointer',
                    height: '40px',
                    width: '100%',
                    padding: '6px',
                    backgroundColor: selected === FoodAddTabEnum.minerals && css.overlay.selected,
                    border: `1px solid ${css.colour.border}`,
                    // text
                    textAlign: 'center',
                    fontSize: isMobileDisplay ? css.size.font.xs : css.size.font.sm,
                    fontWeight: 'bolder',
                }}
                children="Minerals"
            />
        </div>
    </>);
};