// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as FoodMetadata from 'model/FoodMetadata';
// project components
import NutrientInput from './common/NutrientInput';

export default function SetFoodMacronutrients({
    form,
    setForm = () => {},
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const columnNbs = isMobileDisplay ? [0] : [0, 1, 2];

    return (<>
        <div style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
        }}>
            <span style={{
                padding: '8px',
                color: css.colour.text.soft,
                fontSize: css.size.font.sm,
                fontWeight: 'bold',
            }}>
                Main macronutrients
            </span>
            <div style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                gap: '8px',
            }}>
                {columnNbs.map(colNb =>
                    <div
                        key={colNb}
                        style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'column',
                            gap: '8px',
                        }}
                    >
                        {FoodMetadata.MainMacronutrients.map((nutrientMetadata, idx) => idx % columnNbs.length === colNb &&
                            <NutrientInput
                                key={nutrientMetadata.id}
                                nutrientMetadata={nutrientMetadata}
                                form={form}
                                setForm={setForm}
                            />
                        )}
                    </div>
                )}
            </div>
            <span style={{
                padding: '8px',
                color: css.colour.text.soft,
                fontSize: css.size.font.sm,
                fontWeight: 'bold',
            }}>
                Detailed macronutrients
            </span>
            <div style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'row',
                gap: '8px',
            }}>
                {columnNbs.map(colNb =>
                    <div
                        key={colNb}
                        style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'column',
                            gap: '8px',
                        }}
                    >
                        {FoodMetadata.DetailedMacronutrients.map((nutrientMetadata, idx) => idx % columnNbs.length === colNb &&
                            <NutrientInput
                                key={nutrientMetadata.id}
                                nutrientMetadata={nutrientMetadata}
                                form={form}
                                setForm={setForm}
                            />
                        )}
                    </div>
                )}
            </div>
        </div>
    </>);
}
