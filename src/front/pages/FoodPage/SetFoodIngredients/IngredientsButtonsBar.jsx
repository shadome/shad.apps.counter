// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
// project components
import Button from 'front/components/Button';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';

export default function IngredientsButtonsBar({
    onCreateSubfood = () => {},
    onClearSubfoodsOnly = () => {},
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row-reverse',
            flexWrap: 'wrap',
            flexGrow: 1,
            alignItems: 'flex-start',
            gap: '8px',
        }}>
            <Dropdown
                triggerComponent={<Button />}
                variant="text"
                icon="EllipsisVertical"
                tooltipProps={{ anchor: 'bottomleft' }}
                style={{
                    padding: '4px',
                    color: css.colour.text.regular,
                    fontWeight: 'bold'
                }}
            >
                <DropdownItem
                    variant="text"
                    title="Clear the ingredients list but do not reset the nutrients values.\nThis option is particularly useful when it is convenient to use one or more existing food's nutrients values for creating a new base food."
                    onClick={onClearSubfoodsOnly}
                    style={{
                        display: 'inline-flex',
                        alignItems: 'center',
                        gap: '8px',
                    }}
                >
                    <Icons.Delete style={{
                        height: css.size.icons.sm,
                        width: css.size.icons.sm,
                    }} />
                    <span>Clear but keep nutrients</span>
                </DropdownItem>
            </Dropdown>
            <Button
                variant="text"
                icon="Plus"
                label={isMobileDisplay ? 'Add' : 'Add an ingredient'}
                onClick={onCreateSubfood}
                style={{
                    padding: '4px',
                    color: css.colour.text.regular,
                    fontWeight: 'bold'
                }}
                spanStyle={{ textTransform: 'none' }}
            />
        </div>
    </>);
}
