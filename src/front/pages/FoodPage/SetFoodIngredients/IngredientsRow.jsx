// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
// project components
import Button from 'front/components/Button';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';

export default function IngredientsRow({
    // 0-based index of the row
    idx,
    ingredientAndQty, // { subfood: {...}, quantityInGrams: Number(...) }
    onRemoveSubfood = ({ idx }) => { },
    onUpdateSubfood = ({ idx, ingredientAndQtyToUpdate }) => { },
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            gap: '16px',
            width: '100%',
            marginLeft: '8px', // additional margin as the padding of the fieldset is also affecting the title, which shouldn't be aligned
        }}>
            <span style={{
                flexGrow: 1,
                fontSize: css.size.font.md,
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',

            }}>
                {ingredientAndQty.subfood.label}
            </span>
            <span style={{
                color: css.colour.text.soft,
                fontStyle: 'italic',
                fontSize: css.size.font.sm,
            }}>
                {ingredientAndQty.quantityInGrams}g
            </span>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                padding: '2px',
            }}>
                <Dropdown
                    triggerComponent={<Button />}
                    variant="text"
                    icon="EllipsisVertical"
                    tooltipProps={{ anchor: 'bottomleft' }}
                    style={{
                        padding: '8px',
                        color: css.colour.text.regular,
                        minWidth: css.size.icons.sm,
                        minHeight: css.size.icons.sm,
                        fontWeight: 'bold'
                    }}
                >
                    <DropdownItem
                        variant="text"
                        title="Update this ingredient"
                        onClick={_ => onUpdateSubfood({ idx: idx, ingredientAndQtyToUpdate: ingredientAndQty })}
                        style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                            gap: '8px',
                        }}
                    >
                        <Icons.Edit style={{
                            height: css.size.icons.sm,
                            width: css.size.icons.sm,
                        }} />
                        <span style={{ flexGrow: 1 }}>Edit</span>
                    </DropdownItem>
                    <DropdownItem
                        variant="text"
                        title="Remove this ingredient"
                        onClick={_ => onRemoveSubfood(idx)}
                        style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                            gap: '8px',
                        }}
                    >
                        <Icons.Delete style={{
                            height: css.size.icons.sm,
                            width: css.size.icons.sm,
                        }} />
                        <span style={{ flexGrow: 1 }}>Remove</span>
                    </DropdownItem>
                </Dropdown>
            </div>
        </div>
    );
}
