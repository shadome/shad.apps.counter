// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as FoodStore from 'repository/StoreFood';
import * as Throttle from 'utils/throttle';
import * as StringUtils from 'utils/strings';
// project components
import DualButton from 'front/components/DualButton';
import Button from 'front/components/Button';
import Drawer from 'front/components/Drawer';
import LabelledInput from 'front/components/LabelledInput';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';
import useForm from 'utils/form';

const NB_OF_DROPDOWN_ENTRIES = 50;

export default function SetFoodSelectionPanel({
    // undefined for no filters, model.FoodDataSource value otherwise
    foodDataSourceFilter = undefined,
    isAddingNewFood = false,
    selectedFoodCode = undefined,
    onAddNewFood = () => {},
    onSelectFood = ({ code }) => {},
}) {
    /* Context */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);

    // value of the input of the autocomplete component (input of the user)
    const [foodInput, setFoodInput] = React.useState('');
    // all the items proposed in the dropdown, unfiltered
    const [allItems, setAllItems] = React.useState([]);
    // filtered items proposed in the dropdown at a point in time, taking the current input into account
    const [items, setItems] = React.useState([]);

    /* Handle autocomplete changes with throttle logic */
    async function _onFoodInputChange(newValue) {
        const matchingEntries = StringUtils.searchStrings({
            entries: allItems.map(x => x.label),
            searchStr: newValue,
            topResults: NB_OF_DROPDOWN_ENTRIES,
        });
        // get the sorted labels and get back the associated items ({code, label})
        const matchingItems = matchingEntries.reduce(
            (buffer, current) => [...buffer, allItems.filter(x => x.label === current)[0]],
            []);
        setItems(matchingItems);
    }
    const onFoodInputChange = Throttle.useThrottle(_ => _onFoodInputChange(foodInput), 500);
    React.useEffect(onFoodInputChange, [foodInput]);

    /* Load all the possible choices for the food dropdown */
    async function setDropdownItems() {
        let isMounted = true;
        const foods = await FoodStore.Read({ data_source: foodDataSourceFilter });
        if (isMounted) {
            const options = foods
                // remove irrelevant food data if necessary
                .filter(_food => !isNaN(_food.kcal))
                // remove duplicates (part 1)
                .map(_food => [_food.label, { code: _food.code, label: _food.label }]);
            // remove duplicates (part 2)
            const uniqueOptions = [...new Map(options).values()];
            setAllItems(uniqueOptions);
            setItems(uniqueOptions.slice(0, NB_OF_DROPDOWN_ENTRIES));
        }
        return () => { isMounted = false; };
    }
    React.useEffect(setDropdownItems, [foodDataSourceFilter]);


    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '1px',
            width: '300px',
            maxHeight: '100%',
            borderRight: `2px solid ${css.colour.border}`,
            backgroundColor: css.colour.background.dp1,
        }}>
            <button
                className="hov-bg"
                onClick={onAddNewFood}
                style={{
                    cursor: isAddingNewFood ? 'default' : 'pointer',
                    height: '40px',
                    width: '100%',
                    padding: '6px',
                    backgroundColor: isAddingNewFood && css.overlay.selected,
                    // border: `1px solid ${css.colour.border}`,
                    Radius: '1px',
                    // text
                    textAlign: 'center',
                    fontSize: css.size.font.sm,
                    fontWeight: 'bolder',
                }}
                children="Add a new food"
            />
            <LabelledInput
                style={{
                    height: '40px',
                }}
                value={foodInput}
                onChange={(evt) => { setFoodInput(evt.target.value); }}
                label="Search a food"
            />
            <div style={{
                maxHeight: '100%',
                overflowY: 'scroll',
            }}>
            {items.map(item => (
                <div
                    key={item.code}
                    className="hov-bg"
                    onClick={() => { onSelectFood({ code: item.code }); }}
                    style={{
                        cursor: selectedFoodCode === item.code  ? 'default' : 'pointer',
                        height: '40px',
                        width: '100%',
                        padding: '6px',
                        backgroundColor: selectedFoodCode === item.code && css.overlay.selected,
                        // border: `1px solid ${css.colour.border}`,
                        // borderRadius: '1px',
                        // text
                        // textAlign: 'center',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                        fontSize: css.size.font.sm,
                        // fontWeight: 'bolder',
                        color: css.colour.text.soft,
                    }}
                    title={item.label}
                    children={item.label}
                />
            ))}
            </div>
        </div>
    </>);
}