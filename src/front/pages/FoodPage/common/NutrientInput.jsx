// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import LabelledInput from 'front/components/LabelledInput';

export default function NutrientInput({
    form,
    setForm = () => {},
    nutrientMetadata,
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const rdaPctValue = nutrientMetadata.rdaLowerBound !== undefined
        ? Number(100 * form[nutrientMetadata.id].value / nutrientMetadata.rdaLowerBound).toFixed(0)
        : null;

    return (<>
        <LabelledInput
            label={`${nutrientMetadata.label} (${nutrientMetadata.unit.symbol} per 100g)`}
            value={form[nutrientMetadata.id].value}
            errorText={form[nutrientMetadata.id].error}
            onChange={(evt) => { setForm({ [nutrientMetadata.id]: evt.target.value }); }}
        >
            {rdaPctValue &&
                <span style={{
                    fontSize: css.size.font.sm,
                    color: css.colour.text.soft,
                }}>
                    {rdaPctValue}% RDA
                </span>
            }
        </LabelledInput>
    </>);
}
