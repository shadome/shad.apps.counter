// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';

export default function SetFoodIngredientsInfo() {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    const textSize = isMobileDisplay ? css.size.font.xs : css.size.font.sm;
    const iconSize = isMobileDisplay ? css.size.icons.xs : css.size.icons.sm;

    return (<>
        <div
            style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                backgroundColor: css.colour.aspect.info.background,
                color: css.colour.aspect.info.foreground,
                gap: '8px',
                padding: '8px',
                borderRadius: '1px',
                fontSize: textSize,
            }}
        >
            <Icons.CircledInformation style={{
                minWidth: iconSize,
                minHeight: iconSize,
                maxWidth: iconSize,
                maxHeight: iconSize,
            }} />
            <p style={{ flexGrow: 1 }}>
                Nutrients can be set directly, or pre-filled by selecting ingredients composing the food, which is the preferred way of specifying a food composed of other foods.
                Note that nutrients and the nominal quantity will be recalculated if an ingredient is added, modified or removed.
            </p>
        </div>
    </>);
}
