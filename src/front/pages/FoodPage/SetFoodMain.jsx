// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
// project components
import LabelledInput from 'front/components/LabelledInput';
import Tooltip from 'front/components/Tooltip';

export default function SetFoodMain({
    form,
    setForm = () => { },
}) {
    /* Set the app's title using the dedicated context */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    React.useEffect(() => { setAppTitle('Create a food'); }, []);

    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    const nominalQtyInputRef = React.useRef();
    const [isNominalQtyHelpOpen, setIsNominalQtyHelpOpen] = React.useState(false);

    return (<>
        <LabelledInput
            label="Food label"
            value={form.label.value}
            onChange={(evt) => { setForm({ label: evt.target.value }); }}
            errorText={form.label.error}
            tooltipProps={{
                tooltipAnchor: 'left',
                gap: 0,
                crossGap: -15,
            }}
        />
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            gap: '16px',
            alignItems: 'end'
        }}>
            <LabelledInput
                label="Nominal quantity (g)"
                value={form.nominalQuantity.value}
                onChange={(evt) => { setForm({ nominalQuantity: evt.target.value }); }}
                errorText={form.nominalQuantity.error}
                tooltipProps={{
                    tooltipAnchor: 'left',
                    gap: 0,
                    crossGap: -15,
                }}
            >
                <button
                    children={<Icons.CircularArrowLeft />}
                    className="hov-bg"
                    title="Reset to default"
                    onClick={() => { setForm({ nominalQuantity: 100 }); }}
                    style={{
                        width: css.size.icons.md,
                        height: css.size.icons.md,
                        cursor: 'pointer',
                        color: css.colour.aspect.info.foreground,
                    }}
                />
                <button
                    ref={nominalQtyInputRef}
                    children={<Icons.Help />}
                    className="hov-bg"
                    onClick={() => { setIsNominalQtyHelpOpen(true); }}
                    style={{
                        width: css.size.icons.md,
                        height: css.size.icons.md,
                        cursor: 'pointer',
                        color: css.colour.aspect.info.foreground,
                    }}
                />
                <Tooltip
                    open={isNominalQtyHelpOpen}
                    onClose={() => { setIsNominalQtyHelpOpen(false); }}
                    triggerComponentRef={nominalQtyInputRef}
                    anchor="bottomleft"
                    style={{
                        width: '500px',
                        maxWidth: '80%',
                        gap: '8px',
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <p style={{ fontWeight: 'bold' }}>Nominal quantity for the new food.</p>
                    <p>The nominal quantity is the quantity which will be pre-entered when adding a meal, 100g by default.</p>
                    <p>Setting a custom nominal quantity might be useful when the food has ingredients considered as recipe ingredients, in which case the food's weight will change after cooking.</p>
                    <p>For instance, if a cake's raw ingredients sum up to 500g, and you know you've eaten half of the baked cake, you can add a meal for 250g of cake, half of the nominal weight.</p>
                    <p style={{ color: css.colour.aspect.warning.foreground }}>Note that the nutritional information provided is always based on a 100g serving size, regardless of the nominal quantity.</p>
                </Tooltip>
            </LabelledInput>
        </div>
    </>);
}
