// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
// project components
import Button from 'front/components/Button';
import DropdownItem from 'front/components/DropdownItem';
import Dropdown from 'front/components/Dropdown';

export default function SetFoodButtonBar({
    foodLabelToEdit = '',
    onAddNewFood = () => { },
    onEditFood = () => { },
}) {
    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    // Note that the edit tooltip is composed of nested div with flex:row so that the inner ones will group elements which will not wrap
    // also, there are multiple flexGrow:1 so that when wrapping occurs, e.g., mobile display, every wrapped line will extend to 100% width
    return (<>
        {foodLabelToEdit ?
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                flexGrow: 1,
                alignItems: 'flex-start',
                gap: '8px',
            }}>
                <span style={{
                    padding: '8px',
                    color: css.colour.text.soft,
                    fontSize: css.size.font.sm,
                    fontWeight: 'bold',
                    flexGrow: 1,
                }}>
                    Editing <span style={{ color: css.colour.text.strong, fontStyle: 'italic' }}>{foodLabelToEdit}</span>
                </span>
                {isMobileDisplay ?
                    <Dropdown
                        triggerComponent={<Button />}
                        variant="text"
                        icon="EllipsisVertical"
                        tooltipProps={{ anchor: 'bottomleft' }}
                        style={{
                            padding: '4px',
                            color: css.colour.text.regular,
                            fontWeight: 'bold',
                        }}
                    >
                        <DropdownItem
                            variant="text"
                            onClick={onAddNewFood}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                            }}
                        >
                            <Icons.Plus style={{ height: css.size.icons.sm, width: css.size.icons.sm }} />
                            <span>Add a new food</span>
                        </DropdownItem>
                        <DropdownItem
                            variant="text"
                            onClick={onEditFood}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                            }}
                        >
                            <Icons.Edit style={{ height: css.size.icons.sm, width: css.size.icons.sm }} />
                            <span>Edit another food</span>
                        </DropdownItem>
                    </Dropdown>
                    :
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                        alignItems: 'center',
                        gap: '8px',
                        flexGrow: 1,
                    }}>
                        <Button
                            variant="text"
                            icon="Edit"
                            label="Edit another food"
                            onClick={onEditFood}
                            style={{
                                color: css.colour.text.regular,
                                fontWeight: 'bold',
                                padding: '4px',
                                width: 'fit-content',
                                minWidth: 'fit-content',
                            }}
                            spanStyle={{ textTransform: 'none' }}
                        />
                        <Button
                            variant="text"
                            icon="Plus"
                            label="Add a new food"
                            onClick={onAddNewFood}
                            style={{
                                color: css.colour.text.regular,
                                fontWeight: 'bold',
                                padding: '4px',
                                width: 'fit-content',
                                minWidth: 'fit-content',
                            }}
                            spanStyle={{ textTransform: 'none' }}
                        />
                    </div>
                }
            </div>
            :
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    flexGrow: 1,
                    alignItems: 'flex-start',
                    gap: '8px',
                }}
            >
                <span style={{
                    padding: '8px',
                    color: css.colour.text.soft,
                    fontSize: css.size.font.sm,
                    fontWeight: 'bold',
                    flexGrow: 1,
                }}>
                    Adding a new food
                </span>
                <Button
                    variant="text"
                    icon="Edit"
                    label="Edit an existing food"
                    onClick={onEditFood}
                    style={{
                        color: css.colour.text.regular,
                        fontWeight: 'bold',
                        padding: '4px 8px 4px 4px',
                    }}
                    spanStyle={{ textTransform: 'none' }}
                />
            </div>
        }

    </>);
}