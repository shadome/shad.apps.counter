// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as FoodStore from 'repository/StoreFood';
import * as Throttle from 'utils/throttle';
import * as StringUtils from 'utils/strings';
// project components
import Button from 'front/components/Button';
import Drawer from 'front/components/Drawer';
import LabelledInput from 'front/components/LabelledInput';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';
import useForm from 'utils/form';

const NB_OF_DROPDOWN_ENTRIES = 50;

export default function SelectFoodToEditModal({
    isOpen,
    onClose,
    onSubmit,
}) {
    /* Context */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);

    // value of the input of the autocomplete component (input of the user)
    // this is not a required part of the form, do not mix up with form.components.selectedFood
    // which is the last selected value from the dropdown list,
    // i.e., after the user has input a few words to find the right dropdown item
    const [foodInput, setFoodInput] = React.useState('');
    // all the items proposed in the dropdown, unfiltered
    const [allItems, setAllItems] = React.useState([]);
    // filtered items proposed in the dropdown at a point in time, taking the current input into account
    const [items, setItems] = React.useState([]);

    /* Handle autocomplete changes with throttle logic */
    async function _onFoodInputChange(newValue) {
        const matchingEntries = StringUtils.searchStrings({
            entries: allItems.map(x => x.label),
            searchStr: newValue,
            topResults: NB_OF_DROPDOWN_ENTRIES,
        });
        // get the sorted labels and get back the associated items ({code, label})
        const matchingItems = matchingEntries.reduce(
            (buffer, current) => [...buffer, allItems.filter(x => x.label === current)[0]],
            []);
        setItems(matchingItems);
    }
    const onFoodInputChange = Throttle.useThrottle(_ => _onFoodInputChange(foodInput), 1000);
    React.useEffect(onFoodInputChange, [foodInput]);

    /* Setup form variables and validation rules */
    const [form, setForm, validateForm, resetForm, resetFormErrors] = useForm({
        selectedFood: {
            value: null,
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'Select a food from the list',
            }],
        },
    });

    /* Setup an autofocus effect on the first input on mounting event. The way MUI enables this seems ugly, esp. the timer part. */
    const inputRef = React.useRef();
    function setFocusOnOpenEffect() {
        if (!isOpen) {
            return;
        }
        const timeout = setTimeout(focusAction, 10);
        return () => { clearTimeout(timeout); };

        function focusAction() {
            inputRef?.current && inputRef.current.focus();
        }
    }
    React.useEffect(setFocusOnOpenEffect, [isOpen]);

    /* Load all the possible choices for the food dropdown */
    async function setDropdownItems() {
        if (!isOpen) {
            return;
        }
        let isMounted = true;
        const foods = await FoodStore.Read();
        if (isMounted) {
            const options = foods
                // remove irrelevant food data if necessary
                .filter(_food => !isNaN(_food.kcal))
                // remove duplicates (part 1)
                .map(_food => [_food.label, { code: _food.code, label: _food.label }]);
            // remove duplicates (part 2)
            const uniqueOptions = [...new Map(options).values()];
            setAllItems(uniqueOptions);
            setItems(uniqueOptions.slice(0, NB_OF_DROPDOWN_ENTRIES));
        }
        return () => { isMounted = false; };
    }
    React.useEffect(setDropdownItems, [isOpen]);

    /* Form actions */
    async function doSubmit() {
        resetFormErrors();
        const isValid = validateForm();
        if (!isValid) {
            return;
        }
        const code = form.selectedFood.value.code;
        resetState();
        onSubmit(code);
    }

    function doClose() {
        resetState();
        onClose();
    }

    function resetState() {
        setFoodInput('');
        setItems([]);
        resetForm();
    }

    function doSelectFood(newValue) {
        setFoodInput(newValue.label);
        setForm({ selectedFood: newValue });
    }

    return !form ? null : (<>
        <Drawer
            open={isOpen}
            onClose={doClose}
            position={isMobileDisplay ? 'full' : 'center'}
            title={
                <span style={{
                    font: css.size.font.xl,
                    fontWeight: 'bold',
                }}>
                    Select the food to update
                </span>
            }
            style={{
                padding: '16px',
                width: '700px', // will be ignored if fullscreen (mobile display)
                height: 'fit-content', // will be ignored if fullscreen (mobile display)
                // required so that the tooltip of the dropdown is visible when it overflows
                // note that just specifying overflowY (X) won't do anything in firefox, for some reason
                overflow: 'visible',
            }}
        >
            {/* modal content + action row */}
            <form style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                height: '100%',
                width: '100%',
            }}>
                {/* modal content */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '16px',
                }}>
                    <Dropdown
                        onSelect={doSelectFood}
                        tooltipProps={{
                            style: {
                                overflowY: 'scroll',
                                maxHeight: '280px',
                            }
                        }}
                        triggerComponent={
                            <LabelledInput
                                ref={inputRef}
                                label="Food"
                                value={foodInput}
                                onChange={(evt) => { setFoodInput(evt.target.value); }}
                                errorText={form.selectedFood.error}
                            />
                        }
                    >
                        {items.map(_item =>
                            <DropdownItem key={_item.code} value={_item}>{_item.label}</DropdownItem>
                        )}
                    </Dropdown>
                </div>
                {/* modal action row */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    gap: '16px',
                }}>
                    <Button
                        icon="Send"
                        variant="contained"
                        onClick={() => { doSubmit(); }}
                        children="Select"
                        props={{
                            border: `1px solid ${css.colour.border}`,
                        }}
                    />
                    <Button
                        icon="Cross"
                        variant="outlined"
                        onClick={() => { doClose(); }}
                        children="Close"
                    />
                </div>
            </form>
        </Drawer>
    </>);
}