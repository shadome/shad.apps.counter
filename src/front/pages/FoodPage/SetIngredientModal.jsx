// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as FoodStore from 'repository/StoreFood';
import * as Throttle from 'utils/throttle';
import * as StringUtils from 'utils/strings';
// project components
import DualButton from 'front/components/DualButton';
import Button from 'front/components/Button';
import Drawer from 'front/components/Drawer';
import LabelledInput from 'front/components/LabelledInput';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';
import useForm from 'utils/form';

const NB_OF_DROPDOWN_ENTRIES = 50;

const Operations = {
    ADD: 1,
    SUB: 2,
};

// TODO catch 'Enter' on any input for form validation
// TODO catch 'Escape' for every modals
// TODO rework visuals for mobile: modals should probably be fit-content height, 100% width and vertically centered
export default function SetIngredientModal({
    title,
    isOpen,
    onClose,
    // existing subfood in case of modification, is undefined otherwise
    subfoodToUpdate,
    // existing food quantity in case of modification, is undefined otherwise
    quantityInGramsToUpdate,
    onSubmit,
    // existing codes which cannot be submitted again
    existingIngredientCodes,
}) {
    /* Context */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);

    /* Component constants */
    const quantityDualButtonValues = isMobileDisplay ? [15, 100] : [5, 30, 100];
    // holds a boolean value which changes when displayed errors should be ignored again
    const [errorResetFlag, setErrorResetFlag] = React.useState(false);

    // value of the input of the autocomplete component (input of the user)
    // this is not a required part of the form, do not mix up with form.components.selectedFood
    // which is the last selected value from the dropdown list,
    // i.e., after the user has input a few words to find the right dropdown item
    const [foodInput, setFoodInput] = React.useState('');
    // all the items proposed in the dropdown, unfiltered
    const [allItems, setAllItems] = React.useState([]);
    // filtered items proposed in the dropdown at a point in time, taking the current input into account
    const [items, setItems] = React.useState([]);

    /* Handle autocomplete changes with throttle logic */
    async function _onFoodInputChange(newValue) {
        const matchingEntries = StringUtils.searchStrings({
            entries: allItems.map(x => x.label),
            searchStr: newValue,
            topResults: NB_OF_DROPDOWN_ENTRIES,
        });
        // get the sorted labels and get back the associated items ({code, label})
        const matchingItems = matchingEntries.reduce(
            (buffer, current) => [...buffer, allItems.filter(x => x.label === current)[0]],
            []);
        setItems(matchingItems);
    }
    const onFoodInputChange = Throttle.useThrottle(_ => _onFoodInputChange(foodInput), 1000);
    React.useEffect(onFoodInputChange, [foodInput]);

    /* Setup form variables and validation rules */
    const [form, setForm, validateForm, resetForm, resetFormErrors] = useForm({
        selectedFood: {
            value: null,
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'Select a food from the list',
            }, {
                predicate: (val) => !existingIngredientCodes.includes(val.code),
                error: 'This ingredient is already in the ingredients list',
            }],
        },
        foodQuantityInGrams: {
            value: 100,
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'The quantity is required',
            }, {
                predicate: (val) => !isNaN(val) && Number(val) > 0 && Number.isInteger(Number(val)),
                error: 'The quantity should be a positive integer',
            }],
        },
    });

    /* Inflate the state variables if this is an 'update' rather than an 'create' */
    async function inflateStateVariablesFromExistingSubfoodEffect() {
        let isMounted = true
            && subfoodToUpdate !== undefined
            && quantityInGramsToUpdate !== undefined
            && form !== undefined;
        isMounted && inflateFormWithSubfoodData({
            food: subfoodToUpdate,
            quantityInGrams: quantityInGramsToUpdate,
        });
        return () => { isMounted = false; };

        function inflateFormWithSubfoodData({ food, quantityInGrams }) {
            // option which should be selected
            const foodValue = {
                code: food.code,
                label: food.label,
            };
            setFoodInput(food.label);
            setForm({
                selectedFood: foodValue,
                foodQuantityInGrams: quantityInGrams,
            });
        }
    }
    React.useEffect(inflateStateVariablesFromExistingSubfoodEffect, [subfoodToUpdate, quantityInGramsToUpdate]);

    /* Setup an autofocus effect on the first input on mounting event. The way MUI enables this seems ugly, esp. the timer part. */
    const inputRef = React.useRef();
    function setFocusOnOpenEffect() {
        if (!isOpen) {
            return;
        }
        const timeout = setTimeout(focusAction, 10);
        return () => { clearTimeout(timeout); };

        function focusAction() {
            // no autofocus if this is a MODIFICATION call
            !subfoodToUpdate && inputRef?.current && inputRef.current.focus();
        }
    }
    React.useEffect(setFocusOnOpenEffect, [isOpen]);

    /* Load all the possible choices for the food dropdown */
    async function setDropdownItems() {
        if (!isOpen) {
            return;
        }
        let isMounted = true;
        const foods = await FoodStore.Read();
        if (isMounted) {
            const options = foods
                // remove irrelevant food data if necessary
                .filter(_food => !isNaN(_food.kcal))
                // remove duplicates (part 1)
                .map(_food => [_food.label, { code: _food.code, label: _food.label }]);
            // remove duplicates (part 2)
            const uniqueOptions = [...new Map(options).values()];
            setAllItems(uniqueOptions);
            setItems(uniqueOptions.slice(0, NB_OF_DROPDOWN_ENTRIES));
        }
        return () => { isMounted = false; };
    }
    React.useEffect(setDropdownItems, [isOpen]);

    /* Form actions */
    async function doSubmit() {
        setErrorResetFlag(!errorResetFlag);
        resetFormErrors();
        const isValid = validateForm();
        if (!isValid) {
            return;
        }
        const subfoodCode = form.selectedFood.value.code;
        const quantityInGrams = Number(form.foodQuantityInGrams.value);
        resetState();
        onSubmit({ subfoodCode: subfoodCode, quantityInGrams: quantityInGrams });
    }

    function doClose() {
        resetState();
        onClose();
    }

    function resetState() {
        setFoodInput('');
        setItems([]);
        resetForm();
    }

    function doSelectFood(newValue) {
        setFoodInput(newValue.label);
        setForm({ selectedFood: newValue });
    }

    function doUpdateQuantity(operation, value) {
        const newQuantity = (
            /* case #1 */ operation === Operations.ADD ? Math.round((Number(form.foodQuantityInGrams.value) || value) + value) :
            /* case #2 */ operation === Operations.SUB ? Math.round((Number(form.foodQuantityInGrams.value) || value) - value) :
            /* default */ form.foodQuantityInGrams.value || 0
        );
        setForm({ foodQuantityInGrams: newQuantity });
    }

    return !form ? null : (<>
        <Drawer
            open={isOpen}
            onClose={doClose}
            position={isMobileDisplay ? 'full' : 'center'}
            title={
                <span style={{
                    font: css.size.font.xl,
                    fontWeight: 'bold',
                }}>
                    {title}
                </span>
            }
            style={{
                padding: '16px',
                width: '700px', // will be ignored if fullscreen (mobile display)
                height: 'fit-content', // will be ignored if fullscreen (mobile display)
                // required so that the tooltip of the dropdown is visible when it overflows
                // note that just specifying overflowY (X) won't do anything in firefox, for some reason
                overflow: 'visible',
            }}
        >
            {/* modal content + action row */}
            <form style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                height: '100%',
                width: '100%',
            }}>
                {/* modal content */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '16px',
                }}>
                    <Dropdown
                        onSelect={doSelectFood}
                        tooltipProps={{
                            style: {
                                overflowY: 'scroll',
                                maxHeight: '280px',
                            }
                        }}
                        triggerComponent={
                            <LabelledInput
                                ref={inputRef}
                                label="Food ingredient"
                                value={foodInput}
                                onChange={(evt) => { setFoodInput(evt.target.value); }}
                                errorText={form.selectedFood.error}
                                errorResetFlag={errorResetFlag}
                            />
                        }
                    >
                        {items.map(_item =>
                            <DropdownItem key={_item.code} value={_item}>{_item.label}</DropdownItem>
                        )}
                    </Dropdown>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        width: '100%',
                        gap: '8px',
                    }}>
                        <LabelledInput
                            label="Quantity (g)"
                            value={form.foodQuantityInGrams.value}
                            onChange={(evt) => { setForm({ foodQuantityInGrams: evt.target.value }); }}
                            errorText={form.foodQuantityInGrams.error}
                            errorResetFlag={errorResetFlag}
                        />
                        <div style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                            {quantityDualButtonValues.map(grams => (
                                <DualButton
                                    key={grams}
                                    onLeftButtonClick={() => { doUpdateQuantity(Operations.SUB, grams); }}
                                    onRightButtonClick={() => { doUpdateQuantity(Operations.ADD, grams); }}
                                    leftButtonProps={{ disabled: (Number(form.foodQuantityInGrams.value) || 0) < grams + 1 }}
                                    label={`${grams}g`}
                                />
                            ))}
                        </div>
                    </div>
                </div>
                {/* modal action row */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    gap: '16px',
                }}>
                    <Button
                        icon="Send"
                        variant="contained"
                        onClick={() => { doSubmit(); }}
                        children="Submit"
                        props={{
                            border: `1px solid ${css.colour.border}`,
                        }}
                    />
                    <Button
                        icon="Cross"
                        variant="outlined"
                        onClick={() => { doClose(); }}
                        children="Close"
                    />
                </div>
            </form>
        </Drawer>
    </>);
}