import * as React from 'react'

export default function NotFoundPage () {
  return (
    <div>
      <h1>404</h1>
      <p>Il semblerait que nous ne soyons pas en mesure d&apos;afficher cette page... Existe-t-elle réellement ?</p>
      <p><a href='/'>Retourner à l'accueil</a></p>
    </div>
  )
}
