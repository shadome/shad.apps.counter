// libraries
import * as React from 'react';
import * as dayjs from 'dayjs';
// project dependencies
import * as Contexts from 'context';
import * as MealStore from 'repository/StoreMeal';
import * as FoodMetadata from 'model/FoodMetadata';
import Pages from 'front/pages';
// project components
import * as Adapter from './DashboardPage/MealList/adapter';
import PrevNextDateBar from 'front/containers/date_bar/PrevNextDateBar';
import MealList from './DashboardPage/MealList';
import MacronutrientsSummaryComponent from './DashboardPage/MacronutrientsSummaryComponent';
import MicronutrientsSummary from './DashboardPage/MicronutrientsSummary';
import SetMealGroupModal from './DashboardPage/SetMealGroupModal';
import SetMealModal from './DashboardPage/SetMealModal';

export function getPageUrl(dayjsDate) {
    return `/dashboard/${dayjsDate.format('YYYY-MM-DD')}`;
}

export default function MealListPage({ match, history }) {
    /* Set the app's title using the dedicated context */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    React.useEffect(() => { setAppTitle('Daily meals'); }, []);

    /* Variables */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    // current day's meals
    const [meals, setMeals] = React.useState([]);
    // meals as used by the MealList component, with redundant groups
    // us handled here because of expandAll / collapseAll triggers
    const [mealVMs, setMealVMs] = React.useState([]);
    // list entries which are checked; null also means entry checking is disabled, whereas empty or non-empty array means it is enabled
    const [checkedMealIds, setCheckedMealIds] = React.useState(null);
    const [isLoadingMeals, setIsLoadingMeals] = React.useState(true);
    // individual meal group expansion / collapse is handled here because of the expandAll / collapseAll triggers
    const [expandedMealGroupLabels, setExpandedMealGroupLabels] = React.useState([]);

    /* Get the URL date to display or redirect to today if it is invalid */
    const [date, setDate] = React.useState();
    function handleUrlDateChangeEffect() {
        let isMounted = true;
        const _date = dayjs(match.params.date || null, 'YYYY-MM-DD');
        isMounted && _date.isValid() && setDate(_date);
        // note that the following statement MUST be valid regarding the previous statement to avoid infinite loops
        isMounted && !_date.isValid() && history.push(getPageUrl(dayjs()));
        return () => { isMounted = false; };
    };
    React.useEffect(handleUrlDateChangeEffect, [match.params.date]);

    /* Load the meal list for today */
    function mealsInitialisationEffect() {
        const doCatch = (error) => {
            setMeals([]);
            setIsLoadingMeals(false);
            console.warn(`Catching mealsInitialisationEffect: '${error}'`);
        };
        let isMounted = date !== undefined;
        isMounted && MealStore
            .Read({
                min_date_consumption: date.toDate(),
                max_date_consumption: date.add(1, 'd').toDate(),
            })
            .then((meals) => {
                if (!isMounted) return;
                setMeals(meals);
                setMealVMs(Adapter.getMealsViewModelFromModel(meals));
            })
            .then(() => isMounted && setIsLoadingMeals(false))
            .catch(doCatch);
        return () => { isMounted = false; };
    };
    React.useEffect(mealsInitialisationEffect, [date]);

    /* Submission methods */
    function doExpandAll() {
        const mealGroupLabels = mealVMs
            .filter(meal => meal.entries?.length > 0)
            .map(mealGroup => mealGroup.label);
        setExpandedMealGroupLabels(mealGroupLabels);
    }

    function doCollapseAll() {
        setExpandedMealGroupLabels([]);
    }

    async function doRemoveMealFromList({ mealId }) {
        await MealStore.Delete({ id: mealId });
        mealsInitialisationEffect();
    }

    function doToggleItemSelection() {
        // empty means selection enabled, null means selection disabled
        setCheckedMealIds(checkedMealIds === null ? [] : null);
    }

    function doItemCheckToggle(event, itemId) {
        event.stopPropagation(); // stop the bubbling which is proven to be slowing the app
        const idx = checkedMealIds.indexOf(itemId);
        const newValue = idx === -1
            ? [itemId, ...checkedMealIds]
            : [...checkedMealIds.slice(0, idx), ...checkedMealIds.slice(idx + 1)];
        setCheckedMealIds(newValue);
    }

    function goPrevDate() {
        setCheckedMealIds(null);
        history.push(getPageUrl(date.add(-1, 'd')));
    }

    function goNextDate() {
        setCheckedMealIds(null);
        history.push(getPageUrl(date.add(1, 'd')));
    }

    async function doSplitMealGroup({ mealIds }) {
        const updatedMeals = mealIds
            .map(_mealId => ({
                id: Number(_mealId),
                group_label: undefined,
            }));
        await MealStore.Update({
            updatedItems: updatedMeals,
        });
        setCheckedMealIds(null);
        mealsInitialisationEffect();
    }

    async function doCreateRecipeFromGroup({ mealGroup }) {
        const label = mealGroup.label;
        const foodCodesAndQuantities = mealGroup.entries
            .map(entry => ({
                c: entry.food_code,
                q: entry.quantity,
            }));
        const url = Pages.FoodPage.getUrl({
            label: label,
            foods: foodCodesAndQuantities,
        });
        history.push(url);
    }

    /* Create or update a meal group (modal) */
    const [mealGroupModalInfo, setMealGroupModalInfo] = React.useState({ isOpen: false });
    function doResetMealGroupModalState() {
        setMealGroupModalInfo({ isOpen: false });
    }

    function goUpdateMealGroup({ mealIds, label, time }) {
        const info = {
            isOpen: true,
            mealIds: mealIds,
            label: label,
            time: time,
        };
        setMealGroupModalInfo(info);
    }

    function goCreateMealGroup() {
        const defaultDateConsumption = meals.filter(_meal => checkedMealIds.includes(_meal.id))[0].date_consumption;
        setMealGroupModalInfo({
            isOpen: true,
            time: defaultDateConsumption,
        }); 
    }

    async function doUpdateMealGroup({ mealIds, label, datetime }) {
        const mealsUpdatedValues = mealIds
            .map((mealId) => ({
                id: Number(mealId),
                group_label: label,
                date_consumption: datetime,
            }));
        await MealStore.Update({
            updatedItems: mealsUpdatedValues,
        });
        doResetMealGroupModalState();
        mealsInitialisationEffect();
    }

    async function doCreateMealGroup({ label, datetime }) {
        const updatedMeals = meals
            .filter(_meal => checkedMealIds.includes(_meal.id))
            .map(mealToUpdate => ({
                ...mealToUpdate,
                group_label: label,
                date_consumption: datetime,
            }));
        await MealStore.Update({
            updatedItems: updatedMeals,
        });
        setCheckedMealIds(null);
        doResetMealGroupModalState();
        mealsInitialisationEffect();
    }

    /* Create or update a meal (modal) */
    const [mealModalInfo, setMealModalInfo] = React.useState({ isOpen: false });
    function doResetMealModalState() {
        setMealModalInfo({ isOpen: false });
    }

    function goUpdateMeal({ mealId, timestamp }) {
        const info = {
            isOpen: true,
            mealIdToUpdate: mealId,
            timestamp: timestamp,
        };
        setMealModalInfo(info);
    }

    function goCreateMeal() {
        // avoid giving the Create Meal modal a date which is not the current meals day's
        const timestamp = date.isSame(dayjs(), 'day')
            ? dayjs()
            : meals.reduce(
                (maxDate, currentMeal) => dayjs(currentMeal.date_consumption).isAfter(maxDate)
                    ? dayjs(currentMeal.date_consumption)
                    : maxDate,
                date);
        setMealModalInfo({
            isOpen: true,
            timestamp: timestamp,
        }); 
    }

    async function doCreateMeal({ meal }) {
        await MealStore.Create({ item: meal });
        doResetMealModalState();
        mealsInitialisationEffect();
    }

    async function doUpdateMeal({ mealIdToUpdate, meal }) {
        await MealStore.Update({
            updatedItem: { ...meal, id: Number(mealIdToUpdate) }
        });
        doResetMealModalState();
        mealsInitialisationEffect();
    }

    const gridStyle = isMobileDisplay
        ? {
            gridTemplateColumns: '100%',
            gridTemplateRows: 'max-content max-content max-content max-content max-content',
            gridTemplateAreas: ` 
                "datebar"
                "meals"
                "macros"
                "vitamins"
                "minerals"
            `,
        }
        : {
            gridTemplateColumns: 'max-content 1fr',
            gridTemplateRows: 'max-content max-content max-content max-content',
            gridTemplateAreas: ` 
                "datebar datebar"
                "macros meals"
                "vitamins meals"
                "minerals meals"
            `,
        };

    return (<>
        <SetMealModal
            title={mealModalInfo.mealIdToUpdate ? 'Modify a meal' : 'Add a meal'}
            isOpen={mealModalInfo.isOpen}
            onClose={doResetMealModalState}
            onSubmit={mealModalInfo.mealIdToUpdate
                ? (paramObj) => doUpdateMeal({ mealIdToUpdate: mealModalInfo.mealIdToUpdate, ...paramObj })
                : doCreateMeal}
            mealIdToUpdate={mealModalInfo.mealIdToUpdate}
            timestamp={mealModalInfo.timestamp}
        />
        <SetMealGroupModal
            title={mealGroupModalInfo.mealIds ? 'Modify a meal group' : 'Regroup meals'}
            isOpen={mealGroupModalInfo.isOpen}
            onClose={doResetMealGroupModalState}
            onSubmit={mealGroupModalInfo.mealIds
                ? (paramObj) => doUpdateMealGroup({ mealIds: mealGroupModalInfo.mealIds, ...paramObj })
                : doCreateMealGroup}
            previousLabel={mealGroupModalInfo.label}
            previousTime={mealGroupModalInfo.time}
        />

        <div style={{
            display: 'grid',
            gap: '16px',
            ...gridStyle
        }}>
            <PrevNextDateBar
                dayjsDate={date}
                onClickPrev={() => { goPrevDate(); }}
                onClickNext={() => { goNextDate(); }}
                style={{ gridArea: 'datebar' }}
            />
            <MealList
                isLoading={isLoadingMeals}
                mealVMs={mealVMs}
                expandedMealGroupLabels={expandedMealGroupLabels}
                setExpandedMealGroupLabels={setExpandedMealGroupLabels}
                onSplitMealGroup={doSplitMealGroup}
                onModifyMealGroup={goUpdateMealGroup}
                onEdit={goUpdateMeal}
                onRemove={doRemoveMealFromList}
                isItemCheckingEnabled={checkedMealIds !== null}
                checkedItemIds={checkedMealIds}
                onItemCheckToggle={doItemCheckToggle}
                onAddMeal={goCreateMeal}
                onCollapseAll={doCollapseAll}
                onExpandAll={doExpandAll}
                onRegroupMeals={goCreateMealGroup}
                onToggleItemSelection={doToggleItemSelection}
                onCreateRecipe={doCreateRecipeFromGroup}
                style={{ gridArea: 'meals' }}
            />
            <MacronutrientsSummaryComponent
                isLoading={isLoadingMeals}
                meals={meals}
                style={{ gridArea: 'macros' }}
            />
            <MicronutrientsSummary
                title="Vitamins"
                micronutrientMetadata={FoodMetadata.Vitamins}
                isLoading={isLoadingMeals}
                meals={meals}
                style={{ gridArea: 'vitamins' }}
            />
            <MicronutrientsSummary
                title="Minerals"
                micronutrientMetadata={FoodMetadata.Minerals}
                isLoading={isLoadingMeals}
                meals={meals}
                style={{ gridArea: 'minerals' }}
            />
        </div>
    </>);
}
