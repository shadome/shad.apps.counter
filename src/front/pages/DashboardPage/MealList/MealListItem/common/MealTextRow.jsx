// libraries
import * as React from 'react';
import * as dayjs from 'dayjs';
// project dependencies
import * as Contexts from 'context';
// project components
import Tag from 'front/components/Tag';

// Corresponds to the text of a meal entry (meal name, tags underneath to display kcal, timestamp, etc.).
export default function MealTextRow({
    meal,
    hideTags = false,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const tagStyle = {
        color: css.colour.text.strong,
        alignItems: 'end',
        padding: '1px'
    };
    return (<>
        <div
            {...props}
            style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '4px',
                width: '100%',
                minWidth: 0, // trick required for flex + text ellipsis - https://stackoverflow.com/questions/29947245/css-ellipsis-inside-flex-item
                ...props.style,
            }}
        >
            <span style={{
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
            }}>
                {meal.label}
            </span>
            {!hideTags &&
                <div style={{
                    display: 'flex',
                    alignItems: 'flex-end',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    gap: '4px',
                }}>
                    <Tag
                        variant='highlighted'
                        size='xs'
                        label={dayjs(meal.date_consumption).format('HH:mm')}
                        icon="Date"
                        style={tagStyle}
                    />
                    <Tag
                        variant='highlighted'
                        size='xs'
                        label={Math.round(meal.kcal)}
                        icon="Energy"
                        style={tagStyle}
                    />
                    <Tag
                        variant='highlighted'
                        size='xs'
                        label={`${meal.quantity}g`}
                        style={tagStyle}
                    />
                </div>
            }
        </div>

    </>);
}
