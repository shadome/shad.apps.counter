// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import * as Icons from 'front/components/Icons';
import Button from 'front/components/Button';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';
import MicronutrientProgress from '../../common/MicronutrientProgress';
import MealTextRow from './common/MealTextRow';

export default function MealListItemGroup({
    meal,
    onGroupEdit,
    onUngroup,
    onGroupExpand,
    onGroupCollapse,
    isGroupExpanded,
    isItemCheckingEnabled,
    onCreateRecipe,
}) {
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'start',
            gap: '16px',
        }}>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'start',
                    gap: '16px',
                    height: '100%',
                    width: '100%',
                    cursor: 'pointer',
                }}
                onClick={(event) => { isGroupExpanded ? onGroupCollapse(event) : onGroupExpand(event); }}
            >

                {isGroupExpanded ?

                    <Button
                        icon="ChevronUp"
                        variant="text"
                        style={{
                            minWidth: 'fit-content',
                            minHeight: 'fit-content',
                            padding: '2px',
                        }}
                        iconStyle={{
                            minWidth: css.size.icons.md,
                            minHeight: css.size.icons.md,
                            color: css.colour.text.strong,
                        }}
                    />
                    :
                    <Button
                        icon="ChevronDown"
                        variant="text"
                        style={{
                            minWidth: 'fit-content',
                            minHeight: 'fit-content',
                            padding: '2px',
                        }}
                        iconStyle={{
                            minWidth: css.size.icons.md,
                            minHeight: css.size.icons.md,
                            color: css.colour.text.strong,
                        }}
                    />
                }

                <MealTextRow
                    meal={meal}
                    hideTags={isGroupExpanded}
                />
            </div>

            {!isItemCheckingEnabled && !isGroupExpanded &&
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    gap: '8px',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                }}>
                    {!isMobileDisplay && meal.mainMicronutrients.map(_micronutrientMetadata =>
                        <MicronutrientProgress
                            key={_micronutrientMetadata.id}
                            size="sm"
                            metadata={_micronutrientMetadata}
                            value={meal[_micronutrientMetadata.id] ?? 0}
                        />
                    )}
                    <Dropdown
                        triggerComponent={<Button/>}
                        variant="text"
                        icon="EllipsisVertical"
                        tooltipProps={{ anchor: 'bottomleft' }}
                        iconStyle={{
                            color: css.colour.text.regular,
                            width: css.size.icons.md,
                            height: css.size.icons.md,
                        }}
                    >
                        <DropdownItem
                            variant="text"
                            onClick={onGroupEdit}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                                color: css.colour.text.strong,
                            }}
                        >
                            <Icons.Edit style={{
                                width: css.size.icons.md,
                                height: css.size.icons.md,
                            }} />
                            <span style={{ flexGrow: 1 }}>Edit the meal group</span>
                        </DropdownItem>
                        <DropdownItem
                            variant="text"
                            onClick={onUngroup}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                                color: css.colour.text.strong,
                            }}
                        >
                            <Icons.LayersClear style={{
                                width: css.size.icons.md,
                                height: css.size.icons.md,
                            }} />
                            <span style={{ flexGrow: 1 }}>Ungroup the meal group</span>
                        </DropdownItem>
                        <DropdownItem
                            variant="text"
                            onClick={onCreateRecipe}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                                color: css.colour.text.strong,
                            }}
                        >
                            <Icons.MenuBook style={{
                                width: css.size.icons.md,
                                height: css.size.icons.md,
                            }} />
                            <span style={{ flexGrow: 1 }}>Create a recipe from this meal group</span>
                        </DropdownItem>
                    </Dropdown>
                </div>
            }
        </div>
    </>);
}
