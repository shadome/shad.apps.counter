// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Icons from 'front/components/Icons';
// project components
import MicronutrientProgress from '../../common/MicronutrientProgress';
import Button from 'front/components/Button';
import MealTextRow from './common/MealTextRow';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';

export const MealListItemHeightInPx = 56;

export default function MealListItemSolo({
    meal,
    onEdit,
    onRemove,
    isItemCheckingEnabled,
    checkedItemIds,
    onItemCheckToggle,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            gap: '16px',
            height: `${MealListItemHeightInPx}px`,
            maxWidth: '100%',
            ...props.style,
        }}>
            {isItemCheckingEnabled &&
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    minWidth: 'fit-content',
                    minHeight: 'fit-content',
                    padding: '2px',
                }}>
                    <input
                        type="checkbox"
                        checked={checkedItemIds.includes(meal.id)}
                        onChange={(event) => { onItemCheckToggle(event, meal.id); }}
                        style={{
                            minWidth: css.size.icons.md,
                            minHeight: css.size.icons.md,
                            cursor: 'pointer',
                            accentColor: css.colour.background.dp2,
                        }}
                    />
                </div>
            }
            <MealTextRow
                meal={meal}
                style={{
                    ...(isItemCheckingEnabled && {
                        cursor: 'pointer',
                    }),
                }}
                onClick={(event) => { isItemCheckingEnabled && onItemCheckToggle(event, meal.id); }}
            />
            {!isItemCheckingEnabled &&
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    gap: '8px',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    minWidth: 'fit-content',
                }}>
                    {!isMobileDisplay && meal.mainMicronutrients.map(_micronutrientMetadata =>
                        <MicronutrientProgress
                            key={_micronutrientMetadata.id}
                            size="sm"
                            metadata={_micronutrientMetadata}
                            value={meal[_micronutrientMetadata.id] ?? 0}
                        />
                    )}
                    <Dropdown
                        triggerComponent={<Button/>}
                        variant="text"
                        icon="EllipsisVertical"
                        tooltipProps={{ anchor: 'bottomleft' }}
                        iconStyle={{
                            color: css.colour.text.regular,
                            width: css.size.icons.md,
                            height: css.size.icons.md,
                        }}
                    >
                        <DropdownItem
                            variant="text"
                            onClick={onEdit}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                                color: css.colour.text.strong,
                            }}
                        >
                            <Icons.Edit style={{
                                width: css.size.icons.md,
                                height: css.size.icons.md,
                            }} />
                            <span style={{ flexGrow: 1 }}>Edit the meal item</span>
                        </DropdownItem>
                        <DropdownItem
                            variant="text"
                            onClick={onRemove}
                            style={{
                                display: 'inline-flex',
                                alignItems: 'center',
                                gap: '8px',
                                color: css.colour.text.strong,
                            }}
                        >
                            <Icons.Delete style={{
                                width: css.size.icons.md,
                                height: css.size.icons.md,
                            }} />
                            <span style={{ flexGrow: 1 }}>Delete the meal item</span>
                        </DropdownItem>
                    </Dropdown>
                </div>
            }
        </div>

    </>);
}
