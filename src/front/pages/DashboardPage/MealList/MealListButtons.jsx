// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import Button from 'front/components/Button';
import Toggle from 'front/components/Toggle';
import Hoverable from 'front/components/Hoverable';

export default function MealListButtons({
    isItemSelectionEnabled,
    isRegroupMealsEnabled,
    isAtLeastOneGroupCollapsed,
    areAnyGroups,
    onToggleItemSelection,
    onRegroupMeals,
    onAddMeal,
    onCollapseAll,
    onExpandAll,
    ...props
}) {

    /* Variables */
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'row-reverse',
            flexWrap: 'wrap',
            flexGrow: 1,
            alignItems: 'flex-start',
            gap: '16px',
            ...props?.style,
        }}>
            <Button
                variant="text"
                icon="Plus"
                label={!isMobileDisplay && 'Add meals'}
                onClick={onAddMeal}
                style={{
                    padding: '4px',
                    color: css.colour.text.regular,
                    fontWeight: 'bold'
                }}
                spanStyle={{ textTransform: 'none' }}
            />
            <Hoverable
                component={props => <label {...props} />}
                htmlFor="item_selection"
                style={{
                    height: '100%',
                    color: css.colour.text.regular,
                    fontSize: css.size.font.sm,
                    cursor: 'pointer',
                    fontWeight: 'bold',
                    display: 'flex',
                    flexDirection: 'row',
                    flexWrap: 'wrap-reverse',
                    gap: '8px',
                    alignItems: 'center',
                    alignSelf: 'center',
                    padding: '0px 4px'
                }}
            >
                <>
                    <Toggle
                        id="item_selection"
                        isChecked={isItemSelectionEnabled}
                        onToggle={onToggleItemSelection}

                    />
                    {!isMobileDisplay &&

                        <span>Select meals</span>}
                </>
            </Hoverable>
            {areAnyGroups && isAtLeastOneGroupCollapsed &&
                <Button
                    icon="PlusDoubleSquare"
                    variant="text"
                    onClick={onExpandAll}
                    style={{
                        color: css.colour.text.regular,
                        fontWeight: 'bold',
                        textTransform: undefined,
                        padding: '4px',
                        alignSelf: 'center',
                    }}
                    label={!isMobileDisplay && 'Collapse / Expand'}
                />
            }
            {areAnyGroups && !isAtLeastOneGroupCollapsed &&
                <Button
                    size="sm"
                    icon="MinusDoubleSquare"
                    variant="text"
                    onClick={onCollapseAll}
                    style={{
                        color: css.colour.text.regular,
                        fontWeight: 'bold',
                        textTransform: undefined,
                        padding: '4px',
                        alignSelf: 'center',
                    }}
                    label={!isMobileDisplay && 'Collapse / Expand'}
                />
            }
            {isRegroupMealsEnabled &&
                <Button
                    variant="text"
                    icon="Layers"
                    label="Regroup selection"
                    onClick={onRegroupMeals}
                    style={{
                        padding: '4px',
                        color: css.colour.text.regular,
                        fontWeight: 'bold',
                    }}
                    spanStyle={{ textTransform: 'none' }}
                />
            }
        </div>

    </>);
}
