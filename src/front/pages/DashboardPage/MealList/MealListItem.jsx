// libraries
import * as React from 'react';
// project components
import MealListItemSolo, { MealListItemHeightInPx } from './MealListItem/MealListItemSolo';
import MealListItemGroup from './MealListItem/MealListItemGroup';

export default function MealListItem({
    meal,
    checkedItemIds,
    expandedMealGroupLabels,
    isItemCheckingEnabled,
    // Whether the meal is a group or a leaf under a group, and the group is currently expanded
    onItemCheckToggle,
    onEdit,
    onRemove,
    onGroupCollapse,
    onGroupExpand,
    onGroupEdit,
    onUngroup,
    onCreateRecipe,
}) {
    // Can be either a list item group or a solo / standalone list item
    /* Variables */
    const isGroupExpanded = expandedMealGroupLabels.includes(meal.label);
    const isMealListItemGroup = meal.entries?.length > 0;
    const mealListItemSoloGapInPx = 8;
    const mealListItemSoloHeightInPx = isMealListItemGroup && isGroupExpanded
        ? (MealListItemHeightInPx * meal.entries.length) + (mealListItemSoloGapInPx * (meal.entries.length - 1))
        : 0;
    return (
        <React.Fragment key={meal.label}>
            {!isMealListItemGroup &&
                <MealListItemSolo
                    isItemCheckingEnabled={isItemCheckingEnabled}
                    meal={meal}
                    checkedItemIds={checkedItemIds}
                    onEdit={() => onEdit({ mealId: meal.id, timestamp: meal.date_consumption })}
                    onRemove={() => onRemove({ mealId: meal.id })}
                    onItemCheckToggle={onItemCheckToggle}
                />
            }
            {isMealListItemGroup && <>
                <MealListItemGroup
                    meal={meal}
                    isGroupExpanded={isGroupExpanded}
                    isItemCheckingEnabled={isItemCheckingEnabled}
                    onGroupCollapse={onGroupCollapse}
                    onGroupExpand={onGroupExpand}
                    onGroupEdit={onGroupEdit}
                    onUngroup={onUngroup}
                    onCreateRecipe={onCreateRecipe}
                />
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: `${mealListItemSoloGapInPx}px`,
                     paddingLeft: '16px',
                    // collapsible
                    height: `${mealListItemSoloHeightInPx}px`,
                    overflow: 'hidden',
                    transition: 'height .2s',
                }}>
                    {meal.entries.map(mealEntry =>
                        <MealListItemSolo
                            key={mealEntry.id}
                            isItemCheckingEnabled={isItemCheckingEnabled}
                            meal={mealEntry}
                            checkedItemIds={checkedItemIds}
                            onEdit={() => onEdit({ mealId: mealEntry.id, timestamp: mealEntry.date_consumption })}
                            onRemove={() => onRemove({ mealId: mealEntry.id })}
                            onItemCheckToggle={onItemCheckToggle}
                        />
                    )}
                </div>
            </>}
        </React.Fragment>
    );
}
