
import * as FoodMetadata from 'model/FoodMetadata';

// Meal view models distinguish meal groups (no `id` field, but an `entries` field with 1 or more sub-meal),
// submeals and groupless meals.
// This function takes database meals (e.g., no hierarchical meals with an optional `group` field)
// and transform them into view models by aggregating around the optional `group` field.
// Meal groups are redundant with their entries, e.g., kcals are summed, so that the first level of the
// resulting list of view models suffices to make nutrient-based calculations.
export function getMealsViewModelFromModel(meals) {
    const aggregatedHigherOrderMeals = []
        // contains both standalone meals and meal groups with entries
        .concat(getHigherOrderMeals(meals))
        // aggregate via reducers the sum of relevant nutrient for each entry of the meal group and for each nutrient
        .map(meal => aggregateMealGroupNutrients(meal))
        // add the "main micronutrient" field to single meals, meal groups and meal entries
        .map(meal => inflateMealGroupMainMicronutrients(meal));
    return aggregatedHigherOrderMeals;
}

/* private */

function getHigherOrderMeals(meals) {
    return meals.reduce(
        (accumulator, meal) =>
            meal.group_label
                ? addMealEntry(accumulator, meal)
                : addGrouplessMeal(accumulator, meal),
        []);
    // create the meal group if not already created and add another meal in its entries
    function addMealEntry(meals, mealEntry) {
        const optExistingMealGroupIdx = meals.findIndex(x => x.label === mealEntry.group_label);
        const optExistingMealGroup = meals[optExistingMealGroupIdx] || {};
        const restOfHighestOrderMeals = optExistingMealGroupIdx >= 0
            ? meals.slice(0, optExistingMealGroupIdx).concat(meals.slice(optExistingMealGroupIdx + 1))
            : meals;
        const previousMealGroupEntries = optExistingMealGroup.entries ?? [];
        return [
            ...restOfHighestOrderMeals,
            {
                // mind the order of spread operators
                date_consumption: mealEntry.date_consumption,
                label: mealEntry.group_label,
                ...optExistingMealGroup,
                entries: [...previousMealGroupEntries, mealEntry],
            },
        ];
    }
    // add a standalone meal
    function addGrouplessMeal(meals, meal) {
        return [
            ...meals,
            meal,
        ];
    }
}

function aggregateMealGroupNutrients(meal) {
    // this function make newly created meal groups (above their entries)
    // ressemble a typical standalone/groupless meal
    // standalone meals don't require this function to do anything
    return meal.entries && meal.entries.length
        ? {
            ...meal,
            // must aggregate the numeric values of a meal group
            quantity: meal.entries.reduce((acc, mealEntry) => acc + mealEntry.quantity, 0),
            ...Object.values(FoodMetadata.All).reduce(getMealGroupNutrientsReducer(meal), {}),
        }
        : meal;

    function getMealGroupNutrientsReducer(mealGroup) {
        return (dict, nutrientMetadata) => ({
            ...dict,
            [nutrientMetadata.id]: mealGroup.entries.reduce(getMealEntryNutrientReducer(nutrientMetadata), 0),
        })
        function getMealEntryNutrientReducer(nutrientMetadata) {
            return (sum, mealEntry) => sum + (Number(mealEntry[nutrientMetadata.id]) || 0);
        }
    }
}

function inflateMealGroupMainMicronutrients(meal) {
    return {
        ...meal,
        mainMicronutrients: getMainMicronutrientsForMeal(meal),
        entries: meal.entries
            ?.map(mealEntry => ({
                ...mealEntry,
                mainMicronutrients: getMainMicronutrientsForMeal(mealEntry),
            }))
    }

    function getMainMicronutrientsForMeal(meal) {
        return []
            .concat(FoodMetadata.Minerals)
            .concat(FoodMetadata.Vitamins)
            .filter(x => getNutrientRdaPctForMeal(meal, x) > 0)
            .sort((a, b) => getNutrientRdaPctForMeal(meal, b) - getNutrientRdaPctForMeal(meal, a))
            .slice(0, 5);

        function getNutrientRdaPctForMeal(meal, nutrientMetadata) {
            return !isNaN(nutrientMetadata.rdaLowerBound)
                ? (meal[nutrientMetadata.id] ?? 0) / nutrientMetadata.rdaLowerBound
                : 0;
        }
    }
}