// libraries
import * as React from 'react';
import * as dayjs from 'dayjs';
// project dependencies
import * as Contexts from 'context';
import * as MealStore from 'repository/StoreMeal';
import * as FoodStore from 'repository/StoreFood';
import * as Throttle from 'utils/throttle';
import * as StringUtils from 'utils/strings';
import * as MealAdapter from 'model/adapters/MealAdapter';
// project components
import DualButton from 'front/components/DualButton';
import Button from 'front/components/Button';
import Drawer from 'front/components/Drawer';
import LabelledInput from '../../components/LabelledInput';
import Dropdown from 'front/components/Dropdown';
import DropdownItem from 'front/components/DropdownItem';
import useForm from 'utils/form';

const NB_OF_DROPDOWN_ENTRIES = 50;

const Operations = {
    ADD: 1,
    SUB: 2,
};

// TODO catch 'Enter' on any input for form validation
// TODO catch 'Escape' for every modals
// TODO rework visuals for mobile: modals should probably be fit-content height, 100% width and vertically centered
export default function SetMealModal({
    title,
    isOpen,
    onClose,
    // existing meal id in case of modification, may be undefined
    mealIdToUpdate,
    // existing meal timestamp in case of modification, may be undefined
    timestamp,
    onSubmit,
}) {
    /* Context */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);

    /* Component constants */
    const quantityDualButtonValues = isMobileDisplay ? [15, 100] : [5, 30, 100];
    const timeDualButtonValues = isMobileDisplay ? [10, 60] : [10, 20, 60];
    // holds a boolean value which changes when displayed errors should be ignored again
    const [errorResetFlag, setErrorResetFlag] = React.useState(false);

    // value of the input of the autocomplete component (input of the user)
    // this is not a required part of the form, do not mix up with form.components.selectedFood
    // which is the last selected value from the dropdown list,
    // i.e., after the user has input a few words to find the right dropdown item
    const [foodInput, setFoodInput] = React.useState('');
    // all the items proposed in the dropdown, unfiltered
    const [allItems, setAllItems] = React.useState([]);
    // filtered items proposed in the dropdown at a point in time, taking the current input into account
    const [items, setItems] = React.useState([]);

    /* Handle autocomplete changes with throttle logic */
    async function _onFoodInputChange(newValue) {
        const matchingEntries = StringUtils.searchStrings({
            entries: allItems.map(x => x.label),
            searchStr: newValue,
            topResults: NB_OF_DROPDOWN_ENTRIES,
        });
        // get the sorted labels and get back the associated items ({code, label})
        const matchingItems = matchingEntries.reduce(
            (buffer, current) => [...buffer, allItems.filter(x => x.label === current)[0]],
            []);
        setItems(matchingItems);
    }
    const onFoodInputChange = Throttle.useThrottle(_ => _onFoodInputChange(foodInput), 1000);
    React.useEffect(onFoodInputChange, [foodInput]);

    /* Setup form variables and validation rules */
    const [formComponents, setFormComponents, validateForm, resetForm, resetFormErrors] = useForm({
        selectedFood: {
            value: null,
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'Select a food from the list',
            }],
        },
        mealQuantity: {
            value: 100,
            validations: [{
                predicate: (val) => Boolean(val),
                error: 'The quantity is required',
            }, {
                predicate: (val) => !isNaN(val) && Number(val) > 0 && Number.isInteger(Number(val)),
                error: 'The quantity should be a positive integer',
            }],
        },
        dateConsumption: {
            value: dayjs(),
            validations: [{
                predicate: (val) => Boolean(val) && val.isValid(),
                errorMessage: 'The time is invalid',
            }],
        },
    });
    // The timestamp props will be received after this component has been mounted,
    // so an effect is required to change the form's initial value.
    React.useEffect(() => { Boolean(timestamp) && setFormComponents({ dateConsumption: dayjs(timestamp) }) }, [timestamp]);

    /* Inflate the state variables if this is an 'update' rather than an 'create' */
    async function inflateStateVariablesFromExistingMealEffect() {
        if (mealIdToUpdate === undefined || formComponents === undefined) {
            return;
        }
        let isMounted = true;
        const meal = isMounted && await MealStore.Read({ id: mealIdToUpdate });
        const food = isMounted && await FoodStore.Read({ code: meal.food_code });
        isMounted && inflateFormWithMealData({ meal: meal, food: food });
        return () => { isMounted = false; };

        function inflateFormWithMealData({ meal, food }) {
            // option which should be selected
            const foodValue = {
                code: food.code,
                label: food.label
            };
            setFoodInput(food.label);
            setFormComponents({
                selectedFood: foodValue,
                mealQuantity: meal.quantity,
                dateConsumption: dayjs(meal.date_consumption),
            });
        }
    }
    React.useEffect(inflateStateVariablesFromExistingMealEffect, [mealIdToUpdate]);

    /* Setup an autofocus effect on the first input on mounting event. The way MUI enables this seems ugly, esp. the timer part. */
    const inputRef = React.useRef();
    function setFocusOnOpenEffect() {
        if (!isOpen) {
            return;
        }
        const timeout = setTimeout(focusAction, 10);
        return () => { clearTimeout(timeout); };

        function focusAction() {
            // no autofocus if this is a MODIFICATION call
            !mealIdToUpdate && inputRef?.current && inputRef.current.focus();
        }
    }
    React.useEffect(setFocusOnOpenEffect, [isOpen]);

    /* Load all the possible choices for the food dropdown */
    async function setDropdownItems() {
        if (!isOpen) {
            return;
        }
        let isMounted = true;
        const foods = await FoodStore.Read();
        if (isMounted) {
            const options = foods
                // remove irrelevant food data if necessary
                .filter(_food => !isNaN(_food.kcal))
                // remove duplicates (part 1)
                .map(_food => [_food.label, { code: _food.code, label: _food.label, nominalQty: _food.nominal_quantity }]);
            // remove duplicates (part 2)
            const uniqueOptions = [...new Map(options).values()];
            setAllItems(uniqueOptions);
            setItems(uniqueOptions.slice(0, NB_OF_DROPDOWN_ENTRIES));
        }
        return () => { isMounted = false; };
    }
    React.useEffect(setDropdownItems, [isOpen]);

    /* Form actions */
    async function doSubmit() {
        setErrorResetFlag(!errorResetFlag);
        resetFormErrors();
        const isValid = validateForm();
        if (!isValid) {
            return;
        }
        const food = await FoodStore.Read({
            code: formComponents.selectedFood.value.code,
        });
        // get the URL date, only relevant for ADDING a new meal
        const urlDate = dayjs(timestamp || null, 'YYYY-MM-DD');
        // consolidate date with time, typically the URL date with the time of the input
        const consolidatedDate = urlDate && urlDate.isValid()
            ? formComponents.dateConsumption.value
                .set('date', urlDate.get('date'))
                .set('month', urlDate.get('month'))
                .set('year', urlDate.get('year'))
            : formComponents.dateConsumption.value;
        const meal = MealAdapter.foodToMeal({
            food: food,
            mealQuantity: Number(formComponents.mealQuantity.value),
            timestamp: consolidatedDate.toDate(),
        });
        resetState();
        onSubmit({ meal: meal });
    }

    function doClose() {
        resetState();
        onClose();
    }

    function resetState() {
        setFoodInput('');
        setItems([]);
        resetForm();
    }

    function doSelectFood(newValue) {
        // optional nominal quantity to set, formated to be compatible with js spread operator
        const optEnclosedNominalQuantity = newValue.nominalQty !== undefined
            ? { mealQuantity: newValue.nominalQty }
            : {};
        setFoodInput(newValue.label);
        setFormComponents({
            selectedFood: newValue,
            ...optEnclosedNominalQuantity,
        });
    }

    function doUpdateQuantity(operation, value) {
        const newQuantity = (
            /* case #1 */ operation === Operations.ADD ? Math.round((Number(formComponents.mealQuantity.value) || value) + value) :
            /* case #2 */ operation === Operations.SUB ? Math.round((Number(formComponents.mealQuantity.value) || value) - value) :
            /* default */ formComponents.mealQuantity.value || 0
        );
        setFormComponents({ mealQuantity: newQuantity });
    }

    function doUpdateTimestamp(operation, value) {
        const newTimestamp = (
            /* case #1 */ operation === Operations.ADD ? formComponents.dateConsumption.value.add(value, 'm') :
            /* case #2 */ operation === Operations.SUB ? formComponents.dateConsumption.value.subtract(value, 'm') :
            /* default */ formComponents.dateConsumption.value);
        setFormComponents({ dateConsumption: newTimestamp });
    }

    return !formComponents ? null : (<>
        <Drawer
            open={isOpen}
            onClose={doClose}
            position={isMobileDisplay ? 'full' : 'center'}
            title={
                <span style={{
                    font: css.size.font.xl,
                    fontWeight: 'bold',
                }}>
                    {title}
                </span>
            }
            style={{
                padding: '16px',
                width: '700px', // will be ignored if fullscreen (mobile display)
                height: 'fit-content', // will be ignored if fullscreen (mobile display)
            }}
        >
            {/* modal content + action row */}
            <form style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                height: '100%',
                width: '100%',
            }}>
                {/* modal content */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '16px',
                    flexGrow: 1,
                }}>
                    <Dropdown
                        onSelect={doSelectFood}
                        tooltipProps={{
                            style: {
                                overflowY: 'scroll',
                                maxHeight: '280px',
                            }
                        }}
                        triggerComponent={
                            <LabelledInput
                                ref={inputRef}
                                label="Meal"
                                value={foodInput}
                                onChange={(evt) => { setFoodInput(evt.target.value); }}
                                errorText={formComponents.selectedFood.error}
                                errorResetFlag={errorResetFlag}
                            />
                        }
                    >
                        {items.map(_item =>
                            <DropdownItem key={_item.code} value={_item}>{_item.label}</DropdownItem>
                        )}
                    </Dropdown>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        width: '100%',
                        gap: '8px',
                    }}>
                        <LabelledInput
                            label="Quantity (g)"
                            value={formComponents.mealQuantity.value}
                            onChange={(evt) => { setFormComponents({ mealQuantity: evt.target.value }); }}
                            errorText={formComponents.mealQuantity.error}
                            errorResetFlag={errorResetFlag}
                        />
                        <div style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                            {quantityDualButtonValues.map(grams => (
                                <DualButton
                                    key={grams}
                                    onLeftButtonClick={() => { doUpdateQuantity(Operations.SUB, grams); }}
                                    onRightButtonClick={() => { doUpdateQuantity(Operations.ADD, grams); }}
                                    leftButtonProps={{ disabled: (Number(formComponents.mealQuantity.value) || 0) < grams + 1 }}
                                    label={`${grams}g`}
                                />
                            ))}
                        </div>
                    </div>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        width: '100%',
                        gap: '8px',
                    }}>
                        <LabelledInput
                            type="datetime-local"
                            label="Time of consumption"
                            value={formComponents.dateConsumption.value.format("YYYY-MM-DD HH:mm:ss")}
                            onChange={(evt) => { setFormComponents({ dateConsumption: dayjs(evt.target.value) }); }}
                            errorText={formComponents.dateConsumption.error}
                            errorResetFlag={errorResetFlag}
                        />
                        <div style={{
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }}>
                            {timeDualButtonValues.map(minutes => (
                                <DualButton
                                    key={minutes}
                                    onLeftButtonClick={() => { doUpdateTimestamp(Operations.SUB, minutes); }}
                                    onRightButtonClick={() => { doUpdateTimestamp(Operations.ADD, minutes); }}
                                    label={`${minutes}min`}
                                />
                            ))}
                        </div>
                    </div>
                </div>
                {/* modal action row */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    gap: '16px',
                }}>
                    <Button
                        icon="Send"
                        variant="contained"
                        onClick={() => { doSubmit(); }}
                        children="Submit"
                    />
                    <Button
                        icon="Cross"
                        variant="outlined"
                        onClick={() => { doClose(); }}
                        children="Close"
                    />
                </div>
            </form>
        </Drawer>
    </>);
}
