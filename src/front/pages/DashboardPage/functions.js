export function sumNutrient(meals, nutrientId) {
    return meals?.reduce(getNutrientReducer(nutrientId), 0) || 0;

    function getNutrientReducer(nutrientId) {
        return (total, currentMeal) => total + (Number(currentMeal[nutrientId]) || 0);
    }
}