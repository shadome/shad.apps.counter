// libraries
import * as React from 'react';
import * as dayjs from 'dayjs';
// project dependencies
import * as Contexts from 'context';
// project components
import MealListItem from './MealList/MealListItem';
import MealListButtons from './MealList/MealListButtons';

export default function MealList({
    isLoading,
    mealVMs,
    expandedMealGroupLabels,
    setExpandedMealGroupLabels,
    onModifyMealGroup,
    onSplitMealGroup,
    onEdit,
    onRemove,
    isItemCheckingEnabled,
    checkedItemIds,
    onItemCheckToggle,
    // buttons bar
    onAddMeal,
    onCollapseAll,
    onExpandAll,
    onRegroupMeals,
    onToggleItemSelection,
    onCreateRecipe,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);

    /* Action methods */

    function doGroupExpand(meal) {
        setExpandedMealGroupLabels([...expandedMealGroupLabels, meal.label]);
    }

    function doGroupCollapse(meal) {
        setExpandedMealGroupLabels(expandedMealGroupLabels.filter(x => x !== meal.label));
    }

    function doGroupEdit(meal) {
        onModifyMealGroup({
            mealIds: meal.entries.map(entry => entry.id),
            label: meal.label,
            time: meal.date_consumption,
        });
    }

    function doUngroup(meal) {
        onSplitMealGroup({
            mealIds: meal.entries.map(entry => entry.id),
        });
    }

    /* Variables */
    const hasMeals = mealVMs.length > 0;
    const mealGroups = mealVMs.filter(x => x.entries?.length > 0);
    const areAnyGroups = mealGroups.length > 0;
    const isAtLeastOneGroupCollapsed = areAnyGroups &&  mealGroups.length > expandedMealGroupLabels?.length;

    return (<>
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            padding: '16px',
            backgroundColor: css.colour.background.dp1,
            border: `solid 1px ${css.colour.border}`,
            borderRadius: '1px',
            height: 'fit-content',
            ...props?.style,
            width: '100%',
        }}>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: '4px',
                width: '100%',
            }}>
                <span style={{
                    fontSize: css.size.font.sm,
                }}>
                    Meals of the day
                </span>

                <MealListButtons
                    isItemSelectionEnabled={isItemCheckingEnabled}
                    isRegroupMealsEnabled={Boolean(checkedItemIds?.length)}
                    isAtLeastOneGroupCollapsed={isAtLeastOneGroupCollapsed}
                    areAnyGroups={areAnyGroups}
                    onAddMeal={onAddMeal}
                    onCollapseAll={onCollapseAll}
                    onExpandAll={onExpandAll}
                    onRegroupMeals={onRegroupMeals}
                    onToggleItemSelection={onToggleItemSelection}
                    style={{ flexGrow: 1 }}
                />
            </div>
            {!isLoading && hasMeals && mealVMs.map((meal) => (
                <MealListItem
                    key={`${meal.label} - ${dayjs(meal.date_consumption).valueOf()}`}
                    checkedItemIds={checkedItemIds}
                    expandedMealGroupLabels={expandedMealGroupLabels}
                    isItemCheckingEnabled={isItemCheckingEnabled}
                    meal={meal}
                    onItemCheckToggle={onItemCheckToggle}
                    onEdit={onEdit}
                    onRemove={onRemove}
                    onGroupExpand={_ => doGroupExpand(meal)}
                    onGroupCollapse={_ => doGroupCollapse(meal)}
                    onGroupEdit={_ => doGroupEdit(meal)}
                    onUngroup={_ => doUngroup(meal)}
                    onCreateRecipe={_ => onCreateRecipe({ mealGroup: meal })}
                />
            ))}
            {!isLoading && !hasMeals &&
                <span style={{
                    padding: '8px',
                    color: css.colour.text.soft,
                }}>
                    No items
                </span>
            }
            {isLoading && !hasMeals &&
                <span style={{
                    padding: '8px',
                    color: css.colour.text.regular,
                }}>
                    Loading...
                </span>
            }
        </div>
    </>);
}
