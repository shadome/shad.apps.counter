// libraries
import * as React from 'react';
// project dependencies
import * as CssInterop from 'front/CssInterop';
import * as Contexts from 'context';
// project components
import CircularProgress from 'front/components/CircularProgress';

export default function MicronutrientProgress({
    metadata,
    value,
    // sm, md
    size = 'md',
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const diameter = size === 'sm' ? 50 : 65;
    const percent = Math.round(Number(value * 100 / metadata.rdaLowerBound));
    const boundedPercent = Math.min(100, percent);
    const aspect =
        /* case #1 */ Number(value) > Number(metadata.ulUpperBound) ? 'error' :
        /* case #2 */ Number(value) > Number(metadata.rdaLowerBound) ? 'success' :
        /* default */ undefined;
    
    return (<>
        <CircularProgress
            diameterInPx={diameter}
            aspect={aspect}
            percent={boundedPercent}
            mustDisplayPct={true}
            pctSpanStyle={{
                fontWeight: 'normal',
                fontSize: css.size.font[CssInterop.getRelativeSize(size, -1)],
                marginTop: '-4px',
            }}
            label={metadata.symbol}
            labelSpanStyle={{
                fontSize: css.size.font[CssInterop.getRelativeSize(size, -1)],
            }}
            {...props}
        />
    </>);
}

