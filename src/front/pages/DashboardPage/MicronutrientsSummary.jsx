// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import * as Functions from './functions';
// project components
import MicronutrientProgress from './common/MicronutrientProgress';

export default function MicronutrientsSummary({
    title,
    micronutrientMetadata,
    meals,
    isLoading,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    return (<>
        <div style={{
            width: isMobileDisplay ? '100%' : 'fit-content',
            height: isMobileDisplay ? '100%' : 'auto',
            display: 'flex',
            flexDirection: 'column',
            gap: '16px',
            padding: '16px',
            backgroundColor: css.colour.background.dp1,
            border: `solid 1px ${css.colour.border}`,
            borderRadius: '1px',
            ...props?.style,
        }}>
            <span style={{
                fontSize: css.size.font.sm,
            }}>
                {title}
            </span>
            {isLoading
                ? <span>Loading...</span>
                : <div style={{
                    display: 'grid',
                    gridTemplateColumns: '1fr 1fr 1fr 1fr',
                    gridTemplateRows: '1fr 1fr 1fr',
                    gap: '16px',
                    justifyItems: 'center',
                    alignItems: 'center',
                    height: '100%',
                }}>
                    {micronutrientMetadata.map(micronutrientMetadata =>
                        <MicronutrientProgress
                            size='sm'
                            key={micronutrientMetadata.id}
                            metadata={micronutrientMetadata}
                            value={Functions.sumNutrient(meals, micronutrientMetadata.id)}
                        />
                    )}
                </div>
            }
        </div>
    </>);
}
