// libraries
import * as React from 'react';
import * as dayjs from 'dayjs';
// project dependencies
import { FormComponent, Form } from 'utils/FormComponent';
import * as Contexts from 'context';
// project components
import DualButton from 'front/components/DualButton';
import Button from 'front/components/Button';
import Drawer from 'front/components/Drawer';
import LabelledInput from '../../components/LabelledInput';

// TODO catch 'Enter' on any input for form validation
// TODO catch 'Escape' for every modals
// TODO rework visuals for mobile: modals should probably be fit-content height, 100% width and vertically centered
export default function SetMealGroupModal({
    title,
    isOpen,
    onClose,
    onSubmit,
    previousLabel,
    previousTime,
}) {
    /* Context */
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const css = React.useContext(Contexts.CssContext);

    /* Constants */
    const timeDualButtonValues = isMobileDisplay ? [10, 60] : [10, 20, 60];
    const initialLabel = previousLabel || '';
    const initialTime = previousTime ? dayjs(previousTime) : dayjs();

    /* Setup an autofocus effect on the first input on mounting event. The way MUI enables this seems ugly, esp. the timer part. */
    const inputRef = React.useRef();
    function setFocusOnOpenEffect() {
        isOpen && inputRef.current.focus();
    }
    React.useEffect(setFocusOnOpenEffect, [isOpen]);

    /* Form */
    // changes whenever errors are re-triggered (the actual value, i.e. true or false, is meaningless, as long as it toggles)
    const [errorResetFlag, setErrorResetFlag] = React.useState(false);

    const form = new Form() // inliner so that there is no need to define things in one place and to add them in a list afterwards
        // date and time of consumption
        .addComponent({
            propertyName: 'datetime',
            formComponent: FormComponent
                .make({
                    valueState: React.useState(initialTime),
                    errorState: React.useState(''),
                })
                .addValidation({
                    predicate: (datetime) => Boolean(datetime) && datetime.isValid(),
                    errorMessage: 'The time is invalid',
                })
        })
        // meal label (default to the selected food label but may differ from food label)
        .addComponent({
            propertyName: 'label',
            formComponent: FormComponent
                .make({
                    valueState: React.useState(initialLabel),
                    errorState: React.useState(''),
                })
                .addValidation({
                    predicate: (label) => Boolean(label),
                    errorMessage: 'The label is required',
                })
        });

    /* Update the form values when new props are provided */
    function updateFormValuesOnNewPropsEffect() {
        isOpen && form.reset();
        isOpen && form.components.label.setValue(initialLabel);
        isOpen && form.components.datetime.setValue(initialTime);
    }
    React.useEffect(updateFormValuesOnNewPropsEffect, [isOpen]);

    /* Form actions */
    function doSubmit() {
        setErrorResetFlag(!errorResetFlag);
        form.resetErrors();
        form.validate();
        if (!form.isValid) {
            return;
        }
        onSubmit({
            label: form.components.label.value,
            datetime: form.components.datetime.value.toDate(),
        });
        form.reset();
    }

    function doClose() {
        form.reset();
        onClose();
    }

    return (<>

        <Drawer
            open={isOpen}
            onClose={onClose}
            position={isMobileDisplay ? 'full' : 'center'}
            title={
                <span style={{
                    font: css.size.font.xl,
                    fontWeight: 'bold',
                }}>
                    {title}
                </span>
            }
            style={{
                padding: '16px',
                width: '100%',
                height: 'fit-content',
                maxWidth: css.modal.maxDimension, // will be ignored if fullscreen
                maxHeight: css.modal.maxDimension, // will be ignored if fullscreen
            }}
        >
            {/* modal content + action row */}
            <form style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '16px',
                height: '100%',
                width: '100%',
            }}>
                {/* modal content */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '16px',
                    flexGrow: 1,
                }}>
                    <LabelledInput
                        label="Meal group label"
                        value={form.components.label.value}
                        onChange={(evt) => { form.components.label.setValue(evt.target.value); }}
                        errorText={form.components.label.error}
                        errorResetFlag={errorResetFlag}
                        ref={inputRef}
                        inputProps={{
                            required: 'required',
                        }}
                    />
                    <LabelledInput
                        type="datetime-local"
                        label="Time of consumption"
                        value={form.components.datetime.value.format("YYYY-MM-DD HH:mm:ss")}
                        onChange={(evt) => { form.components.datetime.setValue(dayjs(evt.target.value)); }}
                        errorText={form.components.datetime.error}
                        errorResetFlag={errorResetFlag}
                        inputProps={{
                            required: 'required',
                        }}
                    />
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                    }}>
                        {timeDualButtonValues.map(minutes => (
                            <DualButton
                                key={minutes}
                                onLeftButtonClick={() => { form.components.datetime.setValue(form.components.datetime.value.subtract(minutes, 'm')); }}
                                onRightButtonClick={() => { form.components.datetime.setValue(form.components.datetime.value.add(minutes, 'm')); }}
                                label={`${minutes}min`}
                            />
                        ))}
                    </div>
                </div>
                {/* modal action row */}
                <div style={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                    gap: '16px',
                }}>
                    <Button
                        icon="Send"
                        variant="contained"
                        onClick={() => { doSubmit(); }}
                        children="Submit"
                    />
                    <Button
                        icon="Cross"
                        variant="outlined"
                        onClick={() => { doClose(); }}
                        children="Close"
                    />
                </div>
            </form>
        </Drawer>
    </>);
}
