// libraries
import * as React from 'react';
// project dependencies
import * as Functions from './functions';
import * as Contexts from 'context';
import * as FoodMetadata from 'model/FoodMetadata';

export default function MacronutrientsSummaryComponent({
    meals,
    isLoading,
    ...props
}) {
    const css = React.useContext(Contexts.CssContext);
    const isMobileDisplay = React.useContext(Contexts.DisplayContext) === 'mobile';
    const trackedMacronutrients = [
        FoodMetadata.All.kcal,
        FoodMetadata.All.fat,
        FoodMetadata.All.proteins,
        FoodMetadata.All.carbohydrates,
        FoodMetadata.All.fibres,
    ];
    return (<>
        <div style={{
            width: isMobileDisplay ? '100%' : 'auto',
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            padding: '16px',
            backgroundColor: css.colour.background.dp1,
            border: `solid 1px ${css.colour.border}`,
            borderRadius: '1px',
            ...props?.style,
        }}>
            <span style={{
                fontSize: css.size.font.sm,
            }}>
                Macronutrients
            </span>
            {isLoading
                ? <span>Loading...</span>
                : trackedMacronutrients.map((nutrient, i) => (
                    <React.Fragment key={nutrient.id}>
                        {i > 0 &&
                            <hr style={{
                                borderTopStyle: 'solid',
                                borderTopWidth: '1px',
                                width: '100%',
                                height: '1px',
                                color: css.colour.border,
                            }} />
                        }
                        <div style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <span style={{
                                fontSize: css.size.font.md,
                            }}>
                                {`${nutrient.label} (${nutrient.unit.symbol})`}
                            </span>
                            <h6 style={{
                                fontSize: css.size.font.lg,
                                fontWeight: 'bold',
                            }}>
                                {Math.round(Functions.sumNutrient(meals, nutrient.id), 0)}
                            </h6>
                        </div>
                    </React.Fragment>
                ))
            }
        </div>
    </>);
}
