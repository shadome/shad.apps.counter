import DashboardPage, { getPageUrl as getDashboardPageUrl } from './DashboardPage';
import HomePage from './HomePage';
import DataPage from './DataPage';
import CounterAddPage from './counter_add/CounterAddPage';
import ShowcasePage from './ShowcasePage';
import NotFoundPage from './NotFoundPage';
import FoodPage, { getPageUrl as getFoodPageUrl } from './FoodPage';

export default {
    HomePage: {
        Component: HomePage,
        paths: ['/'],
    },
    CounterAddPage: {
        Component: CounterAddPage,
        paths: ['/counter', '/counter/add'],
    },
    FoodPage: {
        Component: FoodPage,
        paths: ['/food', 'food/add'],
        getUrl: getFoodPageUrl,
    },
    DashboardPage: {
        Component: DashboardPage,
        paths: ['/dashboard/:date?'],
        getUrl: getDashboardPageUrl,
    },
    DataPage: {
        Component: DataPage,
        paths: ['/data'],
    },
    ShowcasePage: {
        Component: ShowcasePage,
        paths: ['/showcase'],
    },
    NotFoundPage: {
        Component: NotFoundPage,
        paths: ['/*'],
    },
};