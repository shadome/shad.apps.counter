import * as React from 'react';
// project dependencies
import * as Contexts from '../../context';

export default function HomePage() {
    /* Set the app's title using the dedicated context */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    const display = React.useContext(Contexts.DisplayContext);
    React.useEffect(() => { setAppTitle('Welcome 😀'); }, []);

    return (
        <div>
            <h1>Hey, you!</h1>

            {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
                it is a temporary quickfix as text elements have no good default styling yet */}
            
            <p style={{ all: 'revert' }}>This site is currently under construction and aims at:</p>
            <ul style={{ all: 'revert' }}>
                <li>Serving a true functional purpose: listing daily food consumption to keep track of them and pointing out inconsistencies, esp. in micronutrients</li>
                <li>Keep in touch with front end design and modern-ish technologies <em>(PWA, SPA, DBaaS, etc.)</em></li>
                <li>Providing fun as I'm making it!</li>
            </ul>

            <p style={{ all: 'revert' }}>Thanks a bunch to <a target="_blank" href="https://game-icons.net/">the game-icons project and community</a> for their incredible work.</p>

            <p style={{ all: 'revert' }}>Thanks to <a target="_blank" href="https://marc-delpech.pagesperso-orange.fr/">my friend and ex-colleague</a> for being the initial motivational trigger to this project.</p>

            <p style={{ all: 'revert' }}><a target="_blank" href="https://gitlab.com/shadome/shad.apps.counter">Source code repository</a></p>
        </div>
    )
}
