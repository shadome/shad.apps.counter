// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from '../../../context';
import * as Breakpoints from '../../Breakpoints';
import Input from '../../components/Input';
import InputBuffer from '../../components/InputBuffer';
import Dropdown from '../../components/Dropdown';
import DropdownItem from '../../components/DropdownItem';
import CounterContainer from 'front/containers/CounterTile/CounterContainer';

export default function CounterAddPage({ match, history }) {
    /* Set the page's title */
    const setAppTitle = React.useContext(Contexts.SetTitleContext);
    const breakpoint = React.useContext(Contexts.BreakpointContext);
    React.useEffect(() => { setAppTitle('Create a counter'); }, []);

    const css = React.useContext(Contexts.CssContext);

    const [label, setLabel] = React.useState('');
    const [incrementValue, setIncrementValue] = React.useState('');
    const [newDayFixedValue, setNewDayFixedValue] = React.useState('');

    const newDayInitialValueLabel = 'Select new day\'s initial value';
    const newDayInitialValueLast = 'Last day\'s value';
    const newDayInitialValueFixed = 'Fixed value';
    const [selectedNewDayInitialValue, setSelectedNewDayInitialValue] = React.useState(null);
    
    // Reponsive variables for grid display of the 'select new day's initial value' dropdown + dedicated but not always displayed input field
    // https://css-tricks.com/snippets/css/complete-guide-grid/
    const isTinyDisplay = breakpoint < Breakpoints.xs;
    const gridAreaDropdown = 'dropdown';
    const gridAreaValue = 'value';
    const isDropdownOnOwnRow = isTinyDisplay || selectedNewDayInitialValue !== newDayInitialValueFixed;
    const isInitValueOnOwnRow = selectedNewDayInitialValue === newDayInitialValueFixed && isTinyDisplay;
    const newDaysInitialValueGridTemplate = (
        /* case #1 */ isDropdownOnOwnRow && isInitValueOnOwnRow ? `"${gridAreaDropdown} ${gridAreaDropdown}" "${gridAreaValue} ${gridAreaValue}"` :
        /* case #2 */ isDropdownOnOwnRow && !isInitValueOnOwnRow ? `"${gridAreaDropdown} ${gridAreaDropdown}"` :
        /* default */ `"${gridAreaDropdown} ${gridAreaValue}"`
    );


    return (<>
        <CounterContainer />
        
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyItems: 'stretch',
            marginTop: '8px',
            gap: '24px',
            maxWidth: '500px',
            marginRight: 'auto',
            marginLeft: 'auto',
        }}>
            <Input
                label="Counter label *"
                value={label}
                onChange={evt => { setLabel(evt.target.value); }}
                style={{
                    width: '100%',
                    maxWidth: '500px',
                }}
            />
            <Input
                inputMode="numeric"
                label="Increment step"
                value={incrementValue}
                onChange={evt => { setIncrementValue(evt.target.value); }}
                tooltipValue="Value which can be added or subtracted using a shorthand button in the daily page"
                style={{
                    width: '100%',
                    maxWidth: '500px',
                }}
            />

            <div style={{
                display: 'grid',
                gridTemplate: newDaysInitialValueGridTemplate,
                columnGap: '8px',
                rowGap: '24px',
                justifyItems: 'stretch',
                width: '100%',
                maxWidth: '500px',
            }}>
                <Dropdown
                    placeholder={newDayInitialValueLabel}
                    selectedValue={selectedNewDayInitialValue}
                    onSelect={(value) => { setSelectedNewDayInitialValue(value) }}
                    style={{
                        gridArea: gridAreaDropdown,
                        height: '48px',
                        borderColor: css.colour.border,
                        color: !selectedNewDayInitialValue
                            ? css.colour.text.soft
                            : css.colour.text.regular,
                    }}
                >
                    <DropdownItem value={newDayInitialValueLast}>
                        {newDayInitialValueLast}
                    </DropdownItem>
                    <DropdownItem value={newDayInitialValueFixed}>
                        {newDayInitialValueFixed}
                    </DropdownItem>
                </Dropdown>
                {selectedNewDayInitialValue === newDayInitialValueFixed && 
                    <Input
                        inputMode="numeric"
                        label="Initial value"
                        value={newDayFixedValue}
                        onChange={evt => { setNewDayFixedValue(evt.target.value); }}
                        style={{ gridArea: gridAreaValue }}
                    />
                }
            </div>
            <InputBuffer
                placeholder="Available tags"
                style={{
                    width: '100%',
                }}
            />
        </div>
    </>);
}
