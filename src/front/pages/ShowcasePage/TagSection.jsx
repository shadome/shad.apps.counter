// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import Tag from 'front/components/Tag';
import { aspectToIconName } from 'front/components/Icons';

export default function TagSection() {
    const css = React.useContext(Contexts.CssContext);

    const aspectsInfo = [
        { aspect: undefined, label: 'Default' },
        { aspect: 'info', label: 'Info' },
        { aspect: 'success', label: 'Success' },
        { aspect: 'warning', label: 'Warning' },
        { aspect: 'error', label: 'Error' },
    ];
    const sizes = ['xs', 'sm', 'md', 'lg', 'xl'];
    function _void() { };

    return (<>
        {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
            it is a temporary quickfix as text elements have no good default styling yet */}
        <div>
            <h2>Tags</h2>
            <p style={{ all: 'revert' }}>
                Tags are display components meant to emphasis meta-information or to mark each element as being unique amongst others.
                A tag will typically be associated with multiple-select form inputs or to indicate
                a certain status, type, event, etc. for an element. This component can also be used, with the appropriate size, to display notifications.
            </p>
            <p style={{ all: 'revert' }}>
                It can outline an icon, a text or both. It also supports:
            </p>
            <ul style={{ all: 'revert' }}>
                <li>onclose callbacks</li>
                <li>usual aspects</li>
                <li>different sizes</li>
                <li>variants: outlined (default), highlighted, contained and contained-highlighted</li>
            </ul>
            <p style={{ all: 'revert' }}>Note that highlighted elements may not be as readable as other variants for contrast reasons.</p>
        </div>
        <fieldset style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            border: `2px solid ${css.colour.border}`,
            borderRadius: '4px',
            padding: '8px',
            width: '100%',
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.label,
            }}>
                Finish
            </legend>

            <div style={{
                display: 'flex',
                alignItems: 'flex-end',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                
                <Tag icon="Moon" />
                
                <Tag label="Text-only" />
                
                <Tag label="Closable" onClose={_void} />
                
                <Tag
                    aspect="info"
                    variant="highlighted"
                    label="🐯  1"
                    size="md"
                />
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'baseline',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {aspectsInfo.map(_aspectVariant => (
                    <Tag
                        key={_aspectVariant.label}
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.aspect ? aspectToIconName[_aspectVariant.aspect] : 'Moon'}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'baseline',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {aspectsInfo.map(_aspectVariant => (
                    <Tag
                        key={_aspectVariant.label}
                        variant="highlighted"
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.aspect ? aspectToIconName[_aspectVariant.aspect] : 'Moon'}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'baseline',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {aspectsInfo.map(_aspectVariant => (
                    <Tag
                        key={_aspectVariant.label}
                        variant="contained"
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.aspect ? aspectToIconName[_aspectVariant.aspect] : 'Moon'}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'baseline',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {aspectsInfo.map(_aspectVariant => (
                    <Tag
                        key={_aspectVariant.label}
                        variant="contained-highlighted"
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.aspect ? aspectToIconName[_aspectVariant.aspect] : 'Moon'}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                alignItems: 'flex-end',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {sizes.map(_size => (
                    <Tag
                        key={_size}
                        size={_size}
                        label={`Size: ${_size}`}
                        icon="Moon"
                    />
                ))}
            </div>
        </fieldset>
    </>);
};
