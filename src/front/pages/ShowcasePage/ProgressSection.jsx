// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
// project components
import CircularProgress from 'front/components/CircularProgress';

export default function ProgressSection() {
    const css = React.useContext(Contexts.CssContext);

    return (<>
        {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
            it is a temporary quickfix as text elements have no good default styling yet */}
        <div>
            <h2>Circular progress bar</h2>
            <p style={{ all: 'revert' }}>Circular progress bars display a progress from 0 to 100.</p>
            <p style={{ all: 'revert' }}>It can also display the current progress as text, and/or a custom label.</p>
            <p style={{ all: 'revert' }}>Custom sizes and usual variants are supported.</p>
        </div>
        <fieldset style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            border: `2px solid ${css.colour.border}`,
            borderRadius: '4px',
            padding: '8px',
            width: '100%',
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.label,
            }}>
                Finish
            </legend>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}>
                <CircularProgress
                    percent={42}
                />
                <CircularProgress
                    percent={42}
                    label="Custom label"
                />
                <CircularProgress
                    percent={42}
                    mustDisplayPct
                />
                <CircularProgress
                    percent={42}
                    mustDisplayPct
                    label="Label & pct"
                    labelSpanStyle={{ fontSize: css.size.font.tn }}
                />
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}>
                {['info', 'success', 'warning', 'error'].map(aspect => (
                    <CircularProgress
                        key={aspect}
                        aspect={aspect}
                        percent={42}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}>
                {[20, 40, 60, 80, 100].map(diameter => (
                    <CircularProgress
                        key={diameter}
                        percent={42}
                        diameterInPx={diameter}
                    />
                ))}
            </div>
        </fieldset>
    </>);
};
