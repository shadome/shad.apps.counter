// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import Button from 'front/components/Button';
import DropdownItem from 'front/components/DropdownItem';
import Tag from 'front/components/Tag';
import Dropdown from 'front/components/Dropdown';

export default function DropdownSection() {
    const css = React.useContext(Contexts.CssContext);
    const addFeedback = React.useContext(Contexts.AddFeedbackContext);

    const defaultValues = ['Entry 1', 'Entry 2 padding 16px', 'Entry 3 with more text'];
    const settingsValues = ['Setting 1', 'Setting 2', 'Setting 3'];
    const longText = 'Somewhat longer text used to trigger max-width';
    const shortText = 'short text';
    function doSelect(value) {
        addFeedback({
            label: `You selected the value '${value}'`,
        });
    }

    const Code = (props) => (
        <code>
            <Tag
                label={props.children}
                variant="contained-highlighted"
                size="sm"
                style={{
                    padding: '0px',
                    fontWeight: 'normal'
                }}
            />
        </code>
    );

    const Key = (props) => (
        <Tag
            icon={props.icon}
            label={props.label}
            size="xs"
            style={{
                verticalAlign: 'top',
                padding: '1px',
                marginLeft: '1px',
                marginRight: '1px',
            }}
        />
    );

    return (<>
        {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
            it is a temporary quickfix as text elements have no good default styling yet */}
        <div>
            <h2>Dropdown menus</h2>
            <p style={{ all: 'revert' }}>The dropdown menu is an element composed of other elements: button, tooltip, etc.</p>
            <p style={{ all: 'revert' }}>
                The trigger component can be specified. By default, it is a button
                with the given <Code>props.label</Code> (which
                is ignored if a trigger component is provided).
                The dropdown area's min and max width can be set to the trigger component's.
            </p>
            <p style={{ all: 'revert' }}>
                Once opened, the dropdown area's element can be selected
                using the keys <Key icon="ArrowTop" />, <Key icon="ArrowBottom" /> and <Key label="Enter" />.
            </p>
        </div>
        <fieldset style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            border: `2px solid ${css.colour.border}`,
            borderRadius: '4px',
            padding: '8px',
            width: '100%',
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.label,
            }}>
                Finish
            </legend>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                {[true, false].map(_withAnimations => (
                    <Dropdown
                        onSelect={x => { doSelect(x); }}
                        placeholder={_withAnimations ? 'Default dropdown menu' : 'Unanimated'}
                        disableAnimations={!_withAnimations}
                        key={_withAnimations}
                    >
                        {defaultValues.map((_value, _i) => (
                            <DropdownItem
                                key={_i}
                                value={_value}
                                { ...((_i === 1) ? {style: { paddingLeft: '16px' }} : { })}
                            >
                                {_value}
                            </DropdownItem>
                        ))}
                    </Dropdown>
                ))}
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Dropdown
                    placeholder="Disabled dropdown Minwidth"
                    disableTooltipMinWidth
                    onSelect={x => { doSelect(x); }}
                >
                    <DropdownItem value={shortText}>
                        {shortText}
                    </DropdownItem>
                </Dropdown>
                <Dropdown
                    placeholder="enforced Maxwidth"
                    forceTooltipMaxWidth
                    onSelect={x => { doSelect(x); }}
                >
                    <DropdownItem value={longText}>
                        {longText}
                    </DropdownItem>
                </Dropdown>
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                alignItems: 'baseline',
                gap: '8px',
            }}> 
                <Dropdown
                    placeholder="no content"
                />
                <Dropdown
                    htmlElement="span"
                    dropdownChildren="Span custom trigger"
                />
                <Dropdown
                    triggerComponent={<Button />}
                    onSelect={(x) => { doSelect(x); }}
                    icon="Settings"
                    variant="text"
                >
                    {settingsValues.map((_setting, _i) => (
                        <DropdownItem key={_i} value={_setting}>{_setting}</DropdownItem>
                    ))}
                </Dropdown>
            </div>
        </fieldset>
    </>);
};