// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import Button from 'front/components/Button';

export default function ButtonSection() {
    const css = React.useContext(Contexts.CssContext);
    const addFeedback = React.useContext(Contexts.AddFeedbackContext);

    const aspectVariants = [
        { aspect: 'info', label: 'Info', icon: 'CircledExclamation' },
        { aspect: 'success', label: 'Success', icon: 'CircledCheck' },
        { aspect: 'warning', label: 'Warning', icon: 'Warning' },
        { aspect: 'error', label: 'Error', icon: 'CircledCross' },
    ];
    function onButtonClick(aspect) {
        const n = aspect === 'error' || aspect === 'info' ? 'n' : '';
        const text = aspect
            ? `You clicked a${n} ${aspect} button!`
            : 'You clicked a button!';
        addFeedback({
            label: text,
            aspect: aspect,
        });
    }

    return (<>
        {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
            it is a temporary quickfix as text elements have no good default styling yet */}
        <div>
            <h2>Buttons</h2>
            <p style={{ all: 'revert' }}>Buttons can specify an icon and an aspect. Three variants are proposed:</p>
            <ul style={{ all: 'revert' }}>
                <li>Outlined, the default variant (default emphasis)</li>
                <li>Contained (high emphasis)</li>
                <li>Text (low emphasis)</li>
            </ul>
        </div>
        <fieldset style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            border: `2px solid ${css.colour.border}`,
            borderRadius: '4px',
            padding: '8px',
            width: '100%',
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.label,
            }}>
                Finish
            </legend>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="Moon"
                    onClick={() => onButtonClick()}
                />
                <Button
                    label="default button"
                    onClick={() => onButtonClick()}
                />
                {aspectVariants.map(_aspectVariant => (
                    <Button
                        key={_aspectVariant.aspect}
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.icon}
                        onClick={() => onButtonClick(_aspectVariant.aspect)}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    label="contained button"
                    variant="contained"
                    onClick={() => onButtonClick()}
                />
                {aspectVariants.map(_aspectVariant => (
                    <Button
                        key={_aspectVariant.aspect}
                        variant="contained"
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.icon}
                        onClick={() => onButtonClick(_aspectVariant.aspect)}
                    />
                ))}
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    label="text button"
                    variant="text"
                    onClick={() => onButtonClick()}
                />
                {aspectVariants.map(_aspectVariant => (
                    <Button
                        key={_aspectVariant.aspect}
                        variant="text"
                        aspect={_aspectVariant.aspect}
                        label={_aspectVariant.label}
                        icon={_aspectVariant.icon}
                        onClick={() => onButtonClick(_aspectVariant.aspect)}
                    />
                ))}
            </div>
        </fieldset>
    </>);
};
