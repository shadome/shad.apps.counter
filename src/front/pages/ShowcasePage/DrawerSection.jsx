// libraries
import * as React from 'react';
// project dependencies
import * as Contexts from 'context';
import Drawer from 'front/components/Drawer';
import Button from 'front/components/Button';

export default function DrawerSection() {
    const css = React.useContext(Contexts.CssContext);

    const [isTopDrawerOpen, setTopDrawerOpen] = React.useState(false);
    const [isLeftDrawerOpen, setLeftDrawerOpen] = React.useState(false);
    const [isRightDrawerOpen, setRightDrawerOpen] = React.useState(false);
    const [isBottomDrawerOpen, setBottomDrawerOpen] = React.useState(false);
    const [isFullscreenDrawerOpen, setFullscreenDrawerOpen] = React.useState(false);
    const [isCenterModalOpen, setCenterModalOpen] = React.useState(false);

    return (<>
        {/* `style={{ all: 'revert' }}` reverts the `all: unset;` used for the application pages,
            it is a temporary quickfix as text elements have no good default styling yet */}
        <Drawer
            open={isRightDrawerOpen}
            onClose={() => { setRightDrawerOpen(false); }}
            position="right"
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Right content, default width
            </div>
        </Drawer>
        <Drawer
            open={isLeftDrawerOpen}
            onClose={() => { setLeftDrawerOpen(false); }}
            position="left"
            withChevron
            style={{ width: '300px' }}
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Left content, 300px in width, with closing chevron
            </div>
        </Drawer>
        <Drawer
            open={isTopDrawerOpen}
            onClose={() => { setTopDrawerOpen(false); }}
            position="top"
            style={{ height: '500px' }}
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Top content, 500px in height
            </div>
        </Drawer>
        <Drawer
            open={isBottomDrawerOpen}
            onClose={() => { setBottomDrawerOpen(false); }}
            position="bottom"
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Bottom content, default height
            </div>
        </Drawer>

        <Drawer
            open={isFullscreenDrawerOpen}
            onClose={() => { setFullscreenDrawerOpen(false); }}
            position="full"
            title="Unstyled fullscreen drawer title"
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Fullscreen drawer content
            </div>
        </Drawer>

        <Drawer
            open={isCenterModalOpen}
            onClose={() => { setCenterModalOpen(false); }}
            position="center"
            title="Unstyled center modal title"
        >
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                Center modal content
            </div>
        </Drawer>

        <div>
            <h2>Drawers</h2>
            <p style={{ all: 'revert' }}>Drawers are modal components which pop at an end of the screen and help put more information inside a page without overloading it.</p>
            <p style={{ all: 'revert' }}>Note that this component must be provided with a width (height), else a default width (height) of 40% is set. It cannot adjust to its content. Also, a closing chevron can be displayed. A fullscreen version is available for small devices.</p>
        </div>
        <fieldset style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '8px',
            border: `2px solid ${css.colour.border}`,
            borderRadius: '4px',
            padding: '8px',
            width: '100%',
        }}>
            <legend style={{
                paddingLeft: '8px',
                paddingRight: '8px',
                fontSize: '14px',
                fontWeight: '600',
                color: css.colour.text.label,
            }}>
                Finish
            </legend>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="ArrowTop"
                    label="Open top drawer"
                    onClick={() => setTopDrawerOpen(true)}
                />
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="ArrowLeft"
                    label="Open left drawer"
                    onClick={() => setLeftDrawerOpen(true)}
                />
                <Button
                    icon="ArrowRight"
                    label="Open right drawer"
                    onClick={() => setRightDrawerOpen(true)}
                />
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="ArrowBottom"
                    label="Open bottom drawer"
                    onClick={() => setBottomDrawerOpen(true)}
                />
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="Fullscreen"
                    label="Open fullscreen drawer"
                    onClick={() => setFullscreenDrawerOpen(true)}
                />
            </div>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'center',
                gap: '8px',
            }}> 
                <Button
                    icon="CheckboxSquareBlank"
                    label="Open center modal"
                    onClick={() => setCenterModalOpen(true)}
                />
            </div>
        </fieldset>
    </>);
};
