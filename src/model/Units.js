export default {
  kcal: {
    id: "kcal",
    symbol: "kcal",
    label: "kilocalorie"
  },
  g: {
    id: "g",
    symbol: "g",
    label: "gram"
  },
  mg: {
    id: "mg",
    symbol: "mg",
    label: "milligram"
  },
  mcg: {
    id: "mcg",
    symbol: "μg",
    label: "microgram"
  },
}