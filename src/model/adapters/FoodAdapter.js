import * as FoodMetadata from 'model/FoodMetadata';
import FoodCategory from 'model/FoodCategory';
import * as dayjs from 'dayjs';
import FoodDataSource from 'model/FoodDataSource';


export function getNewCode() {
    // quick way to generate a new code unique to the user:
    // take the number of milliseconds of the current timestamp
    // this idb index is marked as unique so a failure would
    // occur in case this is not unique, which should 
    // realistically never happen
    const newCode = dayjs().valueOf().toString();
    return newCode;
}

/// Meals and foods share the same fields concerning food nutrients.
/// This function takes the quantity of the meal, e.g., 150g, and
/// calculate each nutrient as if the quantity was 100g, because
/// foods store their nutrient data per 100g.
export function mealToFood(meal) {
    const newCode = getNewCode();
    const mealQty = meal.quantity;
    if (isNaN(mealQty)) {
        console.error(`The quantity of the meal is NaN: ${mealQty}`);
        console.error(meal);
        return;
    }
    const factor = Number(100 / Number(mealQty));
    const nutrients = Object.values(FoodMetadata.All).reduce(
        (buffer, macro) => !isNaN(meal[macro.id])
            ? { ...buffer, [macro.id]: Number(Number(meal[macro.id]) * factor) }
            : buffer,
        /* initialValue: */ {});
    const food = {
        code: newCode,
        data_source: FoodDataSource.user,
        category: FoodCategory.composed.id,
        label: meal.label,
        ...nutrients,
    };
    return food;
}

export function ciqualToFood(ciqualEntry) {
    return {
        code: ciqualEntry['code'],
        data_source: FoodDataSource.ciqual,
        category: getCategory(ciqualEntry),
        label: ciqualEntry['name_en'],
        [FoodMetadata.All.kcal.id]: Number(ciqualEntry['kcal'] || getKcalFromMacros(ciqualEntry) || 0),
        [FoodMetadata.All.proteins.id]: Number(ciqualEntry['mac-prot']),
        [FoodMetadata.All.fat.id]: Number(ciqualEntry['mac-fat']),
        [FoodMetadata.All.carbohydrates.id]: Number(ciqualEntry['mac-carb']),
        [FoodMetadata.All.alcohol.id]: Number(ciqualEntry['mac-alc']),
        [FoodMetadata.All.carbs_sugar.id]: Number(ciqualEntry['mac-sug']),
        [FoodMetadata.All.fibres.id]: Number(ciqualEntry['mac-fib']),
        [FoodMetadata.All.salt.id]: Number(ciqualEntry['mac-salt']),
        [FoodMetadata.All.cholesterol.id]: Number(ciqualEntry['mac-fat-chol-mg']),
        [FoodMetadata.All.fat_saturated.id]: Number(ciqualEntry['mac-fat-sat']),
        [FoodMetadata.All.fat_fa_n9.id]: Number(ciqualEntry['mac-fat-muf']),
        [FoodMetadata.All.fat_fa_polyunsaturated.id]: Number(ciqualEntry['mac-fat-puf']),
        [FoodMetadata.All.min_ca.id]: Number(ciqualEntry['min-ca-mg']),
        [FoodMetadata.All.min_cl.id]: Number(ciqualEntry['min-cl-mg']),
        [FoodMetadata.All.min_cu.id]: Number(ciqualEntry['min-cu-mg']),
        [FoodMetadata.All.min_fe.id]: Number(ciqualEntry['min-fe-mg']),
        [FoodMetadata.All.min_i.id]: Number(ciqualEntry['min-i-ug']),
        [FoodMetadata.All.min_mg.id]: Number(ciqualEntry['min-mg-mg']),
        [FoodMetadata.All.min_mn.id]: Number(ciqualEntry['min-mn-mg']),
        [FoodMetadata.All.min_p.id]: Number(ciqualEntry['min-p-mg']),
        [FoodMetadata.All.min_k.id]: Number(ciqualEntry['min-k-mg']),
        [FoodMetadata.All.min_se.id]: Number(ciqualEntry['min-se-ug']),
        [FoodMetadata.All.min_na.id]: Number(ciqualEntry['min-na-mg']),
        [FoodMetadata.All.min_zn.id]: Number(ciqualEntry['min-zn-mg']),
        // missing from data source: mo, cr 
        // unused data: 'vit-a-ug'
        [FoodMetadata.All.provit_a.id]: Number(ciqualEntry['vit-pra-ug']),
        [FoodMetadata.All.vit_c.id]: Number(ciqualEntry['vit-c-mg']),
        [FoodMetadata.All.vit_d.id]: Number(ciqualEntry['vit-d-ug']),
        [FoodMetadata.All.vit_e.id]: Number(ciqualEntry['vit-e-mg']),
        [FoodMetadata.All.vit_k.id]: Number(ciqualEntry['vit-k-ug']),
        [FoodMetadata.All.vit_b1.id]: Number(ciqualEntry['vit-b1-mg']),
        [FoodMetadata.All.vit_b2.id]: Number(ciqualEntry['vit-b2-mg']),
        [FoodMetadata.All.vit_b3.id]: Number(ciqualEntry['vit-b3-mg']),
        [FoodMetadata.All.vit_b5.id]: Number(ciqualEntry['vit-b5-mg']),
        [FoodMetadata.All.vit_b6.id]: Number(ciqualEntry['vit-b6-mg']),
        [FoodMetadata.All.vit_b9.id]: Number(ciqualEntry['vit-b9-ug']),
        [FoodMetadata.All.vit_b12.id]: Number(ciqualEntry['vit-b12-ug']),
        // missing from data source: choline, b7
    };

    function getCategory(ciqualEntry) {
        switch (ciqualEntry['alim_grp_nom_eng']) {
            case 'beverages':
                return FoodCategory.beverages.id
            case 'cereal products':
                return FoodCategory.cereals.id
            case 'fats and oils':
                return FoodCategory.fats.id
            case 'ice cream and sorbet':
            case 'sugar and confectionery':
                return FoodCategory.sugar.id
    
    
            // TODO KEEP GOING
    
    
            default:
                return FoodCategory.misc.id
        };
    }
    
    function getKcalFromMacros(entry) {
        return 0
            + Number(entry['mac-prot'] || 0) * 4
            + Number(entry['mac-carb'] || 0) * 4
            + Number(entry['mac-fat'] || 0) * 9
            + Number(entry['mac-alc'] || 0) * 7;
    }

}