import * as FoodMetadata from 'model/FoodMetadata';

/// Meals and foods share the same fields concerning food nutrients.
/// This function takes the quantity of the meal, e.g., 150g, and
/// calculate each nutrient (stored in a food as if the quantity was 100g).
export function foodToMeal({ food, mealQuantity, timestamp }) {
    if (isNaN(mealQuantity)) {
        console.error(`The quantity of the meal is NaN: ${mealQuantity}`);
        return;
    }
    const factor = Number(Number(mealQuantity) / 100);
    
    const nutrients = Object.values(FoodMetadata.All).reduce(
        (buffer, nutrientMetadata) => !food[nutrientMetadata.id] || !isNaN(food[nutrientMetadata.id])
            ? { ...buffer, [nutrientMetadata.id]: Number((Number(food[nutrientMetadata.id]) || 0) * factor) }
            : buffer,
        /* initialValue: */ {});
    const meal = {
        label: food.label,
        food_code: food.code,
        date_consumption: timestamp,
        quantity: mealQuantity,
        category: food.category,
        ...nutrients,
    };
    return meal;
}
