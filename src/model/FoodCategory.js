export default {
  composed: {
    id: "composed",
    label: "Composed food"
  },
  fruits: {
    id: "fruits",
    label: "Fruits"
  },
  vegetables: {
    id: "vegetables",
    label: "Vegetables"
  },
  legumes: {
    id: "legumes",
    label: "Legumes"
  },
  nuts: {
    id: "nuts",
    label: "Nuts"
  },
  cereals: {
    id: "cereals",
    label: "Cereals"
  },
  fats: {
    id: "fats",
    label: "Fats and oils"
  },
  sugar: {
    id: "sugar",
    label: "Sugary"
  },
  beverages: {
    id: "beverages",
    label: "Beverages"
  },
  misc: {
    id: "misc",
    label: "Miscellaneous"
  },
}
