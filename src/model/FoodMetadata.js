import UNITS_METADATA from './Units'

// TODO introduce some medical knowledge with tooltips (e.g. explain what is n-3 fatty acids good for, explain technical terms, etc.)
/**
 * RDA: recommanded daily allowance; minimum value a person should consume on average every day
 * UL: tolerable upper intake level; maximum value a person sould consume on average every day
 */
export const All = {
    /* macronutrients */
    kcal: {
        id: 'kcal',
        required: true,
        unit: UNITS_METADATA.kcal,
        label: 'Energy',
        description: 'Kilocalories per 100g',
    },
    proteins: {
        id: 'proteins',
        required: true,
        unit: UNITS_METADATA.g,
        label: 'Proteins',
        description: 'Proteins per 100g',
    },
    fat: {
        id: 'fat',
        required: true,
        unit: UNITS_METADATA.g,
        label: 'Fat',
        description: 'Fat per 100g',
    },
    carbohydrates: {
        id: 'carbohydrates',
        required: true,
        unit: UNITS_METADATA.g,
        label: 'Carbohydrates',
        description: 'Carbohydrates, including sugar, per 100g',
    },
    alcohol: {
        id: 'alcohol',
        unit: UNITS_METADATA.g,
        label: 'Alcohol',
        description: 'Alcohol per 100g',
    },
    /* detailed macronutrients */
    fibres: {
        id: 'fibres',
        unit: UNITS_METADATA.g,
        label: 'Dietary fibres',
    },
    carbs_sugar: {
        id: 'carbs_sugar',
        unit: UNITS_METADATA.g,
        label: 'Sugar',
    },
    salt: {
        id: 'salt',
        unit: UNITS_METADATA.g,
        label: 'Salt',
    },
    cholesterol: {
        id: 'cholesterol',
        unit: UNITS_METADATA.g,
        label: 'Cholesterol',
    },
    fat_saturated: {
        id: 'fat_saturated',
        unit: UNITS_METADATA.g,
        label: 'Saturated fat',
    },
    fat_fa_n9: {
        id: 'fat_fa_n9',
        unit: UNITS_METADATA.g,
        label: 'Monounsaturated fatty acids',
    },
    fat_fa_polyunsaturated: {
        // note: includes below n6 and n3
        id: 'fat_fa_polyunsaturated',
        unit: UNITS_METADATA.g,
        label: 'Polyunsaturated fatty acids',
    },
    fat_fa_n6: {
        id: 'fat_fa_n6',
        unit: UNITS_METADATA.g,
        label: 'Omega-6 fatty acids',
    },
    fat_fa_n3: {
        id: 'fat_fa_n3',
        unit: UNITS_METADATA.g,
        label: 'Omega-3 fatty acids',
    },
    /* vitamins */
    vit_c: {
        id: 'vit_c',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin C',
        rdaLowerBound: '90',
        symbol: 'C',
    },
    vit_d: {
        id: 'vit_d',
        unit: UNITS_METADATA.mcg,
        label: 'Vitamin D',
        rdaLowerBound: '15',
        ulUpperBound: '100',
        symbol: 'D',
    },
    vit_e: {
        id: 'vit_e',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin E',
        rdaLowerBound: '15',
        ulUpperBound: '300',
        symbol: 'E',
    },
    vit_k: {
        id: 'vit_k',
        unit: UNITS_METADATA.mcg,
        label: 'Vitamin K',
        rdaLowerBound: '110',
        symbol: 'K',
    },
    provit_a: {
        // provitamin A such as β-Carotene, do not mix us with retinol (actual vit A)
        id: 'provit_a',
        unit: UNITS_METADATA.mcg,
        label: 'Provitamin A',
        rdaLowerBound: '5400',
        symbol: 'Pro-A',
    },
    vit_b1: {
        id: 'vit_b1',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin B1',
        labelSub: '1',
        rdaLowerBound: '1.2',
        symbol: 'B1',
    },
    vit_b2: {
        id: 'vit_b2',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin B2',
        labelSub: '2',
        rdaLowerBound: '1.3',
        symbol: 'B2',
    },
    vit_b3: {
        id: 'vit_b3',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin B3',
        labelSub: '3',
        rdaLowerBound: '16',
        ulUpperBound: '35',
        symbol: 'B3',
    },
    vit_b5: {
        id: 'vit_b5',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin B5',
        labelSub: '5',
        rdaLowerBound: '5',
        symbol: 'B5',
    },
    vit_b6: {
        id: 'vit_b6',
        unit: UNITS_METADATA.mg,
        label: 'Vitamin B6',
        labelSub: '6',
        rdaLowerBound: '1.7',
        ulUpperBound: '25',
        symbol: 'B6',
    },
    vit_b7: {
        id: 'vit_b7',
        unit: UNITS_METADATA.mcg,
        label: 'Vitamin B7',
        labelSub: '7',
        rdaLowerBound: '30',
        symbol: 'B7',
    },
    vit_b9: {
        id: 'vit_b9',
        unit: UNITS_METADATA.mcg,
        label: 'Vitamin B9',
        labelSub: '9',
        rdaLowerBound: '400',
        ulUpperBound: '1000',
        symbol: 'B9',
    },
    vit_b12: {
        id: 'vit_b12',
        unit: UNITS_METADATA.mcg,
        label: 'Vitamin B12',
        labelSub: '12',
        rdaLowerBound: '2.4',
        symbol: 'B12',
    },
    vit_choline: {
        id: 'vit_choline',
        unit: UNITS_METADATA.mg,
        label: 'Choline',
        rdaLowerBound: '550',
        ulUpperBound: '3500',
        symbol: 'Ch',
    },
    /* minerals */
    min_ca: {
        id: 'min_ca',
        unit: UNITS_METADATA.mg,
        label: 'Calcium',
        rdaLowerBound: '1200',
        ulUpperBound: '2500',
        symbol: 'Ca',
    },
    min_cl: {
        id: 'min_cl',
        unit: UNITS_METADATA.mg,
        label: 'Chlorine',
        rdaLowerBound: '2300',
        ulUpperBound: '3600',
        symbol: 'Cl',
    },
    min_cr: {
        id: 'min_cr',
        unit: UNITS_METADATA.mcg,
        label: 'Chromium',
        rdaLowerBound: '35',
        symbol: 'Cr',
    },
    min_cu: {
        id: 'min_cu',
        unit: UNITS_METADATA.mg,
        label: 'Copper',
        rdaLowerBound: '0.9',
        ulUpperBound: '5',
        symbol: 'Cu',
    },
    min_fe: {
        id: 'min_fe',
        unit: UNITS_METADATA.mg,
        label: 'Iron',
        rdaLowerBound: '18',
        ulUpperBound: '45',
        symbol: 'Fe',
    },
    min_i: {
        id: 'min_i',
        unit: UNITS_METADATA.mcg,
        label: 'Iodine',
        rdaLowerBound: '150',
        ulUpperBound: '600',
        symbol: 'I',
    },
    min_k: {
        id: 'min_k',
        unit: UNITS_METADATA.mg,
        label: 'Potassium',
        rdaLowerBound: '4700',
        symbol: 'K',
    },
    min_mg: {
        id: 'min_mg',
        unit: UNITS_METADATA.mg,
        label: 'Magnesium',
        rdaLowerBound: '300',
        symbol: 'Mg',
    },
    min_mn: {
        id: 'min_mn',
        unit: UNITS_METADATA.mg,
        label: 'Manganese',
        rdaLowerBound: '2.3',
        ulUpperBound: '11',
        symbol: 'Mn',
    },
    min_mo: {
        id: 'min_mo',
        unit: UNITS_METADATA.mcg,
        label: 'Molybdenum',
        rdaLowerBound: '45',
        ulUpperBound: '600',
        symbol: 'Mo',
    },
    min_na: {
        id: 'min_na',
        unit: UNITS_METADATA.mg,
        label: 'Sodium',
        rdaLowerBound: '1500',
        symbol: 'Na',
    },
    min_p: {
        id: 'min_p',
        unit: UNITS_METADATA.mg,
        label: 'Phosphorus',
        rdaLowerBound: '700',
        ulUpperBound: '4000',
        symbol: 'P',
    },
    min_se: {
        id: 'min_se',
        unit: UNITS_METADATA.mcg,
        label: 'Selenium',
        rdaLowerBound: '55',
        ulUpperBound: '300',
        symbol: 'Se',
    },
    min_zn: {
        id: 'min_zn',
        unit: UNITS_METADATA.mg,
        label: 'Zinc',
        rdaLowerBound: '11',
        ulUpperBound: '25',
        symbol: 'Zn',
    },
};

export const MainMacronutrients = [
    All.kcal,
    All.proteins,
    All.fat,
    All.carbohydrates,
    All.alcohol,
];

export const DetailedMacronutrients = [
    All.fibres,
    All.carbs_sugar,
    All.salt,
    All.cholesterol,
    All.fat_saturated,
    All.fat_fa_n9, // is displayed as monounsaturated
    All.fat_fa_polyunsaturated,
    All.fat_fa_n6,
    All.fat_fa_n3,
];

export const Vitamins = [
    All.vit_c,
    All.vit_d,
    All.vit_e,
    All.vit_k,
    All.provit_a,
    // All.vit_choline, // detactivated: no data sources
    All.vit_b1,
    All.vit_b2,
    All.vit_b3,
    All.vit_b5,
    All.vit_b6,
    // All.vit_b7, // detactivated: no data sources
    All.vit_b9,
    All.vit_b12,
];

export const Minerals = [
    All.min_ca,
    All.min_cl,
    // All.min_cr, // detactivated: no data sources
    All.min_cu,
    All.min_fe,
    All.min_i,
    All.min_k,
    All.min_mg,
    All.min_mn,
    // All.min_mo, // detactivated: no data sources
    All.min_na,
    All.min_p,
    All.min_se,
    All.min_zn,
];