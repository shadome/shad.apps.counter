import * as React from 'react';

export const ThemeContext = React.createContext(null);

export const DisplayContext = React.createContext('display');

export const ThemeModeContext = React.createContext('dark');

export const BreakpointContext = React.createContext(null);

export const SetTitleContext = React.createContext(() => { });

export const IsFoodDatabaseEmptyContext = React.createContext(null);

export const AddFeedbackContext = React.createContext(() => { });

export const CssContext = React.createContext(null);