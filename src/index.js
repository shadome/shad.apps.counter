'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
// import CssInteropInitialiser from './front/CssInterop';

// import './normalize.css';
import './counter.css';

// Note: the code below needs to be nested in the App component, else React won´t understand its hot changes
// ReactDOM.render(<CssInteropInitialiser />, document.querySelector('#head-style'));
ReactDOM.render(<App />, document.querySelector('#app'));
