/*
 * Counters are little tiles which can be added to the dashboard 
 * and whose sole purpose is to keep track of a value over time on a daily basis.
 * They come with some additional customisation settings such as checkable tags,
 * today's colour, etc.
 * Exemples: counting one's weight in kg, number of minutes doing physical exercise, number of glasses of water drunk, etc.
 * This store keeps track of the created counters and their settings, not the daily values (cf. StoreCounterValues)..
 * 
 * Fields:
 * - id (key, int, auto-incremented)
 * - is_active (whether the counter is displayed on the dashboard - true, or not - false)
 * - title (a string representing the descriptive label of the counter, i.e., what is counted)
 * - tags (an array of strings of the tags which can be checked)
 * - new_day_default_value (value for a new day, number)
 * - starts_with_last_count (true if a new day should start with the closest previous counter value, 
 *      if any, else with new_day_default_value)
 * - step (value added or subtracted to the current count when the + (resp. -) button is hit, number)
 * - decimal_precision (number of digits after the coma, can be 0, 1 or 2)
 * - is_history_enabled (true if the last days' values are displayed in the counter tile)
 * - evolution_percent_objective (whether the last days' values' percent difference compared to today are displayed, 
 *      and the objective is to maximise or minimise the overall value, can be 'none', 'mini', or 'maxi')
 */

import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 0,
    name: 'st_counter',
    options: { keyPath: 'id', autoIncrement: true },
    indexes: [
        { version: 0, name: 'title', keypath: 'title', options: { unique: true } },
    ],
};

// private function to remove unnecessary fields and provide default values for missing ones
const Fields = {
    id: {
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    title: {
        toDbEntity: x => x.toString().trim(),
        toJsObject: x => x,
        checker: x => Boolean(x?.toString()?.trim()),
    },
    is_active: {
        default: true,
        toDbEntity: x => Boolean(x),
        toJsObject: x => Boolean(x),
        checker: x => x !== undefined,
    },
    step: {
        default: 1,
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    tags: {
        default: [],
        toDbEntity: x => x,
        toJsObject: x => x,
        checker: x => Array.isArray(x),
    },
    new_day_default_value: {
        default: 0,
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    starts_with_last_count: {
        default: false,
        toDbEntity: x => Boolean(x),
        toJsObject: x => Boolean(x),
        checker: x => true,
    },
    decimal_precision:  {
        default: 0,
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    is_history_enabled: {
        default: false,
        toDbEntity: x => Boolean(x),
        toJsObject: x => Boolean(x),
        checker: x => true,
    },
    evolution_percent_objective: {
        default: 'none',
        toDbEntity: x => x.toString().toLowerCase(),
        toJsObject: x => x,
        checker: x => ['none', 'mini', 'maxi'].includes(x?.toString().toLowerCase()),
    },
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( */

/// Create a new counter.
/// The given item must specify
export async function Create({
    item = undefined,
} = {}) {

    if (Boolean(item)) {
        item = MainDatabase.formatItem({
            fieldsTemplate: Fields,
            item: item,
            isCreation: true,
            isWritingToDatabase: true,
        });
    }
    // not an `else` because the above `if` reassigns `item`
    if (!Boolean(item)) {
        console.warn('the given item could not be created');
        return Promise.reject('the given item could not be created, check your navigator\'s warnings');
    }
    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: [item],
        currentProgressCallback: undefined,
    });
}

export async function Read({
    id = undefined,
    title = undefined,
} = {}) {
    // the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const [indexToUse, retrivalMethodToUse] =
        id !== undefined ?    [x => x, x => x.get(Number(id))] :
        title !== undefined ? [x => x.index('title'), x => x.get(title.toString())] :
        /* --- default --- */ [x => x, x => x.getAll()];

    return MainDatabase
        .Read({
            storeName: StoreInfo.name,
            indexAccessorFunc: indexToUse,
            readFunc: retrivalMethodToUse,
        })
        .then(_items =>
            _items.map(_item => MainDatabase.formatItem({
                fieldsTemplate: Fields,
                item: _item,
                isCreation: false,
                isWritingToDatabase: false,
            }))
        );
}

/// Update the item with the given id.
/// All values found in the newValues object will either override existing values or be added.
/// Existing item's values are preserved when not overriden.
export async function Update({
    updatedItem = undefined,
} = {}) {
    if (Boolean(updatedItem)) {
        updatedItem = MainDatabase.formatItem({
            fieldsTemplate: Fields,
            item: updatedItem,
            isCreation: false,
            isWritingToDatabase: true,
        });
    }
    // not an `else` because the above `if` reassigns `item`
    if (!Boolean(updatedItem)) {
        console.warn('the given item could not be updated');
        return Promise.reject('the given item could not be updated, check your navigator\'s warnings');
    }
    return MainDatabase.Update({
        storeName: StoreInfo.name,
        itemsToUpdate: [updatedItem],
        currentProgressCallback: undefined,
    })
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Clear() {
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: x => x.clear(),
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Delete({
    id = undefined,
} = {}) {
    const deleteMethodToUse = id !== undefined
        ? x => x.delete(Number(id))
        : x => x.clear();

    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: deleteMethodToUse,
    });
}