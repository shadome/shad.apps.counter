/*
 * Foods are elements that hold nutritional data (macronutrients, minerals, vitamins) which will be counted.
 * Foods are used to define meals, recipes, check for which food is rich in a certain nutrients, etc.
 * All nutrients of each row in this store assume a 100 gram quantity of the food, hence the `_pctgr` suffix.
 * The food store should initially be filled with an out-of-the-box food database (the current data source is Ciqual's 2020) 
 * and then be amended or completed by the user.
 * A food_code is kept for foods which come from a reference database but not used yet.
 * 
 * Fields:
 * - id (key, int, auto-incremented)
 * - code (dependant on the food database being used, at this point: ciqual's food "code")
 * - label
 * - category (legumes, drinks, sugary, fruits, etc., check model/FoodCategory not exploited yet)
 * - kcal_pctgr, fat_pctgr, prot_pctgr, carbs_pctgr, fibre_pctgr, plus all Vitamins and Minerals from the model/FoodMetadata's similarly named lists 
 *    (nutrient value in respective unit for 100g of meal, the unit for each nutrient can be found in model/Units)
 * - [OBSOLETE] is_user_defined (bool, whether the food has been created by the user and should not be purged with the base food database)
 * - data_source (enum: "user_defined", "ciqual")
 * - nominal_quantity (typical serving for this food / quantity in grams which should be pre-entered when selecting this food for adding a meal)
 */

import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 0,
    name: 'st_food',
    options: { keyPath: 'id', autoIncrement: true },
    indexes: [
        { version: 0, name: 'category', keypath: 'category', options: { unique: false } },
        { version: 0, name: 'code', keypath: 'code', options: { unique: true } },
        // deleted in version 2
        // { version: 1, name: 'is_user_defined', keypath: 'is_user_defined', options: { unique: false } },
        { version: 3, name: 'data_source', keypath: 'data_source', options: { unique: false } },
    ],
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( */

export async function Create({
    item = undefined,
    items = [],
    // function returning an int to notify the caller of the current item's index being processed, in case they want to handle any sort of progress callback
    currentProgressCallback = undefined,
} = {}) {
    const itemsToAdd = item !== undefined ? [item, ...items] : items;

    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: itemsToAdd,
        currentProgressCallback: currentProgressCallback,
    });
}

export async function Read({
    id = undefined,
    code = undefined,
    // same as code, but matches multiple values
    codes = undefined,
    data_source = undefined,
} = {}) {
    // the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, retrievalMethodToUse, hasCursor = false, cursorElementPredicate = undefined } = ( // affectation-switch syntax
        id !== undefined ? {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.get(Number(id)),
        } :
        codes !== undefined ? {
            indexToUse: x => x.index('code'),
            retrievalMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: food => codes.includes(food.code),
        } :
        code !== undefined ? {
            indexToUse: x => x.index('code'),
            retrievalMethodToUse: x => x.get(code.toString()),
        } :
        data_source !== undefined ? {
            indexToUse: x => x.index('data_source'),
            retrievalMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: food => food.data_source === data_source,
        } :
        /* -- default --- */ {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.getAll(),
        });

    return MainDatabase.Read({
        storeName: StoreInfo.name,
        indexAccessorFunc: indexToUse,
        readFunc: retrievalMethodToUse,
        hasCursor,
        cursorElementPredicate,
    });
}

/// Update the item with the given id.
/// updatedItem(s) must contain the id to modify and the new values.
/// Existing item's values are preserved when not overriden, updatedItem(s) do not need to include them.
export async function Update({
    updatedItem = undefined,
    updatedItems = [],
    // function returning an int to notify the caller of the current item's index being processed, in case they want to handle any sort of progress callback
    currentProgressCallback = undefined,
} = {}) {
    const itemsToUpdate = updatedItem !== undefined
        ? [updatedItem, ...updatedItems]
        : [...updatedItems];

    return MainDatabase.Update({
        storeName: StoreInfo.name,
        itemsToUpdate: itemsToUpdate,
        currentProgressCallback: currentProgressCallback,
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Clear() {
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: x => x.clear(),
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Delete({
    // id to delete
    id = undefined,
    // only rows matching this data_source value will be deleted
    data_source = undefined,
} = {}) {
    // the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, deletionMethodToUse, hasCursor = false, cursorElementPredicate = undefined } = ( // affectation-switch syntax
        id !== undefined ? {
            indexToUse: x => x,
            deletionMethodToUse: x => x.delete(Number(id)),
        } :
        data_source !== undefined ? {
            indexToUse: x => x.index('data_source'),
            deletionMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: food => food.data_source === data_source,
        } :
        /* -- default --- */ {
            indexToUse: x => x,
            deletionMethodToUse: x => x.clear(),
        });
        
    
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        indexAccessorFunc: indexToUse,
        deleteFunc: deletionMethodToUse,
        hasCursor: hasCursor,
        cursorElementPredicate: cursorElementPredicate,
    });
}