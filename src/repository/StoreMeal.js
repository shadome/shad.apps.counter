/*
 * Meals are instances of foods that the user has eaten during a specific point in time.
 * All food (nutrient per 100g) data are copied so that this store is not relational and its data is standalone.
 * A food_code is kept but not used yet.
 *
 * NOTE: This store is mostly redundant with the FoodStore: all nutrients found in the food list are copied in this store, 
 * calculated with regards to the actual meal's quantity. The redundancy allows:
 * - to change the data source (the 'food_code' is not used yet),
 * - to calculate statistics on past meals easily, as all the nutrient are calculated from the meal's quantity before the storage
 * 
 * NOTE: Since foods include very low-level elements such as spices, meals should be grouped / groupable, esp. when displayed, 
 * to avoid flooding the user with too many unwanted information. To achieve this, rather than using two relational stores 
 * (e.g., meal groups and meal entries), some fields of this store are at the graularity of a meal group (group_label, date_consumption)
 * and are therefore redundant (their values are duplicated for all the meal group's entries) whereas the rest of the fields are at the 
 * lowest granularity, the store row granularity.
 * 
 * Fields:
 * - id [PK auto-incremented]
 * - meal_id [NOT NULL] [FK StoreMeal.id]
 * - label [NOT NULL] (equal to the food's)
 * - group_label (user defined, can be null if the meal has only one entry)
 * - food_code [NOT NULL] (not used but just in case, also dependant on the food database being used, at this point: ciqual's food "CODE", not id)
 * - date_consumption [NOT NULL] (both date and time part are useful)
 * - quantity [NOT NULL] (always in grams, even for liquids)
 * - category [NOT NULL] (legumes, drinks, sugary, fruits, etc., not exploited yet)
 * - kcal, fat, prot, carbs, fibre, plus all Vitamins and Minerals from the model/FoodMetadata's similarly named lists 
 *    (nutrient value in respective unit for the actual quantity of meal, the unit for each nutrient can be found in model/Units)
 *
 */

import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 0,
    name: 'st_meal',
    options: { keyPath: 'id', autoIncrement: true },
    indexes: [
        { version: 0, name: 'date_consumption', keypath: 'date_consumption', options: { unique: false } },
    ],
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( - also, cf. StoreFood's expanded comments */

export async function Create({
    item = undefined,
    items = [],
    // function returning an int to notify the caller of the current item's index being processed, in case they want to handle any sort of progress callback
    currentProgressCallback = undefined,
} = {}) {
    const itemsToAdd = item !== undefined ? [item, ...items] : items;

    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: itemsToAdd,
        currentProgressCallback: currentProgressCallback,
    });
}

// Both min and max are included
export async function Read({
    id = undefined,
    min_date_consumption = undefined,
    max_date_consumption = undefined,
} = {}) {
    // note: use key range if necessary
    // note: the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, retrievalMethodToUse, hasCursor = false } = (// affectation-switch syntax
        id !== undefined ? {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.get(Number(id)),
        } :
        min_date_consumption !== undefined || max_date_consumption !== undefined ? {
            indexToUse: x => x.index('date_consumption'),
            retrievalMethodToUse: // careful, reverted boolean logic, being in this scope means at least one date is NOT undefined
                min_date_consumption === undefined ? x => x.openCursor(IDBKeyRange.upperBound(max_date_consumption)) :
                max_date_consumption === undefined ? x => x.openCursor(IDBKeyRange.lowerBound(min_date_consumption)) :
                /* -- default: both are defined - */ x => x.openCursor(IDBKeyRange.bound(min_date_consumption, max_date_consumption)),
            hasCursor: true,
        } :
        /* -- default -- */ {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.getAll(),
        }
    );

    return MainDatabase.Read({
        storeName: StoreInfo.name,
        indexAccessorFunc: indexToUse,
        readFunc: retrievalMethodToUse,
        hasCursor: hasCursor,
    });
}

/// Update the item with the given id.
/// updatedItem(s) must contain the id to modify and the new values.
/// Existing item's values are preserved when not overriden, updatedItem(s) do not need to include them.
export async function Update({
    updatedItem = undefined,
    updatedItems = [],
    // function returning an int to notify the caller of the current item's index being processed, in case they want to handle any sort of progress callback
    currentProgressCallback = undefined,
} = {}) {
    const itemsToUpdate = updatedItem !== undefined
        ? [updatedItem, ...updatedItems]
        : [...updatedItems];

    return MainDatabase.Update({
        storeName: StoreInfo.name,
        itemsToUpdate: itemsToUpdate,
        currentProgressCallback: currentProgressCallback,
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Delete({
    id = undefined,
} = {}) {
    if (isNaN(id)) {
        return;
    }
    const deleteMethodToUse = x => x.delete(Number(id));

    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: deleteMethodToUse,
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Clear() {
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: x => x.clear(),
    });
}