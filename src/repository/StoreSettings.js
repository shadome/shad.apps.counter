import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 0,
    name: 'st_settings',
    options: { keyPath: 'id', autoIncrement: true },
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( */

export async function Create({
    item = undefined,
} = {}) {
    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: [item],
        currentProgressCallback: undefined,
    });
}

export async function Read({
    // id = undefined, // unused and unhandled yet
} = {}) {
    return MainDatabase.Read({
        storeName: StoreInfo.name,
        indexAccessorFunc: x => x,
        readFunc: x => x.getAll(),
    });
}

/// Update the item with the given id.
/// All values found in the newValues object will either override existing values or be added.
/// Existing item's values are preserved when not overriden.
export async function Update({
    updatedItem = undefined,
} = {}) {
    return MainDatabase.Update({
        storeName: StoreInfo.name,
        itemsToUpdate: [updatedItem],
        currentProgressCallback: undefined,
    })
}

export async function Delete({
    // id = undefined, // unused and unhandled yet
} = {}) {
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        deleteFunc: x => x.clear(),
    });
}