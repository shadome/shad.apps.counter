/*
 * Counters are little tiles which can be added to the dashboard 
 * and whose sole purpose is to keep track of a value over time on a daily basis.
 * They come with some additional customisation settings such as checkable tags,
 * today's colour, etc.
 * Exemples: counting one's weight in kg, number of minutes doing physical exercise, number of glasses of water drunk, etc.
 * This store keeps track of the created counters and their settings, not the daily values (cf. StoreCounterValues)..
 * 
 * Fields:
 * - id (key, int, auto-incremented)
 * - counter_id (FK towards st_counter)
 * - date (day for the value, in dayjs's milliseconds, for range comparisons and number operations)
 * - value (value for this counter for this day)
 */

import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 3,
    name: 'st_counter_values',
    options: { keyPath: 'id', autoIncrement: true },
    indexes: [
        { version: 3, name: 'counter_id', keypath: 'counter_id', options: { unique: false } },
        { version: 3, name: 'date', keypath: 'date', options: { unique: false } },
    ],
};

// private function to remove unnecessary fields and provide default values for missing ones
const Fields = {
    id: {
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    counter_id: {
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    date: {
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
    value: {
        toDbEntity: x => Number(x),
        toJsObject: x => Number(x),
        checker: x => !isNaN(x),
    },
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( */

/// Create a new counter value.
export async function Create({
    item = undefined,
} = {}) {

    if (Boolean(item)) {
        item = MainDatabase.formatItem({
            fieldsTemplate: Fields,
            item: item,
            isCreation: true,
            isWritingToDatabase: true,
        });
    }
    // not an `else` because the above `if` reassigns `item`
    if (!Boolean(item)) {
        console.warn('the given item could not be created');
        return Promise.reject('the given item could not be created, check your navigator\'s warnings');
    }
    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: [item],
        currentProgressCallback: undefined,
    });
}

export async function Read({
    counter_id = undefined,
    min_date = undefined,
    max_date = undefined,
} = {}) {
    // note: use key range if necessary
    // note: the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, retrievalMethodToUse, hasCursor = false } = (// affectation-switch syntax
        counter_id !== undefined ? {
            indexToUse: x => x.index('counter_id'),
            retrievalMethodToUse: x => x.get(Number(counter_id)),
        } :
        min_date !== undefined || max_date !== undefined ? {
            indexToUse: x => x.index('date_consumption'),
            retrievalMethodToUse: // careful, reverted boolean logic, being in this scope means at least one date is NOT undefined
                min_date === undefined ? x => x.openCursor(IDBKeyRange.upperBound(max_date)) :
                max_date === undefined ? x => x.openCursor(IDBKeyRange.lowerBound(min_date)) :
                /* -- default: both are defined - */ x => x.openCursor(IDBKeyRange.bound(min_date, max_date)),
            hasCursor: true,
        } :
        /* -- default -- */ {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.getAll(),
        }
    );
    return MainDatabase
        .Read({
            storeName: StoreInfo.name,
            indexAccessorFunc: indexToUse,
            readFunc: retrivalMethodToUse,
            hasCursor: hasCursor,
        })
        .then(_items =>
            _items.map(_item => MainDatabase.formatItem({
                fieldsTemplate: Fields,
                item: _item,
                isCreation: false,
                isWritingToDatabase: false,
            }))
        );
}

/// Update the item with the given id.
/// All values found in the newValues object will either override existing values or be added.
/// Existing item's values are preserved when not overriden.
export async function Update({
    updatedItem = undefined,
} = {}) {
    if (Boolean(updatedItem)) {
        updatedItem = MainDatabase.formatItem({
            fieldsTemplate: Fields,
            item: updatedItem,
            isCreation: false,
            isWritingToDatabase: true,
        });
    }
    // not an `else` because the above `if` reassigns `item`
    if (!Boolean(updatedItem)) {
        console.warn('the given item could not be updated');
        return Promise.reject('the given item could not be updated, check your navigator\'s warnings');
    }
    return MainDatabase.Update({
        storeName: StoreInfo.name,
        itemsToUpdate: [updatedItem],
        currentProgressCallback: undefined,
    })
}
