/*
 * Food relations are the link which may exist between foods when a food is composed other lower-level foods.
 * This is a relational table inside a non-relational database because it is still the best way to store
 * such informations, recursive by nature.
 * Food relations will typically be created when a user creates a group meal and decides to transform the group meal
 * into a standalone food. In this use case, one food creation leads to the creation of multiple rows in this store.
 * 
 * Fields:
 * - id (key, int, auto-incremented)
 * - superfood_code (code of the food store referencing the food that is composed of other (sub-)foods)
 * - subfood_code (code of the food store referencing the food composing, along with other subfoods, a superfood)
 * - quantity_pct (multiple subfoods compose one superfood ; this field stores the share of this specific subfood for the composition, in percent)
 * - quantity_abs (multiple subfoods compose one superfood ; this field stores the share of this specific subfood for the composition, in grams)
 * 
 * Invariants:
 * - the tuple (superfood_code, subfood_code) should be unique for each record
 * - the sum of every values in the field quantity_pct should make 100 for a given superfood_code
 * 
 * Notes:
 * - quantity_abs is kept so that this store can be considered, in some cases, as providing the recipe of the superfood,
 *      e.g., superfood: 'pancakes', subfoods: [{flour: 200g}, {milk: 500g}, ...]
 */

import * as MainDatabase from './DatabaseMain';

export const StoreInfo = {
    version: 1,
    name: 'st_food_relations',
    options: { keyPath: 'id', autoIncrement: true },
    indexes: [
        { version: 1, name: 'superfood_code', keypath: 'superfood_code', options: { unique: false } },
        { version: 1, name: 'subfood_code', keypath: 'superfood_code', options: { unique: false } },
    ]
};

/* CRUD - the functions start with an uppercase because 'delete' is a reserved word :( */

export async function Create({
    item = undefined,
    items = [],
    // function returning an int to notify the caller of the current item's index being processed, in case they want to handle any sort of progress callback
    currentProgressCallback = undefined,
} = {}) {
    const itemsToAdd = item !== undefined ? [item, ...items] : items;

    return MainDatabase.Create({
        storeName: StoreInfo.name,
        itemsToAdd: itemsToAdd,
        currentProgressCallback: currentProgressCallback,
    });
}

export async function Read({
    id = undefined,
    superfood_code = undefined,
    // same as superfood_code, but matches multiple values
    superfood_codes = undefined,
    subfood_code = undefined,
    // same as subfood_code, but matches multiple values
    subfood_codes = undefined,
} = {}) {
    // the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, retrievalMethodToUse, hasCursor = false, cursorElementPredicate = undefined } = ( // affectation-switch syntax
        id !== undefined ? {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.get(Number(id)),
        } :
        superfood_codes !== undefined ? {
            indexToUse: x => x.index('superfood_code'),
            retrievalMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: foodRelation => superfood_codes.includes(foodRelation.superfood_code),
        } :
        superfood_code !== undefined ? {
            indexToUse: x => x.index('superfood_code'),
            retrievalMethodToUse: x => x.getAll(superfood_code.toString()),
        } :
        subfood_codes !== undefined ? {
            indexToUse: x => x.index('subfood_code'),
            retrievalMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: foodRelation => subfood_codes.includes(foodRelation.subfood_code),
        } :
        subfood_code !== undefined ? {
            indexToUse: x => x.index('subfood_code'),
            retrievalMethodToUse: x => x.getAll(subfood_code.toString()),
        } :
        /* -- default --- */ {
            indexToUse: x => x,
            retrievalMethodToUse: x => x.getAll(),
        });

    return MainDatabase.Read({
        storeName: StoreInfo.name,
        indexAccessorFunc: indexToUse,
        readFunc: retrievalMethodToUse,
        hasCursor,
        cursorElementPredicate,
    });
}

/// Clear and Delete are explicitly separate because
/// it is too risky to perform a clear when parameters
/// of the delete function have been messed up
export async function Delete({
    id = undefined,
    superfood_code = undefined,
} = {}) {
    if (id === undefined && superfood_code === undefined) {
        return Promise.reject();
    }

    // the order of the following instruction's ternaries is relevant and would decide which parameters are ignored if too many are given
    const { indexToUse, deletionMethodToUse, hasCursor = false, cursorElementPredicate = undefined } = ( // affectation-switch syntax
        id !== undefined ? {
            indexToUse: x => x,
            deletionMethodToUse: x => x.delete(Number(id)),
        } :
        superfood_code !== undefined ? {
            indexToUse: x => x.index('superfood_code'),
            deletionMethodToUse: x => x.openCursor(),
            hasCursor: true,
            cursorElementPredicate: food => food.superfood_code === superfood_code,
        } :
        /* -- default --- */ {
            indexToUse: x => x,
            deletionMethodToUse: () => {},
        });
        
    return MainDatabase.Delete({
        storeName: StoreInfo.name,
        indexAccessorFunc: indexToUse,
        deleteFunc: deletionMethodToUse,
        hasCursor: hasCursor,
        cursorElementPredicate: cursorElementPredicate,
    });
}