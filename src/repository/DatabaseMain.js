import * as StoreMeal from './StoreMeal';
import * as StoreSettings from './StoreSettings';
import * as StoreFood from './StoreFood';
import * as StoreFoodRelations from './StoreFoodRelations';
import * as StoreCounter from './StoreCounter';
import * as StoreCounterValues from './StoreCounterValues';
import FoodDataSource from 'model/FoodDataSource';

// private constants
const DB_NAME = "Main";
const DB_VERSION = 4;

const TRANSACTION_RW = 'readwrite';
const TRANSACTION_RO = 'readonly';

// private - handles the idb major version upgrades
async function upgrade(event) {
    const db = event.target.result;
    const oldVersion = event.oldVersion;
    const targetVersion = event.newVersion;
    const transaction = event.target.transaction;
    const stores = {
        ...getStoreInfoAndInstance(db, transaction, StoreMeal.StoreInfo),
        ...getStoreInfoAndInstance(db, transaction, StoreSettings.StoreInfo),
        ...getStoreInfoAndInstance(db, transaction, StoreFood.StoreInfo),
        ...getStoreInfoAndInstance(db, transaction, StoreFoodRelations.StoreInfo),
        ...getStoreInfoAndInstance(db, transaction, StoreCounter.StoreInfo),
        ...getStoreInfoAndInstance(db, transaction, StoreCounterValues.StoreInfo),
    };

    console.log(`[....] Global IndexedDB upgrade from version ${oldVersion} to version ${targetVersion}`);
    switch (oldVersion) {
        // Note: no break until the default clause. The cases also need to be ordered ascendingly.
        // This syntax allows any client to step to the version number it is currently up to, 
        // then apply every update in an orderly fashion until it is up to date.
        case 0:
            console.log(`[....] Upgrading from version 0 to version 1`);
            // create or update stores, indexes, etc. for the current version
            createOrUpdateStoreFromInfos(stores, 0);
            console.log(`[DONE] Upgrading from version 0 to version 1`);
        case 1:
            console.log(`[....] Upgrading from version 1 to version 2`);
            // create or update stores, indexes, etc. for the current version
            createOrUpdateStoreFromInfos(stores, 1);
            // update existing foods so that they are tagged as not user defined (in version 0, the foods were only created from Ciqual)
            const existingFoods_1 = await Read({
                _existingObjectStore: stores[StoreFood.StoreInfo.name].instance,
                indexAccessorFunc: x => x,
                readFunc: x => x.getAll(),
            });
            await Update({
                _existingObjectStore: stores[StoreFood.StoreInfo.name].instance,
                itemsToUpdate: existingFoods_1.map(food => ({
                    ...food,
                    is_user_defined: false,
                })),
            })
            console.log(`[DONE] Upgrading from version 1 to version 2`);
        case 2:
            console.log(`[...] Upgrading from version 2 to version 3`);
            // void because I made stupid mistakes in production
            console.log(`[DONE] Upgrading from version 2 to version 3`);
        case 3:
            console.log(`[...] Upgrading from version 3 to version 4`);
            // create or update stores, indexes, etc. for the current version
            createOrUpdateStoreFromInfos(stores, 3);
            // drop the bool index "is_user_defined" 
            const isUserDefinedIdx = 'is_user_defined';
            const doesIndexStillExist = stores[StoreFood.StoreInfo.name].instance.indexNames.contains(isUserDefinedIdx);
            if (doesIndexStillExist) {
                stores[StoreFood.StoreInfo.name].instance.deleteIndex(isUserDefinedIdx);
            }
            // drop the old table "StoreRecipe"
            const stRecipe = 'st_recipe';
            if (db.objectStoreNames.contains(stRecipe)) {
                db.deleteObjectStore('st_recipe'); 
            }
            // replacing the bool index "is_user_defined" by a str enum index "data_source" (values: ciqual, user)
            const existingFoods_2 = await Read({
                _existingObjectStore: stores[StoreFood.StoreInfo.name].instance,
                indexAccessorFunc: x => x,
                readFunc: x => x.getAll(),
            });
            await Update({
                _existingObjectStore: stores[StoreFood.StoreInfo.name].instance,
                itemsToUpdate: existingFoods_2
                    .map(food => ({
                        ...food,
                        data_source: food.is_user_defined ? FoodDataSource.user : FoodDataSouroce.ciqual,
                        is_user_defined: undefined,
                    })),
            });

            console.log(`[DONE] Upgrading from version 3 to version 4`);
        case 4:

            /* ************************************************************** */
            /* CONSIDER THIS BREAK AS PART OF THE FOLLOWING DEFAULT STATEMENT */
            /* ************************************************************** */
            break;
        default:
            console.warn(`Unexpected IDB version number: ${db.version}`);
    }
    console.log(`[DONE] Global IndexedDB upgrade from version ${oldVersion} to version ${targetVersion}`);
    // Note: do not close the database here, it would fire an error because it is closed in the encapsulation function requestDatabase.onsuccess (which, I suppose, triggers .onupgradeneeded whenever necessary)

    // private - aggregates all which is useful for one store: its infos, the IDB object store, etc.
    function getStoreInfoAndInstance(db, transaction, storeInfo) {
        return {
            [storeInfo.name]: {
                info: storeInfo,
                instance: db.objectStoreNames.contains(storeInfo.name)
                    ? transaction.objectStore(storeInfo.name)
                    : db.createObjectStore(storeInfo.name, storeInfo.options),
            }
        };
    }

    // private - factorises the code to create new new indexes, which could be called by different versions
    function createOrUpdateStoreFromInfos(stores, versionToCatchUp) {
        for (const key in stores) {
            if (Object.prototype.hasOwnProperty.call(stores, key)) {
                const store = stores[key];
                if (store.info.version > versionToCatchUp) {
                    // means this store doesn't exist for this version, will be created later
                    continue;
                }
                if (store.info.indexes) {
                    for (const index of store.info.indexes) {
                        if (index.version === versionToCatchUp) {
                            const _ = store.instance.createIndex(index.name, index.keypath, index.options);
                        }
                    }
                }
            }
        }
    }
    
}

// internal to the repository layer
// Takes a treatment in form of a function which will be given an open IDBDatabase object.
// Handles upgrades and errors on opening the database, along with closing it at the end of the treatment.
export function requestDatabase(request = undefined) {
    if (request === undefined) {
        return;
    }
    const databaseRequest = window.indexedDB.open(DB_NAME, DB_VERSION);
    databaseRequest.onerror = (evt) => {
        alert('Opening IndexedDB failed. This application needs your browser to be compatible with IndexedDB.');
        console.warn('Opening IndexedDB failed. This application needs your browser to be compatible with IndexedDB.');
        console.warn(evt);
    };
    databaseRequest.onupgradeneeded = (event) => upgrade(event);
    databaseRequest.onsuccess = (evt) => {
        const database = evt.target.result;
        request(database);
        /* Closing at this point should be OK.
         * From https://w3c.github.io/IndexedDB/#dom-idbdatabase-close
         * The close() method steps are:
         * Run close a database connection with this connection.
         * The connection will not actually close until all outstanding transactions have completed. Subsequent calls to close() will have no effect.
         */
        database.close();
    };
}

/* CRUD - the functions start with an uppercase because 'delete' is a reserved keyword :( */
/* The following functions (and their actual StoreXxx encapsulation) serve three purposes:
 * 1- encapsulating the complexity of the IDB API, 
 *    e.g., .onsuccess functions, etc.
 * 2- normalising the API to a basic CRUD design pattern (at least my implementation of it)
 *    e.g., one "read" function with parameters which holds the variants ".get() vs. .getAll() vs. .openCursor()", ".index() vs. none", with or without cursor ranges, etc.
 * 3- coupling CRUD functions with domain (businessful) parameters, 
 *    e.g., .read({ code: 42 }) (uses the index "code", reads all entries matching code 42), .read() (reads all values), .read({ id: 42 }) (reads the entry with id 42), etc.
 */

// internal to the repository layer
export async function Create({
    storeName,
    itemsToAdd = [],
    currentProgressCallback = undefined,
    _existingTransaction = undefined,
}) {
    /*
     * Dev's note:
     * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
     * "It rejects immediately upon any of the input promises rejecting or non-promises throwing an error, and will reject with this first rejection message / error."
     * Tested: if an insert fails, the whole transaction rolls back and the call is void.
     */
    function itemToAddPromise({ item, getCurrentIdx, objectStore }) {
        // !getCurrentIdx must only be called once!
        return new Promise((resolve, reject) => {
            const addItemRequest = objectStore.add(item);
            addItemRequest.onerror = (evt2) => {
                console.warn('[error] DatabaseMain.Create');
                console.warn(item);
                console.warn(evt2);
                reject();
            };
            addItemRequest.onsuccess = (evt2) => {
                // getCurrentIdx must be called in this scope, 
                // at the very moment there is an actual success and a resolve() call fired, 
                // in order to provide the caller with ordered indexes
                currentProgressCallback && currentProgressCallback(getCurrentIdx());
                resolve(evt2.target.result);
            };
        });
    };

    const resultPromise = new Promise((resolve, reject) => {
        requestDatabase((database) => {
            const transaction = _existingTransaction ?? database.transaction([storeName], TRANSACTION_RW);
            const objectStore = transaction.objectStore(storeName);
            let idx = 0;
            const getCurrentIdx = () => idx++;
            const promises = itemsToAdd.map((item) => itemToAddPromise({
                item: item,
                objectStore: objectStore,
                getCurrentIdx: getCurrentIdx,
            }));
            Promise
                .all(promises)
                .then(values => values && values.length === 1
                    ? resolve(values[0])
                    : resolve(values))
                .catch(reject);
        });
    });

    return resultPromise;
}

// internal to the repository layer
export async function Read({
    storeName,
    indexAccessorFunc,
    readFunc,
    hasCursor,
    cursorElementPredicate,
    _existingObjectStore = undefined,
}) {
    function treatment(resolve, reject, objectStore) {
        const objectStoreIndex = indexAccessorFunc(objectStore);
        const resultRequest = readFunc(objectStoreIndex);
        // two cases: either there is a cursor for data retrieval or there is not
        if (hasCursor) {
            let resultSet = [];
            resultRequest.onsuccess = (evt) => {
                const cursor = evt.target.result;
                if (cursor) {
                    // Two cases: either the cursor is opened with a constraint, like,
                    // `openCursor(IDBKeyRange.Xxx)`, or it is opened constraintless.
                    // The caller can provide a predicate to know if the current value
                    // matches, i.e., should be returned or not.
                    // It is expected to be provided in case #2,
                    // and can also be provided in case #1 if relevant.
                    const doesCurrentElementMatch = !cursorElementPredicate || cursorElementPredicate(cursor.value);
                    if (doesCurrentElementMatch) {
                        resultSet.push(cursor.value);
                    }
                    cursor.continue();
                } else {
                    resolve(resultSet);
                }
            };
        } else {
            resultRequest.onsuccess = (evt) => {
                resolve(evt.target.result);
            };
        }
        // note: resultRequest is instanciated above
        resultRequest.onerror = reject;
    }

    const resultPromise = new Promise((resolve, reject) => {
        // don't request a database if the object store is provided by the caller
        _existingObjectStore !== undefined
            ? treatment(resolve, reject, _existingObjectStore)
            : requestDatabase((database) => {
                const objectStore = database
                    .transaction([storeName], TRANSACTION_RO)
                    .objectStore(storeName);
                return treatment(resolve, reject, objectStore);
            });
    });
    return resultPromise;
}

// internal to the repository layer
export async function Update({
    storeName,
    itemsToUpdate = [],
    currentProgressCallback = undefined,
    _existingObjectStore = undefined,
} = {}) {
    function treatment(resolve, reject, objectStore) {let idx = 0;
        const getCurrentIdx = () => idx++;
        const promises = itemsToUpdate
            .map((item) => isNaN(Number(item.id))
                // cast the id field of the item so that it does not field if it is a serialised number
                ? Promise.reject('at least one item does not have an id')
                : { ...item, id: Number(item.id) })
            .map((item) => itemToUpdatePromise({
                item: item,
                objectStore: objectStore,
                getCurrentIdx: getCurrentIdx,
            }));
        Promise
            .all(promises)
            .then(values => values && values.length === 1
                ? resolve(values[0])
                : resolve(values))
            .catch(reject);
        
        function itemToUpdatePromise({
            // incomplete item to update (the values in this item must override the item with the same id in the store)
            item,
            // !getCurrentIdx must only be called once!
            getCurrentIdx,
            objectStore,
        }) {
            return new Promise((resolve, reject) => {
                const onRequestError = (evt) => {
                    console.warn('[error] DatabaseMain.Update');
                    console.warn(item);
                    console.warn(evt);
                    reject();
                };
                /* 1- read the item to get the values which must not be updated */
                const readItemRequest = objectStore.get(Number(item.id));
                readItemRequest.onerror = onRequestError;
                readItemRequest.onsuccess = (readEvt) => {
                    /* 2- merge the read item and the given one */
                    const itemToUpdate = {
                        ...readEvt.target.result,
                        ...item,
                    };
                    /* 3- put the merged item back in the store */
                    const updateItemRequest = objectStore.put(itemToUpdate);
                    updateItemRequest.onerror = onRequestError;
                    updateItemRequest.onsuccess = (updateEvt) => {
                        // getCurrentIdx must be called in this scope, 
                        // at the very moment there is an actual success and a resolve() call fired, 
                        // in order to provide the caller with ordered indexes
                        currentProgressCallback && currentProgressCallback(getCurrentIdx());
                        resolve(updateEvt.target.result);
                    };
                };
            });
        }
    }

    const resultPromise = new Promise((resolve, reject) => {
        // don't request a database if the object store is provided by the caller
        _existingObjectStore
            ? treatment(resolve, reject, _existingObjectStore)
            : requestDatabase((database) => {
                const objectStore = database
                    .transaction([storeName], TRANSACTION_RW)
                    .objectStore(storeName);
                treatment(resolve, reject, objectStore);
            });
    });

    return resultPromise;
}

// internal to the repository layer
export async function Delete({
    storeName,
    indexAccessorFunc = x => x,
    deleteFunc,
    hasCursor,
    cursorElementPredicate,
}) {
    const resultPromise = new Promise((resolve, reject) => {
        requestDatabase((database) => {
            const objectStore = database
                .transaction([storeName], TRANSACTION_RW)
                .objectStore(storeName);
            const objectStoreIndex = indexAccessorFunc(objectStore);
            const deleteRequest = deleteFunc(objectStoreIndex);
            deleteRequest.onerror = reject;
            // two cases: either there is a cursor for data retrieval or there is not
            if (hasCursor) {
                deleteRequest.onsuccess = (evt) => {
                    const cursor = evt.target.result;
                    if (cursor) {
                        // Two cases: either the cursor is opened with a constraint, like,
                        // `openCursor(IDBKeyRange.Xxx)`, or it is opened constraintless.
                        // The caller can provide a predicate to know if the current value
                        // matches, i.e., should be returned or not.
                        // It is expected to be provided in case #2,
                        // and can also be provided in case #1 if relevant.
                        const doesCurrentElementMatch = !cursorElementPredicate || cursorElementPredicate(cursor.value);
                        if (doesCurrentElementMatch) {
                            cursor.delete();
                        }
                        cursor.continue();
                    } else {
                        resolve(evt.target.result);
                    }
                };
            } else {
                deleteRequest.onsuccess = (evt) => {
                    resolve(evt.target.result);
                };
            }
        });
    });
    return resultPromise;
}

// internal to the repository layer
/// Formats the given item with regards to the given fields template.
export function formatItem({
    fieldsTemplate,
    item,
    isCreation,
    isWritingToDatabase,
}) {
    const recognisedFields = Object.keys(fieldsTemplate);
    const itemFields = Object.keys(item);
    for (const field of itemFields) {
        const recognisedKey = recognisedFields.findIndex(_recognisedField => _recognisedField === field);
        if (recognisedKey === -1) {
            console.warn(`unexpected field for store counter item: ${field}, value: ${item[field]} - this value has been ignored`);
            delete item[field];
        } else {
            const recognisedField = fieldsTemplate[recognisedFields[recognisedKey]];
            if (!recognisedField.checker(item[field])) {
                console.warn(`unexpected checker failure for store counter item, key: ${field}, value: ${item[field]}`);
                if (isCreation && recognisedField.default !== undefined) {
                    console.warn(`the default value has been used`);
                    item[field] = recognisedField.default;
                } else if (!isCreation) {
                    console.warn(`the value has been ignored`);
                    delete item[field];
                } else {
                    console.warn(`there was no possible way to handle this error`);
                    return null;
                }
            }
            item[field] = isWritingToDatabase
                ? recognisedField.toDbEntity(item[field])
                : recognisedField.toJsObject(item[field]);
        }
    }
    for (const field of recognisedFields) {
        const itemKey = itemFields.findIndex(_itemField => _itemField === field);
        if (itemKey === -1 && fieldsTemplate[field].default !== undefined) {
            item[field] = fieldsTemplate[field].default;
        }
    }
    return item;
}
