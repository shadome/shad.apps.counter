import React from 'react';

/*
const [form, setFormValues, validate, reset, resetErrors] = useForm({
    value1: {
        value: 'default1',
        validations: [{
            predicate: x => x > 0,
            error: 'Should be strictly positive',
        },{
            predicate: x => x === 2,
            error: 'Should be equal to two',
        }]
    },
    value2: {
        value: 'default2',
    },
})

form.value1.value;
form.value1.error;

setFormValues({
    value1: 'updated1',
    value2: 'updated2',
})
*/
export default function useForm(initialForm) {
    if (!Boolean(initialForm)) {
        return;
    }
    const initState = _getInitialState(initialForm);
    const validations = _getValidations(initialForm);
    
    const [form, setForm] = React.useState(initState);
    return [
        // form
        form,
        // setFormValues
        newValues => setFormValues(form, setForm, newValues),
        // validate
        () => validate(form, setForm, validations),
        // reset
        () => setForm(initState),
        // resetErrors
        () => resetErrors(form, setForm),
        // setFormErrors
        newErrors => setFormErrors(form, setForm, newErrors),
    ];
}

function resetErrors(form, setFormCallback) {
    let hasAnyErrorBeenReset = false;
    for (const key in form) {
        if (Object.prototype.hasOwnProperty.call(form, key)) {
            form[key].error = '';
        }
    }
    if (hasAnyErrorBeenReset) {
        setFormCallback({ ...form });
    }
}

function validate(form, setFormCallback, validations) {
    let isValid = true;
    for (const key in form) {
        if (Object.prototype.hasOwnProperty.call(form, key) && Object.prototype.hasOwnProperty.call(validations, key)) {
            next_form_value:
            for (const validation of validations[key]) {
                if (!validation.predicate(form[key].value)) {
                    form[key].error = validation.error;
                    isValid = false;
                    break next_form_value;
                }
            }
        }
    }
    if (!isValid) {
        setFormCallback({ ...form });
    }
    return isValid;
}

function setFormValues(form, setFormCallback, newValues) {
    for (const key in form) {
        if (Object.prototype.hasOwnProperty.call(form, key) && Object.prototype.hasOwnProperty.call(newValues, key)) {
            form[key].value = newValues[key];
            form[key].error = '';
        }
    }
    setFormCallback({ ...form });
}

function setFormErrors(form, setFormCallback, newErrors) {
    for (const key in form) {
        if (Object.prototype.hasOwnProperty.call(form, key) && Object.prototype.hasOwnProperty.call(newErrors, key)) {
            form[key].error = newErrors[key];
        }
    }
    setFormCallback({ ...form });
}

function _getInitialState(initialForm) {
    let initialState = {};
    for (const key in initialForm) {
        if (Object.prototype.hasOwnProperty.call(initialForm, key)) {
            initialState[key] = {
                value: initialForm[key].value,
                error: '',
            };
        }
    }
    return initialState;
}

function _getValidations(initialForm) {
    let validations = {};
    for (const key in initialForm) {
        if (Object.prototype.hasOwnProperty.call(initialForm, key)) {
            if (Object.prototype.hasOwnProperty.call(initialForm[key], 'validations')) {
                validations[key] = [...initialForm[key].validations];
            }
        }
    }
    return validations;
}
