import React from 'react';

export const KeyCodes = {
    arrowUp: 38,
    arrowDown: 40,
    enter: 13,
    escape: 27,
};

// useKeyBindings([
//         [ KeyCodes.arrowUp,      (evt) => { /* ... */ } ],
//         [ KeyCodes.arrowDown,    (evt) => { /* ... */ } ],
//         [ KeyCodes.enter,        (evt) => { /* ... */ } ],
//     ],
//     options = {
//         mustPreventDefault: false,
//         mustStopPropagation: false,
//     }
// );
export function useKeyBindings(
    keyBindings = [],
    options = {
        mustPreventDefault: true,
        mustStopPropagation: true,
    },
    deps = [],
) {
    function dispatchKeyCodeHandler(event) {
        const filtered = keyBindings.filter(x => x[0] === event?.keyCode);
        if (filtered.length > 0) {
            filtered[0][1](event);
            options?.mustStopPropagation && event.stopPropagation();
            options?.mustPreventDefault && event.preventDefault();
        }
    }
    // Callback required to persist the reference of the function for event listener removal
    // const dispatchKeyCodeHandlerCallback = React.useCallback(dispatchKeyCodeHandler, [...deps]);
    // return dispatchKeyCodeHandlerCallback;
    return dispatchKeyCodeHandler;
}