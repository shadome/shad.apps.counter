// [COPILOT] A function that validates a color string (hex, rgb, rgba, hsl, hsla)
function _isValidTechnicalColour(colourStr) {
    var hex = /^#[0-9A-F]{6}$/i;
    var rgb = /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/;
    var rgba = /^rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d?\.?\d+)\s*\)$/;
    var hsl = /^hsl\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*\)$/;
    var hsla = /^hsla\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*,\s*(\d?\.?\d+)\s*\)$/;
    return hex.test(colourStr)
        || rgb.test(colourStr)
        || rgba.test(colourStr)
        || hsl.test(colourStr)
        || hsla.test(colourStr);
}

// https://stackoverflow.com/questions/48484767/javascript-check-if-string-is-valid-css-color
export function isCssColour(colourStr) {
    // should handle everything but tokens such as 'red', 'aliceblue', etc.
    if (_isValidTechnicalColour(colourStr)) {
        return true;
    }
    var s = new Option().style;
    s.color = colourStr;
    return s.color === colourStr;
}

// https://stackoverflow.com/questions/14636536/how-to-check-if-a-variable-is-an-integer-in-javascript#14794066
export function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    var x = parseFloat(value);
    return (x | 0) === x;
}