import * as React from 'react';

export function useThrottle(func, delay) {
    let timeoutId = React.useRef(null);
    let lastArgs = React.useRef(null);
    let lastCalledTime = React.useRef(0);

    function throttledFunction(...args) {
        const timeSinceLastCall = Date.now() - lastCalledTime.current;

        if (timeoutId.current) {
            clearTimeout(timeoutId.current);
        }

        if (timeSinceLastCall >= delay) {
            lastCalledTime.current = Date.now();
            func.apply(this, args);
        } else {
            lastArgs.current = args;
            timeoutId.current = setTimeout(setTimeoutCallback, delay - timeSinceLastCall);
        }

        function setTimeoutCallback() {
            lastCalledTime.current = Date.now();
            func.apply(this, lastArgs.current);
        }
    }
    
    function clearTimeoutUponUnmountingComponent() {
        return () => { clearTimeout(timeoutId.current); };
    }
    React.useEffect(clearTimeoutUponUnmountingComponent, []);

    return throttledFunction;

}

// export function useThrottle(func, delay) {
//     let timeoutId;
//     let lastArgs;
//     let lastCalledTime = 0;
  
//     return function throttledFunction(...args) {
//       const timeSinceLastCall = Date.now() - lastCalledTime;
//       lastCalledTime = Date.now();
  
//       if (!timeoutId) {
//         func.apply(this, args);
//         timeoutId = setTimeout(() => {
//           timeoutId = null;
//           if (lastArgs) {
//             throttledFunction.apply(this, lastArgs);
//             lastArgs = null;
//           }
//         }, delay - timeSinceLastCall);
//       } else {
//         lastArgs = args;
//       }
//     };
//   }
  