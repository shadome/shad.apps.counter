'use strict';

export class Form {
    // values unbound to state, do not use those values directly on components' attribute,
    // changing them won't dispatch a re-render
    components = {};
    isValid = undefined;

    addComponent({
        propertyName,
        formComponent,
    }) {
        this.components[propertyName] = formComponent;
        return this;
    }

    validate() {
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                this.components[componentKey].validateComponent();
            }
        }
        this.isValid = true;
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                if (!Boolean(this.components[componentKey].isValid)) {
                    this.isValid = false;
                    return this;
                }
            }
        }
        return this;
    }

    async validateAsync() {
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                // TODO use a Promise.All or something
                await this.components[componentKey].validateComponentAsync();
            }
        }
        this.isValid = true;
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                if (!Boolean(this.components[componentKey].isValid)) {
                    this.isValid = false;
                    return this;
                }
            }
        }
        return this;
    }

    reset() {
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                this.components[componentKey].resetComponent();
            }
        }
        return this;
    }

    resetErrors() {
        for (const componentKey in this.components) {
            if (Object.prototype.hasOwnProperty.call(this.components, componentKey)) {
                this.components[componentKey].setError('');
            }
        }
        return this;
    }

    getErrors() {
        return this.components
            .filter(_componentKey => Object.prototype.hasOwnProperty.call(this.components, _componentKey))
            .map(_componentKey => this.components[_componentKey].error)
            .filter(_error => Boolean(_error?.trim()));
    }

}

export class FormComponent {
    // state values
    value = undefined;
    setValue = undefined;
    error = undefined;
    setError = undefined;
    // values unbound to state, do not use those values directly on components' attribute,
    // changing them won't dispatch a re-render
    initialValue = undefined;
    validations = [];
    validationsAsync = [];
    isValid = undefined;

    constructor({
        valueState,
        errorState,
    }) {
        // for errors, the given state's value and setter are OK
        [this.error, this.setError] = errorState;
        // for values, the given state's value is OK
        // but the given state's setter is overriden to reset errors when invoked
        const [value, setValue] = valueState;
        this.initialValue = value;
        this.value = value;
        this.setValue = (input) => {
            this.setError('');
            this.isValid = undefined;
            setValue(input);
        };
    }

    // cosmetics
    static make({
        valueState,
        errorState = [null, () => { }],
    }) {
        return new FormComponent({ valueState, errorState });
    }

    addValidation({
        predicate,
        errorMessage,
        errorSetter,
    }) {
        const newValidation = {
            predicate: predicate,
            errorMessage: errorMessage || null,
            errorSetter: errorSetter,
        };
        this.validations = [...this.validations, newValidation];
        this.isValid = undefined;
        return this;
    }

    addValidationAsync({
        predicate,
        errorMessage,
        errorSetter,
    }) {
        const newValidation = {
            predicate: predicate,
            errorMessage: errorMessage || null,
            errorSetter: errorSetter,
        };
        this.validationsAsync = [...this.validationsAsync, newValidation];
        this.isValid = undefined;
        return this;
    }

    validateComponent() {
        for (const validation of this.validations) {
            const validationResult = validation.predicate(this.value, this.error);
            if (!Boolean(validationResult)) {
                this.setError(validation.errorMessage);
                this.isValid = false;
                return this;
            }
        }
        this.isValid = true;
        return this;
    }

    async validateComponentAsync() {
        for (const validation of this.validations) {
            const validationResult = validation.predicate(this.value, this.error);
            if (!Boolean(validationResult)) {
                this.setError(validation.errorMessage);
                this.isValid = false;
                return this;
            }
        }
        for (const validation of this.validationsAsync) {
            const validationResult = await validation.predicate(this.value, this.error);
            if (!Boolean(validationResult)) {
                this.setError(validation.errorMessage);
                this.isValid = false;
                return this;
            }
        }
        this.isValid = true;
        return this;
    }

    resetComponent() {
        this.setValue(this.initialValue);
        this.setError('');
        return this;
    }
}
