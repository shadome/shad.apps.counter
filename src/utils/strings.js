export function normaliseStr(str) {
    return str.toLowerCase().replace(/[^\w\s]/g, '').replace(/\s+/g, ' ').trim();
}

/* String matching algorithm:
 * preprocessing- normalise strings: lower case, no punctuation, single whitespace separated
 * 1- mark strings which strictly include the search string, ordered by string size, ascendingly
 * 2- mark strings which match one or more words from the search string, ordered by number of matching characters, descendingly
 * 3- mark strings which contain at least one word whose levenshtein distance to a word of the search string is at most 1, 
 *      ordered by number of matching words then by size of the longest macthing word
 * 4- order the remaining strings by the size of the longest matching subsequence of the search string
 */
export function searchStrings({
    entries,
    searchStr,
    topResults = 5,
}) {
    // preprocessing- normalise strings: lower case, no punctuation, single whitespace separated
    entries = entries.map(x => {
        const normStr = normaliseStr(x);
        return {
            // keep the initial string for returning it to the caller
            initStr: x,
            // compute the normalised string once
            normStr: normStr,
            // compute the split normalised string once
            normWords: normStr.split(' '),
        };
    });
    searchStr = normaliseStr(searchStr);
    const searchStrWords = searchStr.split(' ');

    // this code is densified for better performance
    // the map add scores for every algorithmic step
    // which are later used by a sorting function
    entries = entries
        .map(entry => ({
            ...entry,
            // step 1- mark strings which strictly include the search string, ordered by string size, ascendingly
            score1: entry.normStr.includes(searchStr)
                ? 999999 - entry.normStr.length // note: Number.MAX_VALUE leads to a rounded floating number
                : 0,
            // step 2- mark strings which match one or more words from the search string, ordered by number of matching characters, descendingly
            score2: entry.normWords
                // keep words which match the search string words
                .filter(word => searchStrWords.includes(word))
                // sum the nb of char for those words
                .reduce((_nbOfMatchingChars, currentWord) => _nbOfMatchingChars + currentWord.length, 0),
            // scores 3 and 4
            // mark strings which contain at least one word whose levenshtein distance to a word of the search string is at most 1, 
            // ordered by number of matching words then by size of the longest macthing word
            ...entry.normWords
                // first reducer: match every entry word of the current entry against every search word
                .reduce(
                    (scores, word) => {
                        const matchingSearchWordsResultForCurrentEntryWord = searchStrWords
                            // second reducer: match the current entry word of the current entry against every search word
                            .reduce(
                                (matchingPreviousSearchWordsResults, searchWord) => {
                                    const newDistance = Math.abs(word.length - searchWord.length) > 1
                                        // shortcut: levenshtein distance cannot be <= 1
                                        ? 999
                                        // compute the distance between the current entry word and the current search word
                                        : levenshteinDistance(word, searchWord);
                                    const matchingSearchWordResult = {
                                        dist: newDistance,
                                        matchingWordLength: Math.max(searchWord.length, word.length),
                                    }
                                    return [...matchingPreviousSearchWordsResults, matchingSearchWordResult];
                                },
                                [])
                            .filter(x => x.dist <= 1)
                            .sort((a, b) => b.matchingWordLength - a.matchingWordLength);
                        return matchingSearchWordsResultForCurrentEntryWord.length === 0
                            ? scores
                            : {
                                // nb of matching words
                                score3: scores.score3 + 1,
                                // size of the longest matching word
                                score4: Math.max(scores.score4, matchingSearchWordsResultForCurrentEntryWord[0].matchingWordLength),
                            };
                    },
                    { score3: 0, score4: 0 })
        }));

    // 4- order the remaining strings by the size of the longest matching subsequence of the search string
    // NOT IMPLEMENTED YET - doesn't feel necessary
    
    const result = entries
        .sort((a, b) => b.score4 - a.score4)
        .sort((a, b) => b.score3 - a.score3)
        .sort((a, b) => b.score2 - a.score2)
        .sort((a, b) => b.score1 - a.score1)
        .slice(0, topResults)
        .map(x => x.initStr);
    return result;
}

function levenshteinDistance(a, b) {
    if (a.length === 0) return b.length;
    if (b.length === 0) return a.length;

    let matrix = [];

    // fill the first row
    for (let i = 0; i <= b.length; i++) {
        matrix[i] = [i];
    }

    // fill the first column
    for (let j = 0; j <= a.length; j++) {
        matrix[0][j] = j;
    }

    // fill the rest of the matrix
    for (let i = 1; i <= b.length; i++) {
        for (let j = 1; j <= a.length; j++) {
            if (b.charAt(i - 1) === a.charAt(j - 1)) {
                matrix[i][j] = matrix[i - 1][j - 1];
            } else {
                matrix[i][j] = Math.min(
                    matrix[i - 1][j - 1] + 1, // substitution
                    matrix[i][j - 1] + 1,     // insertion
                    matrix[i - 1][j] + 1      // deletion
                );
            }
        }
    }

    return matrix[b.length][a.length];
}

function longestCommonSubsequence(str1, str2) {
    const len1 = str1.length;
    const len2 = str2.length;

    // initialise a 2D array to store lengths of LCS
    const lcsLengths = [];
    for (let i = 0; i <= len1; i++) {
        lcsLengths[i] = new Array(len2 + 1).fill(0);
    }

    // compute lengths of LCS
    for (let i = 1; i <= len1; i++) {
        for (let j = 1; j <= len2; j++) {
            if (str1[i - 1] === str2[j - 1]) {
                lcsLengths[i][j] = lcsLengths[i - 1][j - 1] + 1;
            } else {
                lcsLengths[i][j] = Math.max(lcsLengths[i - 1][j], lcsLengths[i][j - 1]);
            }
        }
    }

    // extract LCS from the lengths array
    let lcs = '';
    let i = len1;
    let j = len2;
    while (i > 0 && j > 0) {
        if (str1[i - 1] === str2[j - 1]) {
            lcs = str1[i - 1] + lcs;
            i--;
            j--;
        } else if (lcsLengths[i - 1][j] >= lcsLengths[i][j - 1]) {
            i--;
        } else {
            j--;
        }
    }

    return lcs;
}
