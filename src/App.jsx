import * as React from 'react';
import * as Router from 'react-router-dom';

import Pages from 'front/pages';
import CssInteropInitialiser, { getCss } from './front/CssInterop';
import * as Contexts from 'context';
import * as FoodStore from 'repository/StoreFood';
import * as Breakpoints from 'front/Breakpoints';

import Navbar, { NavbarHeightInPx } from 'front/containers/navbar/Navbar';
import Menu, { MenuFixedWidthInPx } from 'front/containers/menu/Menu';
import EmptyFoodDatabaseContainer from 'front/containers/EmptyFoodDatabaseContainer.jsx/EmptyFoodDatabaseContainer';
import FeedbackArea from 'front/containers/feedback_area/FeedbackArea';
import FoodDataSource from 'model/FoodDataSource';

export default function App() {
    /* Initialisers */
    // 900 matches the md breakpoint: https://mui.com/customization/breakpoints/
    const calculateDisplay = (width) => width > 900 ? 'desktop' : 'mobile';
    const initialDisplay = calculateDisplay(window.innerWidth);

    /* State variables */
    const [themeMode, setThemeMode] = React.useState('dark');
    const [navigationTitle, setNavigationTitle] = React.useState('');
    const [isMenuOpen, setMenuOpen] = React.useState(false);
    const [display, setDisplay] = React.useState(initialDisplay);
    const [isFoodDatabaseEmpty, setFoodDatabaseEmpty] = React.useState(false);

    /* Effects */
    // Set the database-is-empty state variable
    const setFoodDatabaseStateEffect = () => {
        let isMounted = true;
        FoodStore
            .Read({ data_source: FoodDataSource.ciqual })
            .then((foods) => isMounted && setFoodDatabaseEmpty(foods.length === 0))
            .catch((error) => console.warn(`Catching setFoodDatabaseStateEffect: '${error}'`));
        return () => { isMounted = false; };
    };
    React.useEffect(setFoodDatabaseStateEffect, []);

    /* Listen to the resize event and set the breakpoint accordingly */
    const [breakpoint, setBreakpoint] = React.useState(Breakpoints.fromPx(window.innerWidth));
    function updateBreakpointEffect() {
        window.addEventListener('resize', updateBreakpoint);
        return () => {
            window.removeEventListener('resize', updateBreakpoint);
        }
        function updateBreakpoint() {
            setBreakpoint(Breakpoints.fromPx(window.innerWidth));
        };
    };
    React.useEffect(updateBreakpointEffect, []);

    // Hook listening to the resize event to switch between display modes
    const updateDisplayListeners = () => {
        const updateDisplay = () => {
            const display = calculateDisplay(window.innerWidth);
            setDisplay(display);
        };
        window.addEventListener('resize', updateDisplay);
        // effect cleanup: on component unmount, remove resize listener
        return () => {
            window.removeEventListener('resize', updateDisplay);
        };
    };
    React.useEffect(updateDisplayListeners, []);

    /* Feedbacks state */
    const feedbackDisplayTimeInMs = 5000;
    const [feedbacks, setFeedbacks] = React.useState([]);
    const [feedbackIdToRemove, setFeedbackIdToRemove] = React.useState(null);
    function addFeedback({ aspect, label, displayTimeInMs = undefined }) {
        const existingFeedbackIds = !!feedbacks && !!feedbacks.length
            ? feedbacks.map(_feedback => _feedback.id)
            : [0];
        const newFeedbackId = Math.max.apply(null, existingFeedbackIds) + 1;
        const newFeedback = {
            id: newFeedbackId,
            aspect: aspect,
            label: label,
            onClose: () => setFeedbackIdToRemove(newFeedbackId),
        };
        setFeedbacks(feedbacks.concat(newFeedback));
        setTimeout(() => setFeedbackIdToRemove(newFeedbackId), displayTimeInMs || feedbackDisplayTimeInMs);
        // this should not be necesasry but sometimes, probably for a fps reason, 
        // too many simultaneous feedbacks let some uncleared
        existingFeedbackIds.forEach(_feedbackId =>
            setTimeout(() => setFeedbackIdToRemove(_feedbackId), displayTimeInMs || feedbackDisplayTimeInMs));
    }
    function removeFeedback() {
        if (feedbackIdToRemove === null) return;
        const newFeedbacks = feedbacks.filter(_feedback => _feedback.id != feedbackIdToRemove);
        setFeedbacks(newFeedbacks);
    }
    React.useEffect(removeFeedback, [feedbackIdToRemove]);

    /* Variables */
    // the appbar's rendering is forced in dark mode to avoid all sorts of overrides with !important and stuff for its subcomponents
    const isMenuMinimised = breakpoint < Breakpoints.lg;

    const css = getCss(themeMode, breakpoint);

    return (
        <React.StrictMode>
            <Router.HashRouter>
                <Contexts.SetTitleContext.Provider value={setNavigationTitle}>
                    <Contexts.IsFoodDatabaseEmptyContext.Provider value={{ isFoodDatabaseEmpty, setFoodDatabaseEmpty }}>
                        <Contexts.DisplayContext.Provider value={display}>
                            <Contexts.BreakpointContext.Provider value={breakpoint}>
                                <Contexts.ThemeModeContext.Provider value={themeMode}>
                                    <Contexts.AddFeedbackContext.Provider value={addFeedback}>
                                        <Contexts.CssContext.Provider value={css}>

                                        <CssInteropInitialiser />

                                        <Navbar
                                            title={navigationTitle}
                                            toggleOpenMenu={() => { setMenuOpen(!isMenuOpen); }}
                                            isMenuMinimised={isMenuMinimised}
                                            setThemeMode={setThemeMode}
                                        />

                                        <Menu
                                            isOpen={isMenuOpen}
                                            isMenuMinimised={isMenuMinimised}
                                            onClose={() => { setMenuOpen(false); }}
                                        />
                                        <div style={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            top: '64px',
                                            left: isMenuMinimised ? '0' : `${MenuFixedWidthInPx}px`,
                                            minWidth: isMenuMinimised ? '100%' : `calc(100% - ${MenuFixedWidthInPx}px)`,
                                            maxWidth: isMenuMinimised ? '100%' : `calc(100% - ${MenuFixedWidthInPx}px)`,
                                            minHeight: `calc(100% - ${NavbarHeightInPx}px)`,
                                            height: `calc(100% - ${NavbarHeightInPx}px)`, // required for the scrollbar
                                            position: 'absolute',
                                            overflow: 'scroll',
                                            padding: '16px',
                                            gap: '16px',
                                            scrollbarColor: `${css.colour.border} ${css.colour.background.dp0}`,
                                        }}>
                                            {isFoodDatabaseEmpty &&
                                                <EmptyFoodDatabaseContainer />
                                            }
                                            <Router.Switch>
                                                <Router.Route
                                                    exact
                                                    path={Pages.HomePage.paths}
                                                    component={Pages.HomePage.Component}
                                                />
                                                {/* <Router.Route
                                                    exact
                                                    path={Pages.DashboardPage.paths}
                                                    component={Pages.DashboardPage.Component}
                                                /> */}
                                                <Router.Route
                                                    exact
                                                    path={Pages.DataPage.paths}
                                                    component={Pages.DataPage.Component}
                                                />
                                                <Router.Route
                                                    exact
                                                    path={Pages.CounterAddPage.paths}
                                                    component={Pages.CounterAddPage.Component}
                                                />
                                                <Router.Route
                                                    exact
                                                    path={Pages.FoodPage.paths}
                                                    component={Pages.FoodPage.Component}
                                                />
                                                <Router.Route
                                                    exact
                                                    path={Pages.DashboardPage.paths}
                                                    component={Pages.DashboardPage.Component}
                                                />
                                                <Router.Route
                                                    exact
                                                    path={Pages.ShowcasePage.paths}
                                                    component={Pages.ShowcasePage.Component}
                                                />
                                                <Router.Route
                                                    path={Pages.NotFoundPage.paths}
                                                    component={Pages.NotFoundPage.Component}
                                                />
                                            </Router.Switch>
                                        </div>

                                        <FeedbackArea feedbacks={feedbacks} />

                                        </Contexts.CssContext.Provider>
                                    </Contexts.AddFeedbackContext.Provider>
                                </Contexts.ThemeModeContext.Provider>
                            </Contexts.BreakpointContext.Provider>
                        </Contexts.DisplayContext.Provider>
                    </Contexts.IsFoodDatabaseEmptyContext.Provider>
                </Contexts.SetTitleContext.Provider>
            </Router.HashRouter>
        </React.StrictMode>
    );
};
