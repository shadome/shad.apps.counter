# Loose conventions

* Database-related fields and variables are in `snake_case` whereas javascript related fields and variables are in `camelCase`
* This project will slowly migrate from the [MaterialUi](https://mui.com/) library to pure HTML/CSS.
  * `JQuery` will never be used, so individual and lightweight libraries will provide complex elements such as a date pickers or a select dropdowns.
  * Motivation 1: although MUI is a great project, I believe I'm better off knowing html, css and UX design in an agnostic fashion rather than specialising into MUI's specificities
  * Motivation 2: it is also my belief that the idea of making one library as generic as possible in order to cover most use-cases on most browsers necessarily comes with a big trade-off in performance, readability of the generated css, complexity of APIs, etc. Trying to resolve issues arising as a result of using complex code with some even more complex code is not my thing, though it has already happened more than once with MUI components and style overrides.

# Functions and scopes

## Loose conventions

* Code-splitting (making dedicated functions, react components, etc.) is motivated by at least one of the following reasons: exposing a simplified interface, factorising code. Aesthetic code-splitting must be avoided: splitting code must give information to the reader.
* Reducing the number of rows of a function, a class, a component, etc. should never be a reason to deteriorate code readability. If necessary, the code could be split considering the above motive **exposing a simplified interface**, e.g., making a dedicated component which takes a reasonable number of `props`. If not possible, the number of rows would remain high.

## Scopes

Specific attention is dedicated to scopes, *i.e., from where is accessible any field, expression, function, etc. (up to the capabilities of ES, which is not the best framework for specifying scopes)*. For instance, if a function is exclusively used in another function (and would never be usable elsewhere for Domain reasons), it should be declared **within** that function.

## Function structure

### Introduction

If it improves readabiliy, *e.g., for functions with heavy treatment,* a typical function or scope differentiates the *what* from the *how*. 

It is structured as follows:
* Noticeable constants, if any
* Main treatment flow *(**what**, or **abstraction**)*
* Subtreatments' implementation *(**how**, or **implementation**)*

As a result, the **Main treatment flow** point of this convention naturally incites the usage of certain type's prototypes function if it enables chaining, especially `Array.prototype`.

*Note: even though any function could be declared either as a function or as a lambda, i.e., `function toto(param) { return param; }` vs. `const toto = (param) => param;`, when implementing the above structure, lambdas should not be used.*

### Example
```js
  // this function must transform a flat list of meals w/ or w/o a meal_group
  // into a two-leveled list with meal groups enclosing meal entries
  // (or with a flat meal with no entries if it is groupless)
  // so that the first level of the list suffices to make all calculations
  function inflateMealViewModelsEffect() {
    // what
    const mealGroupsDict = mealsToMealGroupsDict(meals);
    const mealGroups = Object
      .keys(mealGroupsDict)
      .map(key => mealGroupsDict[key])
      .map(meal => inflateMealNutrients(meal))
      .map(meal => inflateMealGroupMainMicronutrients(meal));
    setMealsViewModel(mealGroups);
    setIsLoading(false);
    // how
    function mealsToMealGroupsDict(meals) {
      // inner what
      return meals.reduce(
        (accumulator, meal) =>
          meal.group_label
            ? addMealEntry(accumulator, meal)
            : addGrouplessMeal(accumulator, meal),
        {});
      // inner how
      function addMealEntry(dict, mealEntry) {
        const previousMealGroupFields = dict[mealEntry.group_label] ?? {};
        const previousMealGroupEntries = dict[mealEntry.group_label]?.entries ?? [];
        return {
          ...dict,
          [mealEntry.group_label]: {
            date_consumption: mealEntry.date_consumption,
            label: mealEntry.group_label,
            ...previousMealGroupFields,
            entries: [...previousMealGroupEntries, mealEntry],
          },
        };
      };
      function addGrouplessMeal(dict, meal) {
        return {
          ...dict,
          [meal.label]: meal,
        };
      };
    };
    function inflateMealNutrients(meal) {
      // inner what
      return meal.entries && meal.entries.length
        ? {
          ...meal,
          quantity: meal.entries.reduce((acc, mealEntry) => acc + mealEntry.quantity, 0),
          ...[]
            .concat(FoodMetadata.MainMacronutrients)
            .concat(FoodMetadata.DetailedMacronutrients)
            .concat(FoodMetadata.Vitamins)
            .concat(FoodMetadata.Minerals)
            .reduce(getMealGroupNutrientsReducer(meal), {}),
        }
        : meal;
      // inner how
      function getMealGroupNutrientsReducer(mealGroup) {
        // inner inner what
        return (dict, nutrientMetadata) => ({
          ...dict,
          [nutrientMetadata.id]: mealGroup.entries.reduce(getMealEntryNutrientReducer(nutrientMetadata), 0),
        });
        // inner inner how
        function getMealEntryNutrientReducer(nutrientMetadata) {
          return (sum, mealEntry) => sum + (Number(mealEntry[nutrientMetadata.id]) || 0);
        };
      };
    };
    // etc.
```

### Conclusion

This convention seems
* harder to understand at first and
* more voluminous than a compressed equivalent, 

but 
* it needs less comments *(the name of the functions can be considered as comments and the logic behind a specific code-splitting also provides information)*, 
* each scope can be understood individually, and
* the risk of error when maintaining this code is, in my experience, greatly decreased.

Use your IDE to collapse unnecessary scopes!

Of course, distinguishing the *what* and the *how* is a simplified way of apprehending development. This distinction cannot, strictly speaking, be observed: there is always of little bit of both everywhere.
